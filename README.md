portal2E
========

portal2D engine with E's

originally by Derek J Evans: https://sourceforge.net/projects/portal2d/  
bodged to run on 'modern hardware' by j0e et al

Runs/tested on linux 32-bit and 64-bit, will probably need tweaking on other OSs.

Doing `make` should be enough to compile.
Uses libm and (either libX11 or sdl2) too, should be self-contained apart from that.

Still a little unstable (might get a few segfaults / div0 errors / asserts here and there) but fun nonetheless :^)

The editor console has full access to the scripting language.
Type commands and hit the 'DO IT' button to execute (or press enter).
Output is whatever is 'returned' by the command.

Use the `loadmap` and `savemap` commands to load and save levels.
For example `loadmap lp2` will load the file at `media/maps/lp2.map`.

The main (and original p2d) editor mode is DRAW mode.
Click in the map view area to draw lines and form enclosed sectors.
Sectors MUST be convex and drawn in a clockwise direction.
If you want to join a sector to another, make sure the walls of your new sector line up exactly with the walls of the existing sector.

KEYS (and mouse buttons):
-----

###  2d editor mode main keys:

| key        | what it does
|------------|--------------
| ENTER      | go 'in-game'
| [ / ]      | change grid size
| - / =      | change zoom
| mousewheel | change zoom
| arrow keys | pan
| alt + drag | pan with mouse
| pgup/pgdn  | move up / down layers
| ctrl+Z     | undo


###  DRAW mode:

| key        | what it does
|------------|--------------
| click      | draw walls
| esc        | cancel drawing once begun
| backspace  | backtrack drawing one line 
|            | (I think, can't remember if I've implemented this yet :^])


###  WALLS mode:

| key           | what it does
|---------------|--------------
| click         | select a wall
| shift + click | select multiple walls
not much else lol


###  SECTORS mode:

| key           | what it does
|---------------|--------------
| click         | select a sector
| shift + click | select multiple sectors
| shift + pgup  | extend sector to layer above
| shift + pgdn  | extend sector to layer below
| delete        | delete selected sectors

Layers can be used for room-over-room effects and other such tomfoolery.
Extending a sector into an adjacent layer will link the two sectors with a floor/ceiling portal.
Check out lp2.map for a primitive example.


###  in 'game' (i.e. 3D mode):

Commands that edit surfaces affect the surface pointed to by the cursor/crosshair.
For height, slope and texture offset, you can hold down SHIFT for smaller increments.

| key         | what it does
|-------------|--------------
| ESC         | go back to the editor
| WASD        | move and strafe
| arrow keys  | turn and look up & down
| SPACE       | to jump
| CTRL        | crouch
| move mouse  | turn and look when mouse look is enabled
| alt+C       | toggle cursor grab / mouse look
| mouse wheel | floor/ceiling height (point cursor at the appropriate floor/ceiling surface)
| J/K + wheel | floor/ceiling X/Y slope
| L + wheel   | surface light level
| T + wheel   | surface texture
| X/Y + wheel | surface X/Y panning (texture offset)
| H/V + wheel | surface horizontal/vertical texture scale
| O           | surface toggle opacity (only applies to midtextures and objects)
| M           | toggle 'middle texture' (multi-level / water / lava) of player's current sector


CREDITS
-------

- [fenster](https://zserge.com/posts/fenster/) for base of Xlib platform (+ win32 in future)
- Bright.exe ([forum thread, best link I could find](https://www.ttlg.com/forums/showthread.php?t=99850)) and [pngnq](https://pngnq.sourceforge.net/) for 256-colour texture conversion magic ([pngnq github](https://github.com/stuart/pngnq))
- [spng](https://libspng.org/) ([github](https://github.com/randy408/libspng)) (and [miniz](https://github.com/richgel999/miniz)) and formerly [gbm](http://www.nyangau.org/gbm/gbm.htm) used for loading image files for graphics & textures
- DEU for some of the editor line/sector selection routines
- [fe](https://github.com/rxi/fe/) for scripting language (plus [aq](https://github.com/rxi/aq/) and [cel7ce](https://github.com/kiedtl/cel7ce) for additional scripting features)
- [microui](https://github.com/rxi/microui.git)
- [Upscayl](https://www.upscayl.org/) for cheesy neural-network image upscaler (used to upscale original 64x64 texture set to 128x128 lol) ([github](https://github.com/upscayl/upscayl))
- [erysdren](https://erysdren.me/) for SDL2 port and support [(github)](https://github.com/erysdren)
- and of course underrated genius [Derek J Evans](https://sourceforge.net/u/buzzphp/profile/) for the splendid portal2D engine.
