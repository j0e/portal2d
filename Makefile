-include config.mk

ifeq ($(strip $(PLATFORM)),)
  $(error PLATFORM is not defined. Pass via command line or copy `config.default.mk` to `config.mk` and follow instructions within)
endif

TARGET_EXEC ?= portal2d

CC ?= gcc
OBJDIR ?= obj

dirs = include p2d platform
OBJDIR ?= obj
srcnames = $(shell cd src; find include p2d -name '*.c') platform/$(PLATFORM).c main.c
srcfiles = $(addprefix src/, $(srcnames))
objnames = $(addsuffix .o, $(srcnames))
objfiles = $(addprefix $(OBJDIR)/, $(objnames))
objdirs = $(addprefix $(OBJDIR)/, $(dirs))

cflagsdefault = -Isrc -DDIV0_SATURATE -DSPNG_USE_MINIZ -Werror-implicit-function-declaration
CLDFLAGS ?= -ggdb3
ldflagsdefault =

# platform: heavily-modified fenster xlib
ifeq ($(filter $(PLATFORM),fenster fen_xlib),$(PLATFORM))
  cflagsdefault := $(cflagsdefault) -DPLAT_FENSTER -DPLAT_INCLUDE_FILE=\"platform/fenster.h\"
  ldflagsdefault := $(ldflagsdefault) -lm -lX11
  # TODO other OSes from fenster example makefile(s)
endif

# platform: SDL2
ifeq ($(PLATFORM), sdl2)
  cflagsdefault := $(cflagsdefault) -DPLAT_SDL2 -DPLAT_INCLUDE_FILE=\"platform/sdl2.h\" `sdl2-config --cflags`
  ldflagsdefault := $(ldflagsdefault) -lm `sdl2-config --libs`
endif

.PHONY: clean default build

default: build

clean:
	rm -rfv obj portal2d

$(objdirs):
	mkdir -p $@

$(OBJDIR): $(objdirs)

$(objfiles): $(OBJDIR)/%.c.o: src/%.c
	$(CC) -c $< -o $@ $(CPPFLAGS) $(cflagsdefault) $(CFLAGS) $(CLDFLAGS)

#bin/:
#	mkdir -p bin
#bin/$(TARGET_EXEC): bin/ $(objfiles)

$(TARGET_EXEC): $(objfiles)

build: $(OBJDIR) $(TARGET_EXEC)
	$(CC) $(objfiles) -o $(TARGET_EXEC) $(ldflagsdefault) $(CLDFLAGS) $(LDFLAGS)
