typedef struct fenster fenster;

#define KB_ESC        27
#define KB_DNARROW    18
#define KB_LTARROW    20
#define KB_RTARROW    19
#define KB_UPARROW    17

#define KB_ENTER      10

#define KB_HOME        2
#define KB_PAGEUP      3
#define KB_END         5
#define KB_PAGEDN      4
#define KB_INSERT     26
#define KB_DELETE    127

#define KB_BACKSPACE   8

#define KB_CTRL       21
#define KB_SHIFT      22
#define KB_ALT        23
#define KB_META       24

#define MOD_CTRL       1
#define MOD_SHIFT      2
#define MOD_ALT        4
#define MOD_META       8

#define MB_LEFT        1
#define MB_MIDDLE      2
#define MB_RIGHT       3
#define MB_WHEELUP     4
#define MB_WHEELDN     5

#define FENSTER_KEYCODE_MAX 256
#define FENSTER_SCANCODE_MAX 140
#define FENSTER_MOUSEBTN_MAX 32
#define PLAT_PALETTE_MAX 16
