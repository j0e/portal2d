#define _XOPEN_SOURCE 600

#include "p2d/platform.h"
#include "p2d/engine.h"
#include "include/fenster.h"

fenster *f; // TODO make non-pointer and perhaps static

// default width and height
unsigned plat_screen_w = 640;
unsigned plat_screen_h = 480;

/*
** Timer Routines. Clocked at 120(!) ticks per second.
*/
unsigned plat_timer;
unsigned plat_mstotal;

static unsigned mstotal_lasttimer;
const unsigned timer_interval = 1000 / 120;

/*
** Keyboard and mouse
*/
int keydown[128];
int keyprev[128];
int moddown;
int modprev;
char mousedown[1]; // only one mouse button for now lmao, moar mouse buttons in future
char mouseprev[1];
int plat_mouse_x;
int plat_mouse_y;

void plat_mouse_show(int on) {
	fenster_showmouse(f, on);
}

void plat_mouse_read(int* x, int* y) {
	if (x) *x = f->x;
  	if (y) *y = f->y;
}

void plat_mouse_write(int x, int y) {
	// just setting f->x and f->y won't do it.
	// need to impl fenster 'set mouse' function.
	// TODO check if the window has focus and only fire if it does
	fenster_setmouse(f, x, y);
}

int plat_mouse_press(int* x, int* y) {
	if (x) *x = f->x;
	if (y) *y = f->y;

	return mousedown[0] && !mouseprev[0];
}

/*
** Display
*/
PIXEL* fscreen;
PALLETE fpalette;

void plat_palette_install(PALLETE newpalette) {
	int i;

	for (i = 0; i < 256; i++) {
		fpalette[i][0] = newpalette[i][0];
		fpalette[i][1] = newpalette[i][1];
		fpalette[i][2] = newpalette[i][2];
	}
}

void plat_fb_init(PICTURE* fb, int width, int height) {
	picture_create(fb, width, height, 32, 0, fscreen);
}
void plat_fb_deinit(PICTURE* fb) {
	picture_destroy(fb);
}
void plat_fb_render(PICTURE* dst, PICTURE* src) {
	int x, y;
	BYTE* colour;
	uint32_t* buffer = dst->buffer;

	for (y = 0; y < dst->height; y++) for (x = 0; x < dst->width; x++) {
		// TODO pixel doubling or otherwise better pixel scaling
		//#if (SCR_W == FB_W*2 && SCR_H == FB_H*2)
		//	colour = fpalette[src->scanlines.b[y>>1][x>>1]];
		//#elif (SCR_W > FB_W || SCR_H > FB_H)
		//	colour = fpalette[src->scanlines.b[(int)(y * src->height / dst->height)][(int)(x * src->width / dst->width)]];
		//#else
			colour = fpalette[src->scanlines.b[y][x]];
		//#endif

		buffer[y*dst->width + x] = colour[0]*65536 + colour[1]*256 + colour[2];
	}
}

/******************************************************************************/

// input event hooks.
// TODO initialise these as default 'no-ops'
void (*plat_event_keypress)(int plat_key, int text_key);
void (*plat_event_keyrelease)(int plat_key, int text_key);
void (*plat_event_mousepress)(int x, int y, int button);
void (*plat_event_mouserelease)(int x, int y, int button);
void (*plat_event_mousemove)(int x, int y);

void plat_init() {
	memset(keydown, 0, sizeof(keydown));
	memset(keyprev, 0, sizeof(keyprev));
	memset(mousedown, 0, sizeof(mousedown));
	memset(mouseprev, 0, sizeof(mouseprev));
	moddown = 0;
	modprev = 0;
	plat_mouse_x = 0;
	plat_mouse_y = 0;

	fscreen = malloc(sizeof(PIXEL) * plat_screen_w * plat_screen_h);

	fenster _f = {
		.title = "portal2d",
		.width = plat_screen_w,
		.height = plat_screen_h,
		.buf = fscreen,
	};

	f = malloc(sizeof(fenster));

	memcpy(f, &_f, sizeof(fenster));

	fenster_open(f);
}

void plat_init_time() {
	plat_timer = 0;
	mstotal_lasttimer = plat_mstotal = fenster_time();
}

int plat_update() {
	memcpy(keyprev, keydown, sizeof(keyprev));
	memcpy(keydown, f->keys, sizeof(keyprev));
	modprev = moddown;
	moddown = f->mod;
	mouseprev[0] = mousedown[0];
	mousedown[0] = f->mouse;

	return fenster_loop(f);
}

unsigned plat_update_time() {
	plat_mstotal = fenster_time();

	return plat_mstotal;
}

int plat_deinit() {
	fenster_close(f);
	free(fscreen);
	free(f);
	return 0;
}

void plat_video_update8(uint32_t *dst, uint8_t *src, int width, int height)
{
	int x, y;
	BYTE* colour;
	
	for (y = 0; y < height; y++) for (x = 0; x < width; x++) {
		colour = fpalette[src[(y * width) + x]]; //= fpalette[src->scanlines.b[y][x]];

		dst[y * width + x] = colour[0]*65536 + colour[1]*256 + colour[2];
	}

	fenster_video_update(f);
}
void plat_input_update()
{
	memcpy(keyprev, keydown, sizeof(keyprev));
	memcpy(keydown, f->keys, sizeof(keyprev));
	modprev = moddown;
	moddown = f->mod;
	mouseprev[0] = mousedown[0];
	mousedown[0] = f->mouse;
	fenster_input_update(f);
}

/******************************************************************************/
