#include "p2d/platform.h"
#include "p2d/engine.h"

#define _XOPEN_SOURCE 600
#define _DEFAULT_SOURCE 1
#include <stdint.h>
#include <stdlib.h>
#include <X11/XKBlib.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <time.h>

int plat_exiting = 0;

/*
	time
*/
unsigned plat_mstotal;

static inline int64_t getmstotal()
{
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
	return time.tv_sec * 1000 + (time.tv_nsec / 1000000);
}

void plat_time_init()
{
	plat_mstotal = getmstotal();
}

unsigned plat_time_update()
{
	return (plat_mstotal = getmstotal());
}

/*
	input event hooks
*/
// Prototypes for platform event hooks.
// Can be extended or overridden by platform implementation.

// TODO put these in platform.h and #define _PLAT_IMPL_ or something in this flie
//      to avoid boilerplate in every platform file?
void plat_default_event_keypress(int plat_key, int text_key) { }
void plat_default_event_keyrelease(int plat_key, int text_key) { }
void plat_default_event_mousepress(int x, int y, int button) { }
void plat_default_event_mouserelease(int x, int y, int button) { }
void plat_default_event_mousemove(int x, int y) { }

plat_fn_keypress_t plat_event_keypress;
plat_fn_keyrelease_t plat_event_keyrelease;
plat_fn_mousepress_t plat_event_mousepress;
plat_fn_mouserelease_t plat_event_mouserelease;
plat_fn_mousemove_t plat_event_mousemove;

/*
	video and rendering
*/
unsigned plat_screen_w = 640;
unsigned plat_screen_h = 480;
// TODO separate window and framebuffer sizes later :^)

uint32_t *plat_framebuffer = 0;

//typedef uint8_t _plat_palette_t_[256][3];

uint8_t plat_palettes[PLAT_PALETTE_MAX][256][3];
uint8_t plat_palette[256][3];

// x11 stuff
static Display* f__dpy;
static Window f__w;
static GC f__gc;
static XImage* f__img;

static char* window_title;

void plat_palette_install(uint8_t palette_i, uint8_t newpalette[256][3])
{
	memcpy(plat_palettes[palette_i], newpalette, sizeof(uint8_t[256][3]));
}

void plat_palette_set(uint8_t palette_i)
{
	memcpy(plat_palette, plat_palettes[palette_i], sizeof(uint8_t[256][3]));
}

void plat_window_update()
{
	XPutImage(f__dpy, f__w, f__gc, f__img, 0, 0, 0, 0, plat_screen_w, plat_screen_h);
	XFlush(f__dpy);
}

void plat_video_update8(uint8_t *src)
{
	// render frame buffer
	int x, y;
	uint8_t* colour;

	for (y = 0; y < plat_screen_h; y++) for (x = 0; x < plat_screen_w; x++) {
		// TODO pixel doubling or otherwise better pixel scaling
		//#if (SCR_W == FB_W*2 && SCR_H == FB_H*2)
		//	colour = fpalette[src->scanlines.b[y>>1][x>>1]];
		//#elif (SCR_W > FB_W || SCR_H > FB_H)
		//	colour = fpalette[src->scanlines.b[(int)(y * src->height / dst->height)][(int)(x * src->width / dst->width)]];
		//#else
		//	colour = fpalette[src->scanlines.b[y][x]];
		//#endif

		colour = plat_palette[src[(y * plat_screen_w) + x]]; // TODO plat_scanlines for faster(?) 'blitting'

		plat_framebuffer[y*plat_screen_w + x] = colour[0]*65536 + colour[1]*256 + colour[2];
	}

	plat_window_update();
}

void plat_video_init(unsigned width, unsigned height, const char* title)
{
	plat_screen_w = width;
	plat_screen_h = height;

	plat_framebuffer = malloc(sizeof(uint32_t) * plat_screen_w * plat_screen_h);

	if (!title) {
		title = "portal2e";
	}
	unsigned memlen = strlen(title) + 1;
	assert(!window_title);
	window_title = malloc(memlen);
	memcpy(window_title, title, memlen);

	f__dpy = XOpenDisplay(NULL);
	int screen = DefaultScreen(f__dpy);
	f__w = XCreateSimpleWindow(
		f__dpy, RootWindow(f__dpy, screen),
		0, 0, plat_screen_w, plat_screen_h, 0,
		BlackPixel(f__dpy, screen), WhitePixel(f__dpy, screen)
	);
	f__gc = XCreateGC(f__dpy, f__w, 0, 0);
	XSelectInput(f__dpy, f__w,
		ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask
		| ButtonReleaseMask | PointerMotionMask
	);
	XStoreName(f__dpy, f__w, window_title);
	XMapWindow(f__dpy, f__w);
	XSync(f__dpy, f__w);
	f__img = XCreateImage(f__dpy,
		DefaultVisual(f__dpy, 0), 24, ZPixmap, 0,
		(char *)plat_framebuffer, plat_screen_w, plat_screen_h, 32, 0
	);
}

void plat_video_deinit()
{
	XCloseDisplay(f__dpy);
	free(plat_framebuffer);
	plat_framebuffer = 0;
}


/*
	input (keyboard and mouse)
*/

uint8_t plat_keydown[FENSTER_KEYCODE_MAX];
uint8_t plat_keyprev[FENSTER_KEYCODE_MAX];
uint32_t plat_moddown;
uint32_t plat_modprev;
uint8_t plat_mousedown[FENSTER_MOUSEBTN_MAX];
uint8_t plat_mouseprev[FENSTER_MOUSEBTN_MAX];
int plat_mouse_x;
int plat_mouse_y;

static int FENSTER_SCANCODES[FENSTER_SCANCODE_MAX] = {XK_BackSpace,8,XK_Delete,127,XK_Down,18,XK_End,5,XK_Escape,27,XK_Home,2,XK_Insert,26,XK_Left,20,XK_Page_Down,4,XK_Page_Up,3,XK_Return,10,XK_Right,19,XK_Tab,9,XK_Up,17,XK_apostrophe,39,XK_backslash,92,XK_bracketleft,91,XK_bracketright,93,XK_comma,44,XK_equal,61,XK_grave,96,XK_minus,45,XK_period,46,XK_semicolon,59,XK_slash,47,XK_space,32,XK_a,65,XK_b,66,XK_c,67,XK_d,68,XK_e,69,XK_f,70,XK_g,71,XK_h,72,XK_i,73,XK_j,74,XK_k,75,XK_l,76,XK_m,77,XK_n,78,XK_o,79,XK_p,80,XK_q,81,XK_r,82,XK_s,83,XK_t,84,XK_u,85,XK_v,86,XK_w,87,XK_x,88,XK_y,89,XK_z,90,XK_0,48,XK_1,49,XK_2,50,XK_3,51,XK_4,52,XK_5,53,XK_6,54,XK_7,55,XK_8,56,XK_9,57,XK_Control_L,21,XK_Control_R,21,XK_Shift_L,22,XK_Shift_R,22,XK_Alt_L,23,XK_Alt_R,23,XK_Meta_L,24,XK_Meta_R,24};

void plat_input_init(void)
{	// TODO perhaps params could have e.g. joysticks etc
	memset(plat_keydown, 0, FENSTER_KEYCODE_MAX);
	memset(plat_keyprev, 0, FENSTER_KEYCODE_MAX);
	memset(plat_mousedown, 0, FENSTER_MOUSEBTN_MAX);
	memset(plat_mouseprev, 0, FENSTER_MOUSEBTN_MAX);
	plat_moddown = 0;
	plat_modprev = 0;

	plat_mouse_x = 0;
	plat_mouse_y = 0;
}

void plat_input_update(void)
{
	memcpy(plat_keyprev, plat_keydown, FENSTER_KEYCODE_MAX);
	memcpy(plat_mouseprev, plat_mousedown, FENSTER_MOUSEBTN_MAX);
	plat_modprev = plat_moddown;

	XEvent ev;
	unsigned int i;
	int k;
	int ispress;
	while (XPending(f__dpy)) {
		XNextEvent(f__dpy, &ev);
		switch (ev.type) {
		case ButtonPress:
			plat_mouse_x = ev.xbutton.x;  plat_mouse_y = ev.xbutton.y;
			plat_mousedown[ev.xbutton.button % FENSTER_MOUSEBTN_MAX] = 1;
			plat_event_mousepress(ev.xbutton.x, ev.xbutton.y, ev.xbutton.button);
		case ButtonRelease:
			plat_mouse_x = ev.xbutton.x;  plat_mouse_y = ev.xbutton.y;
			plat_mousedown[ev.xbutton.button % FENSTER_MOUSEBTN_MAX] = 0;
			plat_event_mouserelease(ev.xbutton.x, ev.xbutton.y, ev.xbutton.button);
			break;
		case MotionNotify:
			plat_mouse_x = ev.xmotion.x;  plat_mouse_y = ev.xmotion.y;
			plat_event_mousemove(ev.xmotion.x, ev.xmotion.y);
			break;
		case KeyPress:
		case KeyRelease:
			ispress = ev.type == KeyPress;
			k = XkbKeycodeToKeysym(f__dpy, ev.xkey.keycode, 0, 0);

			// suppress keyrepeat (https://stackoverflow.com/a/3222566)
			// TODO: separate 'keyrepeat' event
			if (!ispress && XEventsQueued(f__dpy, QueuedAlready /* QueuedAfterReading */)) {
				XEvent nev;
				XPeekEvent(f__dpy, &nev);

				if (nev.type == KeyPress && nev.xkey.time == ev.xkey.time && nev.xkey.keycode == ev.xkey.keycode) {
					XNextEvent(f__dpy, &ev);
					break;
				}
			}

			#define modkeycheck(mask, key1, key2, shift) ( ( ((ev.xkey.state & mask) || ((k == key1 || k == key2) && ispress)) && !((k == key1 || k == key2) && !ispress) ) << shift )
				plat_moddown
					= modkeycheck(ControlMask, XK_Control_L, XK_Control_R, 0)
					| modkeycheck(ShiftMask,   XK_Shift_L,   XK_Shift_R,   1)
					| modkeycheck(Mod1Mask,    XK_Alt_L,     XK_Alt_R,     2)
					| modkeycheck(Mod4Mask,    XK_Meta_L,    XK_Meta_R,    3);
			#undef modkeycheck

			for (i = 0; i < FENSTER_SCANCODE_MAX; i += 2) {
				if (FENSTER_SCANCODES[i] == k) { // TODO see if a hashmap is faster here. :^]
					int text_key = XkbKeycodeToKeysym(f__dpy, ev.xkey.keycode, 0, ev.xkey.state & ShiftMask ? 1 : 0);
					int plat_key = FENSTER_SCANCODES[i + 1];

					if (ispress) {
						plat_event_keypress(plat_key, text_key);
					} else if (ev.type == KeyRelease) {
						plat_event_keyrelease(plat_key, text_key);
					}

					plat_keydown[plat_key] = ispress;
					
					break;
				}
			}
			break;
		}
	}
}

void plat_mouse_write(int x, int y)
{
	XWarpPointer(f__dpy, None, f__w, 0, 0, 0, 0, x, y);

	// assuming here that X and Y are valid values...
	// if they are not, possibility that f->[xy] and actual mouse x/y are mismatching.
	plat_mouse_x = x;
	plat_mouse_y = y;
}

void plat_mouse_show(int visible)
{
	if (visible) {
		XDefineCursor(f__dpy, f__w, None);
	} else {
		static Cursor emptyCursor;

		if (!emptyCursor) {
			XColor color  = { 0 };
			const char data[] = { 0 };

			Pixmap pixmap = XCreateBitmapFromData(f__dpy, f__w, data, 1, 1);
			emptyCursor = XCreatePixmapCursor(f__dpy, pixmap, pixmap, &color, &color, 0, 0);

			XFreePixmap(f__dpy, pixmap);
		}

		XDefineCursor(f__dpy, f__w, emptyCursor);
	}
}
