// headers
#include "p2d/platform.h"
#include "p2d/engine.h"
//#include <SDL2/SDL.h> // shouldn't be needed if included in sdl2.h
#include "sdl2.h"

int plat_exiting = 0;

// oops
static void oops(const char *s)
{
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", s, NULL);
	exit(1);
}

/*
**  Timer Routines. Clocked at 120(!) ticks per second.
*/
unsigned plat_mstotal;

void plat_time_init()
{
	plat_mstotal = SDL_GetTicks();
}

unsigned plat_time_update()
{
	return (plat_mstotal = SDL_GetTicks());
}

/*
**  input event hooks
*/
// Prototypes for platform event hooks.
// Can be extended or overridden by platform implementation.

// TODO put these in platform.h and #define _PLAT_IMPL_ or something in this flie
//      to avoid boilerplate in every platform file?
void plat_default_event_keypress(int plat_key, int text_key) { }
void plat_default_event_keyrelease(int plat_key, int text_key) { }
void plat_default_event_mousepress(int x, int y, int button) { }
void plat_default_event_mouserelease(int x, int y, int button) { }
void plat_default_event_mousemove(int x, int y) { }

plat_fn_keypress_t plat_event_keypress;
plat_fn_keyrelease_t plat_event_keyrelease;
plat_fn_mousepress_t plat_event_mousepress;
plat_fn_mouserelease_t plat_event_mouserelease;
plat_fn_mousemove_t plat_event_mousemove;

/*
**  video and rendering
*/
unsigned plat_screen_w = 640;
unsigned plat_screen_h = 480;

uint32_t *plat_framebuffer;

uint8_t plat_palettes[PLAT_PALETTE_MAX][256][3];
uint8_t plat_palette[256][3];

// sdl2 stuff
SDL_Window *window;
SDL_Renderer *renderer;
SDL_Texture *texture;

static char* window_title;

void plat_palette_install(uint8_t palette_i, uint8_t newpalette[256][3])
{
	memcpy(plat_palettes[palette_i], newpalette, sizeof(uint8_t[256][3]));
}

void plat_palette_set(uint8_t palette_i)
{
	memcpy(plat_palette, plat_palettes[palette_i], sizeof(uint8_t[256][3]));
}

void plat_window_update()
{
	SDL_UpdateTexture(texture, NULL, plat_framebuffer, plat_screen_w * sizeof(uint32_t));
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

void plat_video_update8(uint8_t *src)
{
	int x, y;
	BYTE *colour;

	for (y = 0; y < plat_screen_h; y++)
	{
		for (x = 0; x < plat_screen_w; x++)
		{
			colour = plat_palette[src[(y * plat_screen_w) + x]];
			plat_framebuffer[y*plat_screen_w + x] = colour[0]*65536 + colour[1]*256 + colour[2];
		}
	}

	// blit
	SDL_UpdateTexture(texture, NULL, plat_framebuffer, plat_screen_w * sizeof(uint32_t));
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, NULL, NULL);
	SDL_RenderPresent(renderer);
}

void plat_video_init(unsigned width, unsigned height, const char* title)
{
	// init sdl (TODO shouldn't really be in video_init)
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) != 0)
		oops("Failed to initialize SDL2");

	plat_exiting = 0; // neither should this

	plat_screen_w = width;
	plat_screen_h = height;

	/* create window */

	// window title silliness...
	if (!title) {
		title = "portal2e";
	}
	unsigned memlen = strlen(title) + 1;
	assert(!window_title);
	window_title = malloc(memlen);
	memcpy(window_title, title, memlen);

	window = SDL_CreateWindow(
		window_title,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		plat_screen_w,
		plat_screen_h,
		SDL_WINDOW_RESIZABLE
	);

	if (window == NULL)
		oops("failed to open SDL_Window");

	/* create screen renderer */
	renderer = SDL_CreateRenderer(
		window,
		-1,
		SDL_RENDERER_SOFTWARE | SDL_RENDERER_PRESENTVSYNC
	);

	if (renderer == NULL)
		oops("failed to open SDL_Renderer");

	/* create screen texture */
	texture = SDL_CreateTexture(
		renderer,
		SDL_PIXELFORMAT_ARGB8888,
		SDL_TEXTUREACCESS_STREAMING,
		plat_screen_w,
		plat_screen_h
	);

	if (texture == NULL)
		oops("failed to open SDL_Texture");

	// pixels
	plat_framebuffer = malloc(sizeof(uint32_t) * plat_screen_w * plat_screen_h);
}

void plat_video_deinit()
{
	if (texture) SDL_DestroyTexture(texture);
	if (renderer) SDL_DestroyRenderer(renderer);
	if (window) SDL_DestroyWindow(window);
	if (plat_framebuffer) free(plat_framebuffer);
	SDL_Quit();
}

/*
**  input (keyboard and mouse)
*/

#define KEYCODE_MAX 128
#define MOUSEBTN_MAX 32

uint8_t plat_keydown[KEYCODE_MAX];
uint8_t plat_keyprev[KEYCODE_MAX];
uint32_t plat_moddown;
uint32_t plat_modprev;
uint8_t plat_mousedown[MOUSEBTN_MAX];
uint8_t plat_mouseprev[MOUSEBTN_MAX];
int plat_mouse_x;
int plat_mouse_y;

void plat_input_init()
{	// TODO perhaps params could have e.g. joysticks etc
	memset(plat_keydown, 0, KEYCODE_MAX);
	memset(plat_keyprev, 0, KEYCODE_MAX);
	memset(plat_mousedown, 0, MOUSEBTN_MAX);
	memset(plat_mouseprev, 0, MOUSEBTN_MAX);
	plat_moddown = 0;
	plat_modprev = 0;

	plat_mouse_x = 0;
	plat_mouse_y = 0;
}

// semi-BODGE: this is as far as we need to go to handle all 'plat' keycodes
#define MAX_SDL_SCANCODE SDL_SCANCODE_MODE

static int sdl2_scancodes[MAX_SDL_SCANCODE] = {
	[SDL_SCANCODE_A] = 'A',
	[SDL_SCANCODE_B] = 'B',
	[SDL_SCANCODE_C] = 'C',
	[SDL_SCANCODE_D] = 'D',
	[SDL_SCANCODE_E] = 'E',
	[SDL_SCANCODE_F] = 'F',
	[SDL_SCANCODE_G] = 'G',
	[SDL_SCANCODE_H] = 'H',
	[SDL_SCANCODE_I] = 'I',
	[SDL_SCANCODE_J] = 'J',
	[SDL_SCANCODE_K] = 'K',
	[SDL_SCANCODE_L] = 'L',
	[SDL_SCANCODE_M] = 'M',
	[SDL_SCANCODE_N] = 'N',
	[SDL_SCANCODE_O] = 'O',
	[SDL_SCANCODE_P] = 'P',
	[SDL_SCANCODE_Q] = 'Q',
	[SDL_SCANCODE_R] = 'R',
	[SDL_SCANCODE_S] = 'S',
	[SDL_SCANCODE_T] = 'T',
	[SDL_SCANCODE_U] = 'U',
	[SDL_SCANCODE_V] = 'V',
	[SDL_SCANCODE_W] = 'W',
	[SDL_SCANCODE_X] = 'X',
	[SDL_SCANCODE_Y] = 'Y',
	[SDL_SCANCODE_Z] = 'Z',
	[SDL_SCANCODE_1] = '1',
	[SDL_SCANCODE_2] = '2',
	[SDL_SCANCODE_3] = '3',
	[SDL_SCANCODE_4] = '4',
	[SDL_SCANCODE_5] = '5',
	[SDL_SCANCODE_6] = '6',
	[SDL_SCANCODE_7] = '7',
	[SDL_SCANCODE_8] = '8',
	[SDL_SCANCODE_9] = '9',
	[SDL_SCANCODE_0] = '0',

	[SDL_SCANCODE_MINUS]        = '-',
	[SDL_SCANCODE_EQUALS]       = '=',
	[SDL_SCANCODE_LEFTBRACKET]  = '[',
	[SDL_SCANCODE_RIGHTBRACKET] = ']',
	[SDL_SCANCODE_BACKSLASH]    = '\\',
	[SDL_SCANCODE_NONUSHASH]    = '#',

	[SDL_SCANCODE_SEMICOLON]    = ';',
	[SDL_SCANCODE_APOSTROPHE]   = '\'',
	[SDL_SCANCODE_GRAVE]        = '`',
	[SDL_SCANCODE_COMMA]        = ',',
	[SDL_SCANCODE_PERIOD]       = '.',
	[SDL_SCANCODE_SLASH]        = '/',

	[SDL_SCANCODE_RETURN] = KB_ENTER,
	[SDL_SCANCODE_ESCAPE] = KB_ESC,
	[SDL_SCANCODE_BACKSPACE] = KB_BACKSPACE,
	[SDL_SCANCODE_TAB] = '\t',
	[SDL_SCANCODE_SPACE] = ' ',

	[SDL_SCANCODE_RIGHT] = KB_RTARROW,
	[SDL_SCANCODE_LEFT] = KB_LTARROW,
	[SDL_SCANCODE_DOWN] = KB_DNARROW,
	[SDL_SCANCODE_UP] = KB_UPARROW,

	[SDL_SCANCODE_LCTRL] = KB_CTRL,
	[SDL_SCANCODE_LSHIFT] = KB_SHIFT,
	[SDL_SCANCODE_LALT] = KB_ALT,
	[SDL_SCANCODE_LGUI] = KB_META,
	[SDL_SCANCODE_RCTRL] = KB_CTRL,
	[SDL_SCANCODE_RSHIFT] = KB_SHIFT,
	[SDL_SCANCODE_RALT] = KB_ALT,
	[SDL_SCANCODE_RGUI] = KB_META,
};

// SUPER BODGE!
// Because the genius SDL devs thought it was a good idea to remove the
// SDL_Keysym `unicode` member in SDL2, you can no longer get the actual
// resulting character from an SDL_KEYDOWN/KEYUP event.  You are instead forced
// to check for SDL_TEXTENTRY events, meaning it's impossible to handle both
// game keycodes and text characters in a single `plat_event_keypress/release`
// function call without resorting to bongwater ultra-fragile trickery in the
// event loop.  For simplicity's sake, instead we're using a map of characters
// to use if the Shift key is held down - of course this means it sadly only
// supports the US keyboard layout (call me a traitor to my realm if you will,
// but I can't be arsed handling the non-ASCII '£' and '¬' characters right now,
// I'm sick enough of SDL2 as it is at this point and just want to get it working).
static int sdl2_shiftmap[128] = {
	['a'] = 'A',
	['b'] = 'B',
	['c'] = 'C',
	['d'] = 'D',
	['e'] = 'E',
	['f'] = 'F',
	['g'] = 'G',
	['h'] = 'H',
	['i'] = 'I',
	['j'] = 'J',
	['k'] = 'K',
	['l'] = 'L',
	['m'] = 'M',
	['n'] = 'N',
	['o'] = 'O',
	['p'] = 'P',
	['q'] = 'Q',
	['r'] = 'R',
	['s'] = 'S',
	['t'] = 'T',
	['u'] = 'U',
	['v'] = 'V',
	['w'] = 'W',
	['x'] = 'X',
	['y'] = 'Y',
	['z'] = 'Z',

	['1'] = '!',
	['2'] = '@',
	['3'] = '#',
	['4'] = '$',
	['5'] = '%',
	['6'] = '^',
	['7'] = '&',
	['8'] = '*',
	['9'] = '(',
	['0'] = ')',

	['-'] = '_',
	['='] = '+',
	['['] = '{',
	[']'] = '}',
	[';'] = ':',
	['\''] = '"',
	['\\'] = '|',
	[','] = '<',
	['.'] = '>',
	['/'] = '?',
	['`'] = '~',
	['#'] = '~',
};

void plat_input_update()
{
	SDL_Event event;
	int plat_key, text_key, ispress;

	// copy keys
	memcpy(plat_keyprev, plat_keydown, KEYCODE_MAX);
	memcpy(plat_mouseprev, plat_mousedown, MOUSEBTN_MAX);
	plat_modprev = plat_moddown;

	// poll events
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_KEYDOWN:
			case SDL_KEYUP:
				if (event.key.keysym.scancode >= 0 && event.key.keysym.scancode < MAX_SDL_SCANCODE && (plat_key = sdl2_scancodes[event.key.keysym.scancode])) {
					ispress = event.type == SDL_KEYDOWN;

					// suppress keyrepeat
					// TODO: separate 'keyrepeat' event
					if (event.key.repeat) break;

					#define modkeycheck(mask, key1, key2, shift) ( ( ((event.key.keysym.mod & mask) || ((plat_key == key1 || plat_key == key2) && ispress)) && !((plat_key == key1 || plat_key == key2) && !ispress) ) << shift )
						plat_moddown
							= modkeycheck(KMOD_CTRL,  SDL_SCANCODE_LCTRL,  SDL_SCANCODE_RCTRL,  0)
							| modkeycheck(KMOD_SHIFT, SDL_SCANCODE_LSHIFT, SDL_SCANCODE_RSHIFT, 1)
							| modkeycheck(KMOD_ALT,   SDL_SCANCODE_LALT,   SDL_SCANCODE_RALT,   2)
							| modkeycheck(KMOD_GUI,   SDL_SCANCODE_LGUI,   SDL_SCANCODE_RGUI,   3);
					#undef modkeycheck

					if ((text_key = event.key.keysym.sym) & ~0x7f) {
						text_key = 0;
					} else {
						// BODGE - see above
						if (MOD_DOWN(MOD_SHIFT)) {
							text_key = sdl2_shiftmap[text_key];
						}
					}

					if (ispress) {
						plat_event_keypress(plat_key, text_key);
					} else {
						plat_event_keyrelease(plat_key, text_key);
					}

					plat_keydown[plat_key] = ispress;
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (event.button.button >= 0 && event.button.button < MOUSEBTN_MAX) {
					plat_mousedown[event.button.button] = 1;
				}
				plat_mouse_x = event.button.x;
				plat_mouse_y = event.button.y;
				plat_event_mousepress(event.button.x, event.button.y, event.button.button);
				break;

			case SDL_MOUSEBUTTONUP:
				if (event.button.button >= 0 && event.button.button < MOUSEBTN_MAX) {
					plat_mousedown[event.button.button] = 0;
				}
				plat_mouse_x = event.button.x;
				plat_mouse_y = event.button.y;
				plat_event_mouserelease(event.button.x, event.button.y, event.button.button);
				break;

			case SDL_MOUSEMOTION:
				plat_mouse_x = event.motion.x;
				plat_mouse_y = event.motion.y;
				plat_event_mousemove(event.motion.x, event.motion.y);
				break;

			case SDL_QUIT:
				plat_exiting = 1;
				break;
		}
	}
}

// TODO: this is currently used for 'mouse grab' in 3d/game mode.
// Ideally we'd use SDL_SetRelativeMouseMode for this purpose and have
// mousegrab be a plat-controlled variable as originally intended (i.e.
// plat_mousegrab = 1 etc) but this will require careful consideration for
// platforms such as Xlib which doesn't support relative mouse so you have to
// warp the cursor to the center every frame.  Which of course then generates
// a mouse move event which we'll have to add special code in the event loop
// to ignore.  Argh.
void plat_mouse_write(int x, int y)
{
	SDL_WarpMouseInWindow(window, x, y);
}

void plat_mouse_show(int visible)
{
	SDL_ShowCursor(visible ? SDL_ENABLE : SDL_DISABLE);
}

// This is the function that we need but that Xlib won't let us have. Sob.
/*void plat_mouse_grab(int grab)
{
	SDL_SetRelativeMouseMode(grab ? SDL_TRUE : SDL_FALSE);
}*/
