#include "hashmap.h"
#include <string.h>

static char *strclone(const char *s)
{
	size_t len = strlen(s) + 1;
	char *ns = malloc(len);
	if (!ns) return NULL;
	memcpy(ns, s, len);
	return ns;
}

/*
**  hashmap  *******************************************************************
*/

static unsigned long hm_hash(const char *key)
{
	unsigned long hash = 5381;
	int c;
	while ((c = *key++)) hash = ((hash << 5) + hash) + c;
	return hash;
}

void hm_init(hashmap_t* hm)
{
	memset(hm, 0, sizeof(hashmap_t));
	// TODO some sort of preallocation?
}

void hm_destroy(hashmap_t* hm)
{
	size_t i, j;
	for (i = 0; i < HASHMAP_CELLS; i++) {
		for (j = 0; j < hm->cell[i].numentries; j++) {
			free(hm->cell[i].entry[j].key); // NOTE - does not free values!
		}

		free(hm->cell[i].entry);
	}
}

void hm_put(hashmap_t* hm, const char *key, void* value)
{
	hashcell_t* cell = hm->cell + (hm_hash(key) & HASHMAP_CELLMASK);

	size_t i;
	for (i = 0; i < cell->numentries; i++) {
		if (!strcmp(key, cell->entry[i].key)) {
			cell->entry[i].value = value;
			return;
		}
	}

	cell->entry = realloc(cell->entry, sizeof(hashentry_t) * (cell->numentries + 1));
	cell->entry[cell->numentries].key = strclone(key);
	cell->entry[cell->numentries].value = value;

	cell->numentries++;
}

void* hm_get(hashmap_t* hm, const char *key)
{
	hashcell_t* cell = hm->cell + (hm_hash(key) & HASHMAP_CELLMASK);
	size_t i;
	for (i = 0; i < cell->numentries; i++) {
		if (!strcmp(key, cell->entry[i].key)) {
			return cell->entry[i].value;
		}
	}
	return NULL;
}

int hm_has(hashmap_t* hm, const char *key)
{
	hashcell_t* cell = hm->cell + (hm_hash(key) & HASHMAP_CELLMASK);
	size_t i;
	for (i = 0; i < cell->numentries; i++) {
		if (!strcmp(key, cell->entry[i].key)) {
			return 1;
		}
	}
	return 0;
}

hashiterator_t hm_iter(void)
{
	hashiterator_t iter;
	iter.cell_i = 0;
	iter.entry_i = 0;
	return iter;
}

int hm_next(hashmap_t* hm, hashiterator_t* iter)
{
	while (iter->entry_i >= hm->cell[iter->cell_i].numentries) {
		if (iter->cell_i >= HASHMAP_CELLS - 1) {
			return 0;
		}

		iter->cell_i++;
		iter->entry_i = 0;
	}

	iter->key = hm->cell[iter->cell_i].entry[iter->entry_i++].key;

	return 1;
}

/*
**  hashimap: hashmap with integer keys  ***************************************
*/

/*static uint8_t hmi_hash(uint32_t key)
{
	return key & 0xFF; // TODO: only at all useful for stuff expected to increment by 1.
}*/

// https://gist.github.com/badboy/6267743#robert-jenkins-32-bit-integer-hash-function
static uint32_t hmi_hash(uint32_t key)
{
   key = (key+0x7ed55d16) + (key<<12);
   key = (key^0xc761c23c) ^ (key>>19);
   key = (key+0x165667b1) + (key<<5);
   key = (key+0xd3a2646c) ^ (key<<9);
   key = (key+0xfd7046c5) + (key<<3);
   key = (key^0xb55a4f09) ^ (key>>16);
   return key;
}

void hmi_init(hashimap_t* hm)
{
	memset(hm, 0, sizeof(hashimap_t));
	// TODO some sort of preallocation?
}

void hmi_destroy(hashimap_t* hm)
{
	size_t i/*, j*/;
	for (i = 0; i < HASHMAP_CELLS; i++) {
		// Keys do not need freeing because they are static integers.
		/*
		for (j = 0; j < hm->cell[i].numentries; j++) {
			free(hm->cell[i].entry[j].key);
		}
		*/

		free(hm->cell[i].entry);
	}
}

void hmi_reinit(hashimap_t* hm)
{
	hmi_destroy(hm);
	hmi_init(hm);
}

void hmi_put(hashimap_t* hm, uint32_t key, void* value)
{
	hashicell_t* cell = hm->cell + (hmi_hash(key) & HASHMAP_CELLMASK);

	size_t i;
	for (i = 0; i < cell->numentries; i++) {
		if (cell->entry[i].key == key) {
			cell->entry[i].value = value;
			return;
		}
	}

	cell->entry = realloc(cell->entry, sizeof(hashientry_t) * (cell->numentries + 1));
	cell->entry[cell->numentries].key = key;
	cell->entry[cell->numentries].value = value;

	if (!cell->numentries) {
		hm->cellids[hm->numcellids] = hmi_hash(key) & HASHMAP_CELLMASK;
		hm->numcellids++;
	}

	cell->numentries++;
}

void* hmi_get(hashimap_t* hm, uint32_t key)
{
	hashicell_t* cell = hm->cell + (hmi_hash(key) & HASHMAP_CELLMASK);
	size_t i;

	switch (cell->numentries) {
		case 0:
			return NULL;

		case 1:
			if (cell->entry[0].key == key) {
				return cell->entry[0].value;
			}
			break;

		default:
			for (i = 0; i < cell->numentries; i++) {
				if (cell->entry[i].key == key) {
					return cell->entry[i].value;
				}
			}
			break;
	}

	return NULL;
}

int hmi_has(hashimap_t* hm, uint32_t key)
{
	hashicell_t* cell = hm->cell + (hmi_hash(key) & HASHMAP_CELLMASK);
	size_t i;

	for (i = 0; i < cell->numentries; i++) {
		if (cell->entry[i].key == key) {
			return 1;
		}
	}

	return 0;
}

hashiiterator_t hmi_iter(void)
{
	hashiiterator_t iter;
	iter.cellid_i = 0;
	iter.entry_i = 0;
	return iter;
}

int hmi_next(hashimap_t* hm, hashiiterator_t* iter)
{
	int cellid;

	hmi_next_top:

	if (iter->cellid_i >= hm->numcellids) {
		return 0;
	}

	cellid = hm->cellids[iter->cellid_i];

	if (iter->entry_i >= hm->cell[cellid].numentries) {
		iter->cellid_i++;
		iter->entry_i = 0;
		goto hmi_next_top;
	}

	iter->key = hm->cell[cellid].entry[iter->entry_i++].key;

	return 1;
}

// TODO multi-level foreach :^)
#define HMI_FOREACH(HM_P, TYPE, CODE) do { hashiterator_t _iter_ = hmi_iter(); while (hmi_next((HM_P), &_iter_)) { uint32_t key = _iter_.key; TYPE value = hmi_get((HM_P), _iter_.key); CODE; } } while (0)
