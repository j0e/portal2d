#ifndef _HASHMAP_H_
#define _HASHMAP_H_

#include <stdint.h>
#include <stdlib.h>

#define HASHMAP_CELLS 256
#define HASHMAP_CELLMASK 0xFF

typedef struct hashentry_t {
    char *key;
    void* value;
} hashentry_t;

typedef struct hashcell_t {
    hashentry_t *entry;
    size_t numentries;
} hashcell_t;

typedef struct hashmap_t {
    hashcell_t cell[HASHMAP_CELLS];
    // TODO 'cellids' array from hashimap_t below, and compare speed of each when iterating.
} hashmap_t;

typedef struct hashiterator_t {
    uint8_t cell_i;
    uint32_t entry_i;
    char* key; // the current key of the iterator.
} hashiterator_t;

void hm_init(hashmap_t* hm);
void hm_destroy(hashmap_t* hm);
void hm_put(hashmap_t* hm, const char *key, void* value);
void* hm_get(hashmap_t* hm, const char *key);
int hm_has(hashmap_t* hm, const char *key);
hashiterator_t hm_iter(void);
int hm_next(hashmap_t* hm, hashiterator_t* iter);

// hashimap

typedef struct hashientry_t {
    uint32_t key;
    void* value;
} hashientry_t;

typedef struct hashicell_t {
    hashientry_t *entry;
    size_t numentries;
} hashicell_t;

typedef struct hashimap_t {
    hashicell_t cell[HASHMAP_CELLS];
    // Below:
    // Experimental feature to speed up iterating by containing only the occupied cells.
    // This has the weird side-effect of earliest occupied cells being executed first.
    // Will perhaps need to be checked for empty when hmi_unset() is implemented.
    // If unsetting a key causes that cell's number of entries to be 0, it will be effectively skipped over when iterating.
    // The time taken to do so will be trivial, so there's no need for potentially error-prone removal of the cell ID and shifting down all the subsequent values.
    uint8_t cellids[256];
    size_t numcellids;
} hashimap_t;

typedef struct hashiiterator_t {
    uint8_t cellid_i;
    uint32_t entry_i;
    uint32_t key; // the current key of the iterator.
} hashiiterator_t;

void hmi_init(hashimap_t* hm);
void hmi_destroy(hashimap_t* hm);
void hmi_reinit(hashimap_t* hm);
void hmi_put(hashimap_t* hm, uint32_t key, void* value);
void* hmi_get(hashimap_t* hm, uint32_t key);
int hmi_has(hashimap_t* hm, uint32_t key);
hashiiterator_t hmi_iter(void);
int hmi_next(hashimap_t* hm, hashiiterator_t* iter);

#endif
