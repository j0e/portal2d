#ifndef FEX_H
#define FEX_H

#include "include/fe.h"

// convenience aliases
#define fe_true(ctx) fe_bool(ctx, true)
#define fe_false(ctx) fe_bool(ctx, false)
#define fe_null(ctx) fe_bool(ctx, 0) // TODO change to fe_nil? :^)

fe_Object* fex_read_string(fe_Context *ctx, const char *str);
fe_Object* fex_do_string(fe_Context *ctx, const char *str);
fe_Object* fex_do_file(fe_Context *ctx, const char *filename);

void fex_errorf(fe_Context* ctx, const char *fmt, ...);

#endif
