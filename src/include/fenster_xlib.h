#ifndef FENSTER_H
#define FENSTER_H

#define _DEFAULT_SOURCE 1
#include <X11/XKBlib.h>
#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <time.h>

#include <stdint.h>
#include <stdlib.h>

struct fenster {
  const char *title;
  const int width;
  const int height;
  uint32_t *buf;
  int keys[256]; /* keys are mostly ASCII, but arrows are 17..20 */
  int mod;       /* mod is 4 bits mask, ctrl=1, shift=2, alt=4, meta=8 */
  int x;
  int y;
  int mouse;
  Display *dpy;
  Window w;
  GC gc;
  XImage *img;
};

extern int plat_mouse_x; // BODGE
extern int plat_mouse_y; // BODGE

// MOAR BODGE
extern void (*plat_event_keypress)(int plat_key, int text_key);
extern void (*plat_event_keyrelease)(int plat_key, int text_key);
extern void (*plat_event_mousepress)(int x, int y, int button);
extern void (*plat_event_mouserelease)(int x, int y, int button);
extern void (*plat_event_mousemove)(int x, int y);

#ifndef FENSTER_API
#define FENSTER_API extern
#endif
FENSTER_API int fenster_open(struct fenster *f);
FENSTER_API int fenster_loop(struct fenster *f);
FENSTER_API void fenster_close(struct fenster *f);
FENSTER_API void fenster_sleep(int64_t ms);
FENSTER_API int64_t fenster_time();
FENSTER_API void fenster_setmouse(struct fenster *f, int x, int y);
FENSTER_API void fenster_showmouse(struct fenster *f, int show);
FENSTER_API void fenster_input_update();
FENSTER_API void fenster_video_update();
#define fenster_pixel(f, x, y) ((f)->buf[((y) * (f)->width) + (x)])

#ifndef FENSTER_HEADER
// clang-format off
#define FENSTER_KEYCODE_MAX 140
static int FENSTER_KEYCODES[FENSTER_KEYCODE_MAX] = {XK_BackSpace,8,XK_Delete,127,XK_Down,18,XK_End,5,XK_Escape,27,XK_Home,2,XK_Insert,26,XK_Left,20,XK_Page_Down,4,XK_Page_Up,3,XK_Return,10,XK_Right,19,XK_Tab,9,XK_Up,17,XK_apostrophe,39,XK_backslash,92,XK_bracketleft,91,XK_bracketright,93,XK_comma,44,XK_equal,61,XK_grave,96,XK_minus,45,XK_period,46,XK_semicolon,59,XK_slash,47,XK_space,32,XK_a,65,XK_b,66,XK_c,67,XK_d,68,XK_e,69,XK_f,70,XK_g,71,XK_h,72,XK_i,73,XK_j,74,XK_k,75,XK_l,76,XK_m,77,XK_n,78,XK_o,79,XK_p,80,XK_q,81,XK_r,82,XK_s,83,XK_t,84,XK_u,85,XK_v,86,XK_w,87,XK_x,88,XK_y,89,XK_z,90,XK_0,48,XK_1,49,XK_2,50,XK_3,51,XK_4,52,XK_5,53,XK_6,54,XK_7,55,XK_8,56,XK_9,57,XK_Control_L,21,XK_Control_R,21,XK_Shift_L,22,XK_Shift_R,22,XK_Alt_L,23,XK_Alt_R,23,XK_Meta_L,24,XK_Meta_R,24};
// clang-format on

static Cursor nullCursor(Display *dpy, Drawable dw) // BODGE
{
    XColor color  = { 0 };
    const char data[] = { 0 };

    Pixmap pixmap = XCreateBitmapFromData(dpy, dw, data, 1, 1);
    Cursor cursor = XCreatePixmapCursor(dpy, pixmap, pixmap, &color, &color, 0, 0);

    XFreePixmap(dpy, pixmap);

    return cursor;
}
Cursor emptyCursor; // BODGE

FENSTER_API int fenster_open(struct fenster *f)
{
  f->dpy = XOpenDisplay(NULL);
  int screen = DefaultScreen(f->dpy);
  f->w = XCreateSimpleWindow(f->dpy, RootWindow(f->dpy, screen), 0, 0, f->width,
                             f->height, 0, BlackPixel(f->dpy, screen),
                             WhitePixel(f->dpy, screen));
  f->gc = XCreateGC(f->dpy, f->w, 0, 0);
  XSelectInput(f->dpy, f->w,
               ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask |
                   ButtonReleaseMask | PointerMotionMask);
  XStoreName(f->dpy, f->w, f->title);
  XMapWindow(f->dpy, f->w);
  XSync(f->dpy, f->w);
  f->img = XCreateImage(f->dpy, DefaultVisual(f->dpy, 0), 24, ZPixmap, 0,
                        (char *)f->buf, f->width, f->height, 32, 0);

  emptyCursor = nullCursor(f->dpy, f->w);

  return 0;
}

FENSTER_API void fenster_close(struct fenster *f)
{
  XCloseDisplay(f->dpy);
}

FENSTER_API void fenster_setmouse(struct fenster *f, int x, int y)
{
  XWarpPointer(f->dpy, None, f->w, 0, 0, 0, 0, x, y);
  // assuming here that X and Y are valid values...
  // if they are not, possibility that f->[xy] and actual mouse x/y are mismatching.
  f->x = x;
  f->y = y;
}

FENSTER_API void fenster_showmouse(struct fenster *f, int show)
{
  if (show) {
    XDefineCursor(f->dpy, f->w, None);
  } else {
    XDefineCursor(f->dpy, f->w, emptyCursor);
  }
}

FENSTER_API void fenster_input_update(struct fenster *f)
{
  XEvent ev;
  unsigned int i;
  int k;
  int ispress;
  while (XPending(f->dpy)) {
    XNextEvent(f->dpy, &ev);
    switch (ev.type) {
    case ButtonPress:
      // TODO! other mouse buttons!
      plat_mouse_x = ev.xbutton.x, plat_mouse_y = ev.xbutton.y;
      f->mouse = (ev.type == ButtonPress);
      plat_event_mousepress(ev.xbutton.x, ev.xbutton.y, ev.xbutton.button);
    case ButtonRelease:
      // TODO! other mouse buttons!
      plat_mouse_x = ev.xbutton.x, plat_mouse_y = ev.xbutton.y;
      f->mouse = (ev.type == ButtonPress);
      plat_event_mouserelease(ev.xbutton.x, ev.xbutton.y, 1);
      break;
    case MotionNotify:
      plat_mouse_x = ev.xmotion.x, plat_mouse_y = ev.xmotion.y;
      f->x = ev.xmotion.x, f->y = ev.xmotion.y;
      plat_event_mousemove(ev.xmotion.x, ev.xmotion.y);
      break;
    case KeyPress:
    case KeyRelease: {
      ispress = ev.type == KeyPress;
      k = XkbKeycodeToKeysym(f->dpy, ev.xkey.keycode, 0, 0);

      // suppress keyrepeat (https://stackoverflow.com/a/3222566)
      // TODO: separate 'keyrepeat' event
      if (!ispress && XEventsQueued(f->dpy, QueuedAlready /* QueuedAfterReading */)) {
        XEvent nev;
        XPeekEvent(f->dpy, &nev);

        if (nev.type == KeyPress && nev.xkey.time == ev.xkey.time && nev.xkey.keycode == ev.xkey.keycode) {
          XNextEvent(f->dpy, &ev);
          break;
        }
      }

      #define modkeycheck(mask, key1, key2, shift) ( ( ((ev.xkey.state & mask) || ((k == key1 || k == key2) && ispress)) && !((k == key1 || k == key2) && !ispress) ) << shift )
        f->mod
          = modkeycheck(ControlMask, XK_Control_L, XK_Control_R, 0)
          | modkeycheck(ShiftMask,   XK_Shift_L,   XK_Shift_R,   1)
          | modkeycheck(Mod1Mask,    XK_Alt_L,     XK_Alt_R,     2)
          | modkeycheck(Mod4Mask,    XK_Meta_L,    XK_Meta_R,    3);
      #undef modkeycheck

      for (i = 0; i < FENSTER_KEYCODE_MAX; i += 2) { // TODO change to 140 etc
        if (FENSTER_KEYCODES[i] == k) { // TODO see if a hashmap is faster here. :^]
          int text_key = XkbKeycodeToKeysym(f->dpy, ev.xkey.keycode, 0, ev.xkey.state & ShiftMask ? 1 : 0);
          int plat_key = FENSTER_KEYCODES[i + 1];

          if (ispress) {
            plat_event_keypress(plat_key, text_key);
          } else if (ev.type == KeyRelease) {
            plat_event_keyrelease(plat_key, text_key);
          }

          f->keys[plat_key] = ispress;
          
          break;
        }
      }
    } break;
    }
  }
}

FENSTER_API void fenster_video_update(struct fenster *f)
{
  XPutImage(f->dpy, f->w, f->gc, f->img, 0, 0, 0, 0, f->width, f->height);
  XFlush(f->dpy);
}

FENSTER_API void fenster_sleep(int64_t ms)
{
  struct timespec ts;
  ts.tv_sec = ms / 1000;
  ts.tv_nsec = (ms % 1000) * 1000000;
  nanosleep(&ts, NULL);
}

FENSTER_API int64_t fenster_time()
{
  struct timespec time;
  clock_gettime(CLOCK_REALTIME, &time);
  return time.tv_sec * 1000 + (time.tv_nsec / 1000000);
}

#endif /* !FENSTER_HEADER */
#endif /* FENSTER_H */
