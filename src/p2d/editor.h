#ifndef __EDITOR_H__
#define __EDITOR_H__

#include "level.h"
#include "palette.h"
#include "script.h"
#include "include/hashmap.h"
#include "include/microui.h"
#include <stdbool.h>

/*
**  editor.c / main.c(? lol)
*/

/*
**  editor_undo.c
*/

typedef struct {
	WORD id;
	union {
		SECTOR sector;
		WALL wall;
		//OBJECT object;
	};
} undo_args_t;

typedef void (*undo_func_t)(undo_args_t args);

typedef struct {
	undo_func_t func;
	undo_args_t args;
} undo_action_t;

typedef struct {
	undo_action_t *actions;
	size_t numactions;
	char *opname;
} undo_transaction_t; // TODO rename 'transaction' to 'operation'

#define UNDO_MAX_NEW 1024

typedef struct {
	undo_transaction_t *transactions;
	size_t transaction_n; // number of transactions. most recent is transactions[transaction_n-1].
	
	undo_action_t new[UNDO_MAX_NEW];
	size_t new_n;

	int in_progress; // transaction in progress?
} undo_t;

void undo_init(undo_t* undos);
void undo_deinit(undo_t* undos);
#define undo_clear undo_deinit
#define undo_reinit undo_deinit
void undo_begin(undo_t* undos);
void undo_add(undo_t* undos, undo_func_t func, undo_args_t args);
void undo_add_wall(undo_t* undos, WORD wid);
void undo_add_sector(undo_t* undos, WORD sid);
void undo_commit(undo_t* undos, const char *opname);
void undo_rollback(undo_t* undos);
void undo_undo(undo_t* undos);

void undo_wall(undo_args_t args);
void undo_sector(undo_args_t args);

/*
**  editor.c
*/

typedef enum {
	EM_NONE,
	EM_DRAW,
	EM_WALLS,
	EM_SECTORS,
	EM_OBJECTS,
} EDITORMODE;

extern EDITORMODE ed_mode;

extern undo_t ed_undos;

void screen_change_editor(void);

/*
**  editor_operations.c
*/

WORD wall_create(WORD sid, undo_t* undos);
void find_portals(WORD lid, undo_t* undos);
WORD sector_make(VECTOR pts[], int n, WORD lid, undo_t* undos);
WORD sector_make_above(WORD sid, undo_t* undos);
WORD sector_make_below(WORD sid, undo_t* undos);
bool sector_split(int x1, int y1, int x2, int y2, WORD lid, undo_t* undos);
void sector_free(WORD sid, undo_t* undos);

/*
**  editor_view.c
*/

typedef enum {
	EC_WALL,
	EC_PORTAL,
	EC_WALL_SEL,
	EC_PORTAL_SEL,
	EC_WALL_OTHERLAYER,
	EC_PORTAL_OTHERLAYER,
	EC_WALL_OTHERLAYER_SEL,
	EC_PORTAL_OTHERLAYER_SEL,

	EC_WALL_NEW,

	EC_BACKGROUND,
	EC_VERTEX,
	EC_VERTEX_SEL,
	EC_VERTEX_NEW,
	
	EC_SEL_RECT,
	EC_GRID_LINE,
	EC_GRID_ORIGIN,
	EC_CAMERA,

	EC_MAX,
} EDITORCOLOUR;

typedef struct
{
	int x, y;
	int zoom;
	int zoom_div;
	int grid;
	PICTURE buf;
	int redraw;
	int panning;
	int panning_startvx, panning_startvy;
	int panning_startmx, panning_startmy;
	hashimap_t selection; // hashmap of selected sectors, walls or objects, depending on mode.
} EDVIEW;

extern WORD edview_layer;
extern int ed_view_mx, ed_view_my;
extern bool ed_view_hover;

//extern BYTE selected_sectors[MAX_SECTOR];
//extern BYTE selected_walls[MAX_WALL];

void ed_view_setcolour(int i, uint8_t r, uint8_t g, uint8_t b);
void ed_view_init(EDVIEW* view, int width, int height);
void ed_view_update(mu_Context* ctx, EDVIEW* view, fe_Context* fe_ctx);
void ed_view_deinit(EDVIEW* view);

/*
**  editor_ui.c
*/

extern PICTURE ed_fbuffer;
extern PALLETE ed_palettes[MAX_PALLETE];

extern EDVIEW view;

extern hashimap_t editor_keybinds;

void ed_ui_render(void);

uint8_t rgb32_red(uint32_t rgb);
uint8_t rgb32_green(uint32_t rgb);
uint8_t rgb32_blue(uint32_t rgb);
uint32_t rgb8_to_rgb32(uint8_t red, uint8_t green, uint8_t blue);

void ed_ui_init(void);
void ed_ui_deinit(void);
void ed_ui_update(void);
void ed_ui_render(void);
void ed_ui_keypress(int plat_key, int text_key);
void ed_ui_keyrelease(int plat_key, int text_key);
void ed_ui_mousepress(int mouse_x, int mouse_y, BYTE button);
void ed_ui_mouserelease(int mouse_x, int mouse_y, BYTE button);
void ed_ui_mousemove(int x, int y);

/*
**  editor_script.c
*/

typedef int (*integerbox_edit_func)(int*, int);
void console_submit(void);

#endif
