#ifndef __SCRIPT_H__
#define __SCRIPT_H__

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>
#include "include/fe.h"
#include "include/fex.h"

void script_init(void);
void script_deinit(void);
bool script_inscript(void);
void script_push(void);
void script_pop(void);
int script_tostring(fe_Object* obj, char *dst, int size);
fe_Object* script_do_file(const char *filename);
fe_Object* script_do_string(const char *str);
fe_Object* script_do_string_uncaught(const char *str);
fe_Object* script_do_file(const char *filename);

void script_keypress(fe_Object* func);

// TODO better place to put these / handle these
void panic_(const char *str, int line, const char *file, const char *func);
void expect_(const char *str, int line, const char *file, const char *func);

#define panic(x) panic_(x, __LINE__, __FILE__, __func__)
#define expect(x) do { if (!(x)) { expect_(#x, __LINE__, __FILE__, __func__); }} while(0);

// 'wrapped' ptr type for multiple custom fe types using FE_TPTR.
typedef enum {
	SP_NONE = 0,
	SP_INT, SP_FLOAT, SP_BOOL, // basic scalar types
	SP_LONG, SP_BYTE, SP_WORD, SP_DWORD, // scalar types defined in defs.h
	SP_FIXED, // scalar types handled specially
	SP_SECTOR, SP_WALL, // map data structures
	SP_HASHMAP_S, SP_HASHMAP_I, // other data structures
	SP_MAX
} script_type;

enum {
  SP_READONLY = (1 << 0),
  SP_DYNAMIC  = (1 << 1),
};

typedef struct {
	script_type type;
	void* ptr;
	int flags;
} script_ptr;

#define SP_is_readonly(sptr_p) ((sptr_p)->flags & SP_READONLY)

typedef struct { const char *name; script_type type; void* ptr; int flags; } script_regptr;
typedef struct { const char *name; fe_CFunc fn; } script_regfunc;

void script_register(fe_Context *ctx, script_regptr* ptrs, script_regfunc* funcs);

#endif
