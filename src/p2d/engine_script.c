#include "console.h"
#include "editor.h"
#include "engine.h"
#include "palette.h"
#include "picture.h"
#include "script.h"

static fe_Object* sc_game_set(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];

	// TODO check presence of arg and error here.
	if (fe_type(ctx, fe_car(ctx, arg)) != FE_TSTRING) {
		printf("game name should be string [%d], was [%d]\n", FE_TSTRING, fe_type(ctx, fe_car(ctx, arg))); // TODO gooder error handling
		return fe_bool(ctx, 0);
	}

	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));
	// TODO sanitisation etc. of game name :^)

	game_set(buf);

	// include the game initialisation script 'game.fe' from within script context.
	int gc = fe_savegc(ctx);

	fe_Object* objs[2];
	objs[0] = fe_symbol(ctx, "include");
	char scriptpath[1024];
	sprintf(scriptpath, "%s/game.fe", curgame);
	objs[1] = fe_string(ctx, scriptpath);

	/*fe_Object* result =*/ fe_eval(ctx, fe_list(ctx, objs, 2));

	fe_restoregc(ctx, gc);

	printf("set game to: %s\n", curgame);

	return fe_bool(ctx, 1);
}

// TODO game:loadmap

static fe_Object* sc_loadsky(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/sky/%s.png", curgame, buf);

	PICTURE tempsky;
	tempsky.buffer = 0;
	picture_load_from_png(&tempsky, filepath, 0);

	if (!tempsky.buffer) {
		fprintf(stderr, "failed to load sky textures %s\n", filepath);
		return fe_bool(ctx, 0);
	}
	
	if (pic_sky.width != tempsky.width || pic_sky.height != tempsky.height) {
		// if new sky is a different size, have to reallocate sky texture memory space.
		// assuming both images are same bpp (should both be 8bpp anyway).
		if (pic_sky.buffer) {
			picture_destroy(&pic_sky);
		}

		picture_create(&pic_sky, tempsky.width, tempsky.height, 8, 0, NULL);
	}

	picture_copy(&pic_sky, &tempsky);
	picture_destroy(&tempsky);

	return fe_bool(ctx, 1);
}

static fe_Object* sc_loadpalette(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/colours/%s.png", curgame, buf);

	palette_load_from_png(palette, filepath);
	memcpy(ed_palettes[0], palette, sizeof(PALLETE)); // TEMP BODGE

	return fe_null(ctx);
}

// TODO YOUAREHERE.
// lightmaps and blenders loaded from <game>/colours/.
// also script 'file exists' function if not already.
// recalclightmap takes gamma argument.
// lightmap/blender handling done entirely through script.

// load a lightmap from a file.
static fe_Object* sc_loadlightmap(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/colours/%s.png", curgame, buf);

	PICTURE pic;
	memset(&pic, 0, sizeof(PICTURE));

	picture_load_from_png(&pic, filepath, 0);

	if (!pic.buffer) {
		con_error("failed to load lightmap %s", filepath);
		return fe_null(ctx);
	}

	// check size of file (should be a certain number of bytes)
	if (pic.width * pic.height != sizeof(LIGHTMAP) * NUMLIGHTMAPS) {
		// TODO proper expected resolution of lightmap. hurr.
		con_error("lightmap %s incorrect size (should be %d, was %d)", filepath, sizeof(LIGHTMAP) * NUMLIGHTMAPS, pic.width * pic.height);
	}

	memcpy(&lightmaps, pic.buffer, sizeof(LIGHTMAP) * NUMLIGHTMAPS);

	picture_destroy(&pic);

	return fe_true(ctx);
}

// load a blendmap from a file.
static fe_Object* sc_loadblender(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/colours/%s.png", curgame, buf);

	PICTURE pic;
	memset(&pic, 0, sizeof(PICTURE));

	picture_load_from_png(&pic, filepath, 0);

	if (!pic.buffer) {
		con_error("failed to load blender %s", filepath);
		return fe_null(ctx);
	}

	if (pic.width * pic.height != 256*256) {
		con_error("blender %s incorrect size (should be 256x256, was %dx%d)", filepath, pic.width, pic.height);
		return fe_null(ctx);
	}

	memcpy(&blender, pic.buffer, 256*256);

	picture_destroy(&pic);

	return fe_true(ctx);
}

// recalc lightmap with a specified gamma level (default 3).
static fe_Object* sc_recalclightmap(fe_Context* ctx, fe_Object* arg)
{
	int gamma;

	if (!fe_isnil(ctx, arg)) {
		gamma = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	} else {
		gamma = 3;
	}

	printf("Calculating Lightmaps");
	// TODO render loading to screen or console or something here...
	// maybe an 'append to last console line' function or something
	int i;
	for (i = 32; i--;) {
		lightmap_create(lightmaps[i], palette, i, gamma);
		putchar('.');
		fflush(stdout);
	}
	putchar('\n');

	return fe_null(ctx);
}

// recalc blender with a specified opacity level level (default 0.5).
static fe_Object* sc_recalcblender(fe_Context* ctx, fe_Object* arg)
{
	int alpha;

	if (!fe_isnil(ctx, arg)) {
		alpha = fl2f(fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
	} else {
		alpha = fl2f(0.5);
	}

	printf("Calculating Blender LUT.\n");
	
	blender_create_fixalpha(blender, palette, alpha);

	return fe_null(ctx);
}

// save lightmap to file
static fe_Object* sc_savelightmap(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/colours/%s.png", curgame, buf);

	PICTURE pic;
	picture_create(&pic, 256, LIGHTMAP_RES * NUMLIGHTMAPS, 8, 0, NULL);
	memcpy(pic.buffer, &lightmaps, sizeof(LIGHTMAP) * NUMLIGHTMAPS);
	picture_save_to_png(&pic, filepath, palette); // TODO proper error handling.

	picture_destroy(&pic);

	return fe_true(ctx);
}

// save blender to file
static fe_Object* sc_saveblender(fe_Context* ctx, fe_Object* arg)
{
	char buf[64];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));

	char filepath[1024];
	sprintf(filepath, "%s/colours/%s.png", curgame, buf);

	PICTURE pic;
	picture_create(&pic, 256, 256, 8, 0, NULL);
	memcpy(pic.buffer, &blender, 256*256);
	picture_save_to_png(&pic, filepath, palette); // TODO proper error handling.

	picture_destroy(&pic);

	return fe_true(ctx);
}

static fe_Object* sc_texturereset(fe_Context* ctx, fe_Object* arg)
{
	texture_reset();
	return fe_null(ctx);
}

static fe_Object* sc_texturereg(fe_Context* ctx, fe_Object* arg)
{
	char buf[1024];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));
	return fe_bool(ctx, texture_register(buf));
}

script_regptr engine_regp[] = {
	{ "sv_height",       SP_INT,   &sv_height,       0 },
	{ "sv_gravity",      SP_FIXED, &sv_gravity,      0 },
	{ "sv_friction_div", SP_INT,   &sv_friction_div, 0 },
	{ "sv_landing",      SP_INT,   &sv_landing,      0 },
	{ "sv_accel",        SP_INT,   &sv_accel,        0 },
	{ "sv_accel_div",    SP_INT,   &sv_accel_div,    0 },
	{ "sv_jump",         SP_FIXED, &sv_jump,         0 },
	{ "sv_crouch",       SP_FIXED, &sv_crouch,       0 },
	{ "sv_turn",         SP_INT,   &sv_turn,         0 },
	{}
};

script_regfunc engine_regf[] = {
	{ "game:set", sc_game_set },
	{ "game:loadsky", sc_loadsky },
	{ "loadpalette", sc_loadpalette },
	{ "loadlightmap", sc_loadlightmap },
	{ "loadblender", sc_loadblender },
	{ "recalclightmap", sc_recalclightmap },
	{ "recalcblender", sc_recalcblender },
	{ "savelightmap", sc_savelightmap },
	{ "saveblender", sc_saveblender },
	{ "texturereset", sc_texturereset },
	{ "texturereg", sc_texturereg },
	{}
};
