
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#include "picture.h"
#include "maths.h"
#include "include/spng.h"
#include <fcntl.h>
#include <stdbool.h>

/*
** Name: picture_create
** Desc: creates a picture. allocates required pixel buffer and pre-calculates scanline pointers.
*/
void picture_create(PICTURE* picture, int width, int height, int bpp, int bytes_per_row, void* buffer)
{
	memset(picture, 0, sizeof(PICTURE));

	if (bpp <= 0) bpp = 8; // default to 8 bpp.

	if (bytes_per_row <= 0) bytes_per_row = (bpp * width + 31) >> 5 << 2;

	picture->width           = width;
	picture->height          = height;
	picture->bpp             = bpp;
	picture->bytes_per_row   = bytes_per_row;
	picture->buffer          = buffer ? buffer : calloc(height, bytes_per_row);
	picture->shared          = buffer != 0;
	picture->scanlines.b     = malloc(height * sizeof(void*));

	while (height--) picture->scanlines.b[height] = (BYTE*)(picture->buffer + bytes_per_row * height);
}

/*
** Name: picture_destroy
** Desc: destroys the resourced allocated to a picture.
*/
void picture_destroy(PICTURE* picture)
{
	assert(picture);
	if (!picture->shared) free(picture->buffer);
	free(picture->scanlines.b);
	memset(picture, 0, sizeof(PICTURE));
}

/*
** Name: picture_resize
** Desc: resizes a pictures buffer. does not maintain picture contents.
*/
void picture_resize(PICTURE* picture, int width, int height)
{
	if (picture->width != width || picture->height != height)
	{
		int bpp = picture->bpp;
		picture_destroy(picture);
		picture_create(picture, width, height, bpp, 0, 0);
	}
}

/*
** Name: picture_clear
** Desc: A fast picture content clear.
*/
void picture_clear(PICTURE* picture)
{
	memset(picture->buffer, 0, picture->bytes_per_row * picture->height);
}

/*
** Name: picture_copy
** Desc: A fast picture content copy. Pictures must be compatible.
*/
void picture_copy(PICTURE* dst, PICTURE* src)
{
	memcpy(dst->buffer, src->buffer, dst->bytes_per_row * dst->height);
}

void picture_load_from_png(PICTURE* picture, const char *filename, bool flipped)
{
	int result = 0;

	spng_ctx* ctx = spng_ctx_new(0);

	if (!ctx) {
		fprintf(stderr, "unable to thingy png ctx\n"); // TODO get error code or some shit like that
		return;
	}

	spng_set_crc_action(ctx, SPNG_CRC_USE, SPNG_CRC_USE);

	FILE* file = fopen(filename, "rb");

	if (!file) {
		fprintf(stderr, "unable to load file %s\n", filename);
	} else {
		spng_set_png_file(ctx, file);

		struct spng_ihdr header;
		result = spng_get_ihdr(ctx, &header);

		if (result != 0) {
			fprintf(stderr, "failed to get png header: %s\n", spng_strerror(result));
		} else if (header.bit_depth != 8 || header.color_type != SPNG_COLOR_TYPE_INDEXED) {
			fprintf(stderr, "only 8-bit indexed pngs supported (bpp %u not 8, type %u not %u)\n", header.bit_depth, header.color_type, SPNG_COLOR_TYPE_INDEXED);
		} else {
			int width = header.width;
			int height = header.height;

			size_t png_size;
			result = spng_decoded_image_size(ctx, SPNG_FMT_PNG, &png_size);

			if (result == 0) {
				picture_create(picture, width, height, 8, 0, 0);

				//result = spng_decode_image(ctx, (BYTE*)picture->buffer, png_size, SPNG_FMT_PNG, 0);

				result = spng_decode_image(ctx, NULL, 0, SPNG_FMT_PNG, SPNG_DECODE_PROGRESSIVE);

				if (result != 0) {
					fprintf(stderr, "failed to decode png: %s\n", spng_strerror(result));
				} else {
					struct spng_row_info row_info = {0};

					do {
						result = spng_get_row_info(ctx, &row_info);
						if (result != 0) {
							break;
						}

						result = spng_decode_row(ctx, picture->scanlines.b[flipped ? height - row_info.row_num - 1 : row_info.row_num], width);
					} while (result == 0);
				}
			}
		}

		fclose(file);
	}

	spng_ctx_free(ctx);
}

void picture_save_to_png(PICTURE* picture, const char *filename, PALLETE palette)
{
	int i;

	if (picture->bpp != 8) {
		fprintf(stderr, "picture_save_to_png ``: unsupported bitrate %dbpp\n", picture->bpp); // TODO impl. 16 & 24 bpp if needed
		return;
	}

	int result = 0;

	spng_ctx* ctx = NULL;
	FILE* file = NULL;

	if (ctx = spng_ctx_new(SPNG_CTX_ENCODER)) {
		if (file = fopen(filename, "wb")) {
			spng_set_png_file(ctx, file);

			// HEADER

			struct spng_ihdr header = {0};

			header.width = picture->width;
			header.height = picture->height;

			if (picture->bpp == 8) {
				header.bit_depth = 8;
				header.color_type = SPNG_COLOR_TYPE_INDEXED;
			} else {
				// non-8-bpp pics not yet supported lmao
			}

			spng_set_ihdr(ctx, &header);

			// PALETTE

			struct spng_plte pngpal = {0};

			pngpal.n_entries = 256;

			for (i = 0; i < 256; i++) {
				pngpal.entries[i].red = palette[i][0];
				pngpal.entries[i].green = palette[i][1];
				pngpal.entries[i].blue = palette[i][2];

				pngpal.entries[i].alpha = 255;
			}

			spng_set_plte(ctx, &pngpal);

			// ENCODE

			int format = SPNG_FMT_PNG;

			if (spng_encode_image(ctx, (BYTE*)picture->buffer, picture->width * picture->height /* <- TOTAL GUESS */, format, SPNG_ENCODE_FINALIZE) != 0) {
				fprintf(stderr, "spng_encode_image() error: %s\n", spng_strerror(result));
			}

			fclose(file);
		}

		spng_ctx_free(ctx);
	}
}

void picture_assert_same(PICTURE* dst, PICTURE* src)
{
	assert(dst && src);
	assert(dst->width  == src->width );
	assert(dst->height == src->height);
	assert(dst->bpp    == src->bpp   );
}

void picture_flip8(PICTURE* dst, PICTURE* src)
{
	int x, y1, y2;

	picture_assert_same(dst, src);

	for (y1 = 0, y2 = dst->height - 1; y1 < y2; y1++, y2--)
	{
		BYTE* s1 = src->scanlines.b[y1];
		BYTE* s2 = src->scanlines.b[y2];
		BYTE* d1 = dst->scanlines.b[y1];
		BYTE* d2 = dst->scanlines.b[y2];

		for (x = dst->width; x--;)
		{
			BYTE tmp = *s1++;
			*d1++ = *s2++;
			*d2++ = tmp;
		}
	}
}

void picture_draw8(PICTURE* dst, PICTURE* src, int x, int y, int flags)
{
	picture_blit8(dst, x, y, x + src->width, y + src->height, src, 0, 0, src->width, src->height, flags);
}

void picture_blit8(
	PICTURE* dst, int x1, int y1, int x2, int y2,
	PICTURE* src, int u1, int v1, int u2, int v2,
	int mode)
{
	int w, h, u, uu, vv;

	if ((w = x2 - x1) <= 0 || (h = y2 - y1) <= 0) return;

	u1 = i2f(u1);
	v1 = i2f(v1);
	u2 = i2f(u2);
	v2 = i2f(v2);

	uu = (u2 - u1) / w;
	vv = (v2 - v1) / h;

	if (x1 < 0          )   {u1 -= x1 * uu; x1 = 0;}
	if (y1 < 0          )   {v1 -= y1 * vv; y1 = 0;}
	if (x2 > dst->width )   {x2  = dst->width     ;}
	if (y2 > dst->height)   {y2  = dst->height    ;}

	if ((w = x2 - x1) <= 0 || (h = y2 - y1) <= 0) return;


	#define INNER_LOOP \
		for (;  y1 < y2;  y1++,v1+=vv) { \
			BYTE* I = src->scanlines.b[f2i(v1)]; \
			BYTE* O = &dst->scanlines.b[y1][x1]; \
			for(w = x2 - x1, u = u1;  w--;  u += uu, O++) { AFFINE } \
		}

	switch (mode) {
		case PICTURE_MODE_COPY: {
			if (uu == i2f(1) && 0) {
				// High speed picture blitter.
				for (;y1 < y2; y1++, v1 += vv) {
					memcpy(&dst->scanlines.b[y1][x1], &src->scanlines.b[f2i(v1)][f2i(u1)], x2 - x1);
				}
			} else {
				#define AFFINE *O = I[f2i(u)];
					INNER_LOOP
				#undef AFFINE
			}

			break;
		}
		case PICTURE_MODE_COLORKEY: {
			BYTE pen;
			#define AFFINE if (pen = I[f2i(u)]) *O = pen;
				INNER_LOOP
			#undef AFFINE

			break;
		}
	}
}

/*
** Name: picture_rescale
** Desc: Scales a picture 'in-place' using a temporary buffer.
**       Doesn't apply any filtering (I wonder if gbm could be used for this?).
*/
void picture_rescale(PICTURE* picture, int newwidth, int newheight) {
	PICTURE temppicture;

	picture_create(&temppicture, picture->width, picture->height, picture->bpp, 0, 0);
	picture_copy(&temppicture, picture);

	int origwidth = picture->width, origheight = picture->height;

	picture_resize(picture, newwidth, newheight);
	picture_blit8(picture, 0, 0, newwidth, newheight, &temppicture, 0, 0, origwidth, origheight, PICTURE_MODE_COPY);

	picture_destroy(&temppicture);
}

void picture_luquid_effect8(PICTURE* dst, PICTURE* src, int tick)
{
	int y;
	for (y = dst->height; y--;)
	{
		int x = fixsin(y * 32 + tick * 5) >> 13;

		picture_blit8(dst, x, y, x + dst->width, y + 1, src, 0, y, src->width, y + 1, PICTURE_MODE_COPY);
		picture_blit8(dst, x - dst->width, y, x, y + 1, src, 0, y, src->width, y + 1, PICTURE_MODE_COPY);
		picture_blit8(dst, x + dst->width, y, x + dst->width + dst->width, y + 1, src, 0, y, src->width, y + 1, PICTURE_MODE_COPY);
	}
}

void picture_blend8(PICTURE* dst, PICTURE* src1, PICTURE* src2, CLUT blender)
{
	int x, y;

	for (y = dst->height; y--;)
	{
		BYTE* a = dst ->scanlines.b[y];
		BYTE* b = src1->scanlines.b[y];
		BYTE* c = src2->scanlines.b[y];

		for (x = dst->width; x--;)
		{
			*a++ = blender[*b++][*c++];
		}
	}
}
