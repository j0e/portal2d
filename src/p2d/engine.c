
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

/*
** POSSIBLE GAME TYPES:
**  Carmageddon, Doom, Blood, Duke Nukem, Shadow Warrior, Driver, Tomb Raider, Dark Forces,
**  Jedi Knight, Uprising
**
** DESIGN CRITERIA
**  - Portable ANSIC 32bit code.
**  - Playable on low spec PC's and game consoles. Playstation, Gameboy Advanced.
**  - Easy world design & game programming.
**  - Flexable game platform. Should handle different types of games.
**  - Small world download for dynamic internet games.
**  - Inside & outside rendering.
**  - Portable to any video graphics card. (MIN: 320x200 Chunky graphics)
**  - Independant of floating point processor. (Fixed point maths)
**  - Extendable. Static lighting, multi leveled sectors, zbuffered polygon meshes.
**  - Compatible with DOOM & Build engine AI code.
**
** TODO
**  - Bases sector functions. Doors, lifts.
**  - global effects. Lightning, palette effects, screen shakes.
**  - Better collision detection and kinetics.
**  - 2D sprites (Use the current polygon rendering & add color keying)
**  - Correct sector sorting to remove dependancy on zbuffering. (Then add 2D Span clipping)
**  - OpenGL rendering. (Maybe Glide support for 3DFX. DOS & Linux)
**  - Look into MMX. (Not really useful these days)
**  - Sector based terain render. Create mesh from bitmap.
*/

//#include "div0.h"
#include "console.h"
#include "engine.h"
#include "wall.h"
#include "sector.h"
#include "level.h"
#include "matrix.h"
#include "polygon.h"
#include "picture.h"
#include "pragmas.h"
#include "maths.h"
#include "editor.h"
#include "script.h"

/*
** Global Variables
*/
LIGHTMAP lightmaps[NUMLIGHTMAPS];
CLUT blender;
OBJECT* camera = &objects[1];
//OBJECT* thing  = &objects[2]; // spinny thingy

PICTURE pic_arrow;
PICTURE pic_crosshair;
PICTURE pic_sky;
PICTURE pic_lens;
PICTURE pic_console;
PICTURE pic_font;
PICTURE pic_logo;

SURFACE clipboard;

PALLETE palette;

// default framebuffer width and height
unsigned framebuffer_w = 640;
unsigned framebuffer_h = 480;

char sbuf[1024]; // General purpose string buffer. Results should be copied after use.

int mousegrab;
LONG mouse_dx, mouse_dy; // relative mouse movement when mousegrab is enabled

// new customisable engine/gameplay variables.
int sv_height = 300;
int sv_gravity = fl2f(0.4);
int sv_friction_div = 18;
int sv_landing = 8;
int sv_accel = 6; // TODO refactor these into one, more sensible, variable.
int sv_accel_div = 8;
int sv_jump = fl2f(15);
int sv_crouch = fl2f(0.7);
int sv_turn = 9;
int sv_look = 7; // these two could be customisable controls tbh

void picture_create_mip(PICTURE* dst, PICTURE* src, CLUT blender)
{
	int x, y;

	picture_create(dst, src->width, src->height, src->bpp, 0, 0);

	for (y = 0; y < dst->height; y += 2)
	{
		for (x = 0; x < dst->width; x += 2)
		{
			int a = blender[src->scanlines.b[y + 0][x + 0]][src->scanlines.b[y + 0][x + 1]];
			int b = blender[src->scanlines.b[y + 1][x + 0]][src->scanlines.b[y + 1][x + 1]];
			int c = blender[a][b];

			dst->scanlines.b[y][x] = c;
			dst->scanlines.b[y][x + 1] = c;
			dst->scanlines.b[y + 1][x] = c;
			dst->scanlines.b[y + 1][x + 1] = c;
		}
	}
}

void say(char* s)
{
	printf("%s\n", s);
}

void console_outtext(int x, int y, STRING text)
{
	BYTE* p;
	for (p = &pic_console.scanlines.b[y][x]; (*p++ = *text++) != 0;);
}

// fixed-point
LONG mouse_speed = 2;
LONG mouse_accel = fl2f(2.0);

// TODO functions to load lightmap and blender from a 'current' palette.
void tables_create(void)
{
	int i, j, x, y;

	PICTURE pic;
	memset(&pic, 0, sizeof(PICTURE));

	// lightmaps
	int gamma = 3;

	sprintf(sbuf, "base/lightmap%d.png", gamma); // TODO load lightmaps and blendmaps from game
	picture_load_from_png(&pic, sbuf, 0);

	if (pic.buffer) {
		assert((pic.width * pic.height) == sizeof(LIGHTMAP) * NUMLIGHTMAPS);
		memcpy(&lightmaps, pic.buffer, sizeof(LIGHTMAP) * NUMLIGHTMAPS);
	} else {
		printf("Calculating Lightmaps");
		// TODO! render loading to screen here
		for (i = 32; i--;) {
			lightmap_create(lightmaps[i], palette, i, gamma);
			putchar('.');
			fflush(stdout);
		}
		putchar('\n');

		say("Saving Lightmaps.");

		picture_create(&pic, 256, LIGHTMAP_RES * NUMLIGHTMAPS, 8, 0, NULL);
		memcpy(pic.buffer, &lightmaps, sizeof(LIGHTMAP) * NUMLIGHTMAPS);
		picture_save_to_png(&pic, sbuf, palette);
	}
	picture_destroy(&pic);

	// blender
	assert(!pic.buffer);
	sprintf(sbuf, "base/blender.png", gamma);
	picture_load_from_png(&pic, sbuf, 0);

	if (pic.buffer) {
		assert((pic.width * pic.height) == 256*256);
		memcpy(&blender, pic.buffer, 256*256);
	} else {
		printf("Calculating Blender LUT.\n");

		blender_create_fixalpha(blender, palette, fl2f(0.5));

		picture_create(&pic, 256, 256, 8, 0, NULL);
		memcpy(pic.buffer, &blender, 256*256);
		picture_save_to_png(&pic, sbuf, palette);
	}
	picture_destroy(&pic);
}

int texture_numloaded = 0; // number of currently loaded textures. must be <= MAX_TEXTURE.

void texture_reset(void)
{
	for (int i = 0; i < MAX_TEXTURE; i++) {
		if (textures[i].buffer) {
			picture_destroy(&textures[i]);
		}
	}

	texture_numloaded = 0;
}

bool texture_register(char *filename)
{
	if (texture_numloaded >= MAX_TEXTURE) {
		con_warning("maxmimum number of textures reached");
		return false;
	}

	assert(!textures[texture_numloaded].buffer);

	// assuming texture is png - TODO other formats in future :^)
	picture_load_from_png(&textures[texture_numloaded], filename, true);

	if (textures[texture_numloaded].buffer) {
		if (textures[texture_numloaded].width != TEXTURE_WIDTH || textures[texture_numloaded].height != TEXTURE_HEIGHT) {
			con_warning("texture %d (%s) should be %dx%d - will be resized", texture_numloaded, filename, TEXTURE_WIDTH, TEXTURE_HEIGHT);
			picture_rescale(&textures[texture_numloaded], TEXTURE_WIDTH, TEXTURE_HEIGHT);
			// TODO set surface default x-scale/y-scale at this point?
		}
	} else {
		con_error("could not load texture %s", filename);
		texture_numloaded++; // leave empty texture slot to avoid shifted textures. TODO 'no texture' texture
		return false;
	}

	texture_numloaded++;
	return true;
}

void loadsky(char *filename)
{
	if (pic_sky.buffer) {
		picture_destroy(&pic_sky);
	}

	picture_load_from_png(&pic_sky, filename, false);
}

void engine_create(void)
{
	int i;

	say("AlienKinetics 3D Engine [DOS] - Copyright (C) 2001-2002 AlienKinetics");
	say("---------------------------------------------------------------------");
	say("");

	// initialise keybinds
	hmi_init(&editor_keybinds);
	//hmi_init(engine_keybinds);

	say("Initialising scripting engine.");
	script_init();

	say("Building sine table.");

	for (i = 0; i < SINTABLE_MAX; i++) {
		sintable[i] = fl2f(sin(i * M_PI / 1024.0));
	}

	say("Allocating offscreen buffers.");

	// load global images - TODO put these in a global array w/ enums (`PICS[PIC_FONT]` etc)
	picture_load_from_png(&pic_font,      "base/font8x8new3.png", 0);
	//picture_load_from_png(&pic_sky,       "base/sky.png",         0); // TODO this will be reloaded with the game-specific sky image path.
	picture_create(&pic_sky, 512, 200, 8, 0, NULL);
	picture_load_from_png(&pic_arrow,     "base/arrow.png",       0);
	picture_load_from_png(&pic_crosshair, "base/crosshair.png",   0);
	picture_load_from_png(&pic_lens ,     "base/lens.png",        0);
	picture_load_from_png(&pic_logo ,     "base/logo.png",        0);
	// TODO exit early if any of the above couldn't be loaded

	// set up default game here (TODO get from command line params)
	sprintf(curgame, "game");
	
	game_set("game");
	
	//tables_create();

	undo_init(&ed_undos); // undos are persisted across whole 'lifetime' of program.
}

void engine_destroy(void) {
	script_deinit();
	plat_video_deinit();
	undo_deinit(&ed_undos);
	exit(0); // BODGE - should exit from main() loop
}

// BODGE - TODO find better place to put these without making a whole screen.c|h
void (*current_execute)(void);
void (*current_screenon)(void);
void (*current_screenoff)(void);

unsigned mstotal, prevmstotal;

unsigned msfpstotal_lasttick;

static const int timer_fps = 120;

void engine_screen_on(void)
{
	//div0_init(DM_SATURATE);

	int i;

	mousegrab = 0;

	plat_mouse_show(0);

	render_init_buffers();

	picture_create(&pic_console, 40, 10, 8, 0, 0);

	plat_palette_install(0, palette); // TODO multiple palettes for e.g. underwater, pain, etc
	plat_palette_set(0);

	msfpstotal_lasttick = (prevmstotal = mstotal = plat_time_update()) * timer_fps;

	// spinny thingy
	/*
	thing->sid = 0;
	thing->x   = -900;
	thing->y   = 2000;
	thing->z   = 200;
	thing->front.texture = 124;
	//thing->front.flags |= SURFACE_OPAQUE;
	thing->back.texture = 10;
	//thing->back.flags |= SURFACE_OPAQUE;
	*/

	camera->sid = 0;
	camera->z   = WORLD_UNIT * 4; // TODO get sector_z of actual camera sid

	// set up player height etc - TODO proper player init routine
	// unit is (could be?) in world units. i.e. 64 = 1 'cube'.
	// define a bunch of macros for this.
	camera->below = sv_height;

	tick = 0; // number of ticks elapsed since game start. Must be 120 per 1000 ms.
}

void engine_screen_off(void)
{
	render_deinit_buffers();
	picture_destroy(&pic_console);
}

unsigned tick_count = 0;
unsigned prevmstotal = 0;

void engine_screen_render()
{
	int i, x, y;
	int floor_z, ceil_z, from_floor, from_ceil, view_z;

	picture_luquid_effect8(&textures[41], &textures[42], tick);
	picture_luquid_effect8(&textures[43], &textures[44], tick);

	picture_blend8(&textures[113], &textures[41], &textures[15], blender);
	picture_blend8(&textures[114], &textures[41], &textures[64], blender);

	sprintf(sbuf, "X:%4d Y:%4d Z:%4d SID:%d",
		camera->x,
		camera->y,
		camera->z,
		camera->sid);

	console_outtext(0, 0, sbuf);

	sector_z(camera->sid, camera->x, camera->y, &floor_z, &ceil_z, 0);

	int msframe = plat_mstotal - prevmstotal;
	sprintf(sbuf, "% 6d % 6d  FPS: %4.1f  TPS: %4.1f   ", floor_z, ceil_z, 1000.0 / msframe, ((double)tick_count * 1000.0) / msframe);

	console_outtext(0, 1, sbuf);

	render_view(camera);

	prevmstotal = plat_mstotal;
}

void engine_screen_execute(void)
{
	int i, x, y;
	int floor_z, ceil_z, from_floor, from_ceil, view_z;

	tick_count = 0;

	while ((plat_time_update() * timer_fps) - msfpstotal_lasttick >= 1000) {
		msfpstotal_lasttick += 1000;
		tick++;
		tick_count++;

		plat_input_update();

		camera->zz -= sv_gravity; // Gravity

		//object_update(thing);
		//thing->rot.t += 2; // spinny thingy

		object_update(camera);
		object_collision(camera);

		sector_z(camera->sid, camera->x, camera->y, &floor_z, &ceil_z, NULL);

		view_z = (camera->z << 6);

		from_floor = view_z - (camera->below << 6) - floor_z;
		from_ceil  = ceil_z - view_z - (camera->above << 6); // TODO what/where is camera -> above even defined? I guess should 'default' to 0?

		sprintf(sbuf, "from_floor: %d    ", from_floor);
		console_outtext(0, 2, sbuf);

		sprintf(sbuf, "from_ceil:  %d    ", from_ceil);
		console_outtext(0, 3, sbuf);

		sprintf(sbuf, "sector height:  %d    ", ceil_z - floor_z);
		console_outtext(0, 4, sbuf);

		// traversing through sector portals:
		// use < and >. that way, when obj->z<<6 === floor_z, obj can be in either sector.
		// NOTE: might have to take into account the object's height here,
		//       because that'll have to be above a certain threshold when 'clipping thru' ceiling portal as well!
		// ALSO: using both eye height and obj bot/top could cause weird behaviour between entities
		//       with traversing portals depending on its relative eye height vs. physical Z range.
		//       the entity should ideally 'cross' the portal (i.e. its sid updates) when its Z position goes below the portal floor or above the portal ceiling.
		if (from_floor < 0) {
			if (sectors[camera->sid].lower_sid) {
				if (view_z < floor_z) {
					// set camera sid to its lower sid (we've falled through floor linked portal)
					assert(sectors[sectors[camera->sid].lower_sid].lid);
					camera->sid = sectors[camera->sid].lower_sid;
				}
			} else {
				// on floor
				camera->xx -= camera->xx / sv_friction_div;
				camera->yy -= camera->yy / sv_friction_div;
				camera->zz -= camera->zz / sv_friction_div;

				camera->zz -= from_floor * sv_landing; // from_floor < 0 so camera->zz is incremented here.

				// limit z velocity somehow? or 'floor' it to the nearest 4096?
				camera->zz &= 0xFFFFF000;

				if (KEY_DOWN('W'))
				{
					camera->xx += imuldiv(fixsin(camera->rot.t), sv_accel, sv_accel_div);
					camera->yy += imuldiv(fixcos(camera->rot.t), sv_accel, sv_accel_div);
				}
				if (KEY_DOWN('S'))
				{
					camera->xx -= imuldiv(fixsin(camera->rot.t), sv_accel, sv_accel_div);
					camera->yy -= imuldiv(fixcos(camera->rot.t), sv_accel, sv_accel_div);
				}
				if (KEY_DOWN('A'))
				{
					camera->xx -= imuldiv(fixcos(camera->rot.t), sv_accel, sv_accel_div);
					camera->yy += imuldiv(fixsin(camera->rot.t), sv_accel, sv_accel_div);
				}
				if (KEY_DOWN('D'))
				{
					camera->xx += imuldiv(fixcos(camera->rot.t), sv_accel, sv_accel_div);
					camera->yy -= imuldiv(fixsin(camera->rot.t), sv_accel, sv_accel_div);
				}
				if (KEY_PRESSED(' ')) // Jump - TODO FIXME highly framerate-dependent(?)
				{
					camera->zz += sv_jump;
				}
				if (MOD_DOWN(MOD_CTRL)) // Crouch
				{
					camera->zz -= sv_crouch;
				}
			}
		} else if (from_ceil < 0) {
			if (sectors[camera->sid].upper_sid) {
				if (view_z > ceil_z) {
					// pass through ceiling portal
					assert(sectors[sectors[camera->sid].upper_sid].lid);
					camera->sid = sectors[camera->sid].upper_sid;
				}
			} else {
				// hit ceiling
				camera->zz = -abs(camera->zz / 2);
				// TODO better way of setting Z velocity
				// current way gets 'stuck' in treacle-like ceiling if sufficiently far 'inside' ceiling
				// maybe average cur. vel. and terminal down vel?
			}
		}

		// collision/portal clipping processing completed, now run 'in air' routine (if any?).
		if (from_floor >= 0 && from_ceil <= 0) {

		}

		if (KEY_DOWN(KB_UPARROW) && camera->rot.p <  320) camera->rot.p += sv_look;
		if (KEY_DOWN(KB_DNARROW) && camera->rot.p > -320) camera->rot.p -= sv_look;
		if (KEY_DOWN(KB_RTARROW)) camera->rot.t += sv_turn;
		if (KEY_DOWN(KB_LTARROW)) camera->rot.t -= sv_turn;

		if (KEY_DOWN(KB_ESC)) {
			screen_change_editor();
			return;
		}
	}

	engine_screen_render();
}

#define SURFACE_EDIT(VAR,SUR,A) (SUR.VAR += (A));

#define SC_WALL 0
#define SC_BOT  1
#define SC_TOP  2
#define SC_MID  3
#define SC_OBJFRONT 8
#define SC_OBJBACK 9

void engine_screen_keypress(int plat_key, int text_key)
{
	//#define SURFACE_EDIT(VAR,SUR,A) (clipboard.VAR = (SUR.VAR = MOD_DOWN(MOD_ALT) ? clipboard.VAR : SUR.VAR + (MOD_DOWN(MOD_SHIFT) ? -(A) : (A))))

	WORD code = pic_stencil.scanlines.w[plat_mouse_y][plat_mouse_x];
	WORD id = code & 0xFFF;
	WORD pointingat = code >> 12;

	int x = plat_mouse_x;
	int y = plat_mouse_y;

	switch(plat_key) {
		case '=': // TODO make this F12
			picture_save_to_png(&pic_bbuffer, "screen.png", palette);
			break;

		case 'O': // Opacity
			if (pointingat == SC_MID)      sectors[id].mid  .flags ^= SURFACE_OPAQUE;
			if (pointingat == SC_OBJFRONT) objects[id].front.flags ^= SURFACE_OPAQUE;
			if (pointingat == SC_OBJBACK)  objects[id].back .flags ^= SURFACE_OPAQUE;
			break;

		case 'M':
			sectors[camera->sid].flags ^= SECTOR_RENDER_MIDDLE;
			break;

		case 'C':
			// TOOD simplified mouse grabbing mechanics:
			//    - click to grab mouse in 3d mode
			//    - press esc to ungrab (then press esc again to enter editor?)
			//    - should be toggleable on or off to allow selecting surfaces etc without grabbing mouse if wanted.
			if (MOD_DOWN(MOD_ALT)){
				mousegrab = !mousegrab;
				if (mousegrab) {
					plat_mouse_write(framebuffer_w >> 1, framebuffer_h >> 1);
				}
			}
			break;
	}
}

void engine_screen_keyrelease(int plat_key, int text_key)
{

}

void engine_screen_mousepress(int x, int y, int button)
{
	WORD code = pic_stencil.scanlines.w[y][x];
	WORD id   = code & 0xFFF;
	WORD pointingat = code >> 12;

	switch (button) {
		case MB_WHEELUP:
		case MB_WHEELDN:
			int direction = button==MB_WHEELUP ? 1 : button==MB_WHEELDN ? -1 : 0;
			int amount = i2f(1);

			int held = 0;

			#define keymod(key) (held |= KEY_DOWN(key), KEY_DOWN(key))

			// Texture
			if (keymod('T')) {
				if (pointingat == SC_WALL) SURFACE_EDIT(texture, walls  [id].surface, direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(texture, sectors[id].bot    , direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(texture, sectors[id].top    , direction);
				if (pointingat == SC_MID)  SURFACE_EDIT(texture, sectors[id].mid    , direction);

				if (pointingat == SC_OBJFRONT) SURFACE_EDIT(texture, objects[id].front  , direction);
				if (pointingat == SC_OBJBACK)  SURFACE_EDIT(texture, objects[id].back   , direction);
			}

			// X slope
			if (keymod('J')) {
				amount = MOD_DOWN(MOD_SHIFT) ? 1 : 4;

				if (pointingat == SC_BOT) sectors[id].bot.slopex += (amount * direction);
				if (pointingat == SC_TOP) sectors[id].top.slopex += (amount * direction);
				if (pointingat == SC_MID) sectors[id].mid.slopex += (amount * direction);
			}

			// Y slope
			if (keymod('K')) {
				amount = MOD_DOWN(MOD_SHIFT) ? 1 : 4;

				if (pointingat == SC_BOT) sectors[id].bot.slopey += (amount * direction);
				if (pointingat == SC_TOP) sectors[id].top.slopey += (amount * direction);
				if (pointingat == SC_MID) sectors[id].mid.slopey += (amount * direction);
			}

			// surface X panning
			if (keymod('X')) {
				amount = MOD_DOWN(MOD_SHIFT) ? i2f(1) : i2f(16);

				if (pointingat == SC_WALL) SURFACE_EDIT(panningx, walls  [id].surface, amount * direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(panningx, sectors[id].bot    , amount * direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(panningx, sectors[id].top    , amount * direction);
			}

			// surface Y panning
			if (keymod('Y')) {
				amount = MOD_DOWN(MOD_SHIFT) ? i2f(1) : i2f(16);

				if (pointingat == SC_WALL) SURFACE_EDIT(panningy, walls  [id].surface, amount * direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(panningy, sectors[id].bot    , amount * direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(panningy, sectors[id].top    , amount * direction);
			}

			// H repeat
			if (keymod('H')) {
				if (pointingat == SC_WALL) SURFACE_EDIT(repeatx, walls  [id].surface, direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(repeatx, sectors[id].bot    , direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(repeatx, sectors[id].top    , direction);
			}

			// V repeat
			if (keymod('V')) {
				if (pointingat == SC_WALL) SURFACE_EDIT(repeaty, walls  [id].surface, direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(repeaty, sectors[id].bot    , direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(repeaty, sectors[id].top    , direction);
			}

			// Light (Brightness)
			if (keymod('L')) {
				if (pointingat == SC_WALL) SURFACE_EDIT(light, walls  [id].surface, direction);
				if (pointingat == SC_BOT)  SURFACE_EDIT(light, sectors[id].bot    , direction);
				if (pointingat == SC_TOP)  SURFACE_EDIT(light, sectors[id].top    , direction);
				// TODO mid light level?
			}

			// no key held down; default action is to edit floor & ceiling
			if (!held) {
				amount = amount = MOD_DOWN(MOD_SHIFT) ? 1 : WORLD_UNIT;

				if (pointingat == SC_BOT) SURFACE_EDIT(slopez, sectors[id].bot, amount * direction);
				if (pointingat == SC_TOP) SURFACE_EDIT(slopez, sectors[id].top, amount * direction);
				if (pointingat == SC_MID) SURFACE_EDIT(slopez, sectors[id].mid, amount * direction);
			}

	}
}

void engine_screen_mouserelease(int x, int y, int button)
{

}

void engine_screen_mousemove(int x, int y)
{
	if (mousegrab) {
		LONG mouse_dx = i2f(x - (int)(plat_screen_w >> 1));
		LONG mouse_dy = i2f(y - (int)(plat_screen_h >> 1));

		// TODO get vector magnitude of overall mouse movement, rather than handling axes separately.

		if (mouse_dx) {
			// Fixed point numbers being directly added, so make sure to multiply by fractional values!
			camera->rot.t += f2i(mouse_dx + FIXHALF); // camera->rot.t is turn
		}

		if (mouse_dy < 0 && camera->rot.p < 512) {
			camera->rot.p -= f2i(mouse_dy + FIXHALF);

			if (camera->rot.p > 512) {
				camera->rot.p = 512;
			}
		}

		if (mouse_dy > 0 && camera->rot.p > -512) {
			camera->rot.p -= f2i(mouse_dy + FIXHALF);

			if (camera->rot.p < -512) {
				camera->rot.p = -512;
			}
		}

		if (mouse_dx || mouse_dy) {
			sprintf(sbuf, "dx: %d    ", mouse_dx);
			console_outtext(0, 6, sbuf);

			sprintf(sbuf, "dy: %d    ", mouse_dy);
			console_outtext(0, 7, sbuf);

			plat_mouse_write((int)(plat_screen_w >> 1), (int)(plat_screen_h >> 1)); // TODO should be handled by plat
		}
	}
}

void screen_change_engine(void)
{
	SCREEN_CHANGE(engine);
}
