
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#include "maths.h"
#include "palette.h"
#include "include/spng.h"

/*
**  Load a palette from a raw data file (must be exactly 3x256 = 768 bytes in size).
*/
void palette_load_from_raw(PALLETE palette, const char* filename)
{
	FILE* fp = fopen(filename, "rb");

	if (fp) {
		fseek(fp, 0L, SEEK_END);
		LONG size = ftell(fp);
		rewind(fp);

		if (size != 768) {
			fprintf(stderr, "palette should be 768 bytes, was %d\n", size);
		} else {
			int i, j;

			for (i = 0; i < 256; i++) {
				for (j = 0; j < 3; j++) {
					fread(&palette[i][j], 1, 1, fp);
				}
			}
		}

		fclose(fp);
	} else {
		printf("could not load palette file `%s`", filename);
		perror("");
	}
}

/*
**  Load a palette from an 8-bit or 24-bit png file.
*/
void palette_load_from_png(PALLETE palette, const char *filename)
{
	int result;

	spng_ctx* ctx = spng_ctx_new(0);

	if (!ctx) {
		fprintf(stderr, "failed to initialise spng context\n");
		return;
	}

	spng_set_crc_action(ctx, SPNG_CRC_USE, SPNG_CRC_USE);

	FILE* file = fopen(filename, "rb");

	if (!file) {
		printf("could not load palette file `%s`", filename);
		perror("");
		spng_ctx_free(ctx);
		return;
	}

	spng_set_png_file(ctx, file);

	struct spng_ihdr header;
	result = spng_get_ihdr(ctx, &header);

	if (result != 0) {
		fprintf(stderr, "failed to get png header: %s\n", spng_strerror(result));
	} else if (header.bit_depth != 8) {
		fprintf(stderr, "unsupported bit depth (expected 8, got %d)\n", header.bit_depth);
	} else if (header.color_type == SPNG_COLOR_TYPE_INDEXED) {
		// 8-bit (256-colour) png
		struct spng_plte pngpal = {0};

		result = spng_get_plte(ctx, &pngpal);

		if (result && result != SPNG_ECHUNKAVAIL) {
			fprintf(stderr, "spng_get_plte() error: %s\n", spng_strerror(result));
			goto palette_load_from_png_end;
		}

		if (pngpal.n_entries != 256) {
			fprintf(stderr, "png has non-256 colour palette (%d colours)\n", pngpal.n_entries);
		}

		int i;
		int n = imin(256, pngpal.n_entries);

		for (i = 0; i < n; i++) {
			palette[i][0] = pngpal.entries[i].red;
			palette[i][1] = pngpal.entries[i].green;
			palette[i][2] = pngpal.entries[i].blue;
		}
	} else if (header.color_type == SPNG_COLOR_TYPE_TRUECOLOR || header.color_type == SPNG_COLOR_TYPE_TRUECOLOR_ALPHA) {
		// 24/32-bit png
		unsigned numpixels = header.width * header.height;
		if (numpixels != 256) {
			fprintf(stderr, "image is not 256 pixels big (%u x %u == %u)\n", header.width, header.height, numpixels);
		}

		unsigned n = imin(numpixels, 256);

		size_t png_size;
		result = spng_decoded_image_size(ctx, SPNG_FMT_PNG, &png_size);

		if (result != 0) {
			fprintf(stderr, "spng_decoded_image_size() error: %s\n", spng_strerror(result));
			goto palette_load_from_png_end;
		}

		// BODGE: for now, assume png is 32-bit i.e. png size is 4x number of pixels.
		if (png_size != numpixels * 4) {
			fprintf(stderr, "expected %u-pixel image to be %u bytes, is %u\n", numpixels, numpixels * 4, png_size);
		}

		DWORD *buffer = malloc(sizeof(DWORD) * png_size);
		result = spng_decode_image(ctx, buffer, png_size, SPNG_FMT_PNG, 0);

		if (result != 0) {
			fprintf(stderr, "spng_decode_image() error: %s\n", spng_strerror(result));
			free(buffer);
			goto palette_load_from_png_end;
		}

		int i;

		for (i = 0; i < n; i++) {
			// BODGE: for now, assuming png is in ABGR format (this could change with platform endianness etc)
			palette[i][0] = buffer[i] & 0xff;
			palette[i][1] = (buffer[i] >> 8) & 0xff;
			palette[i][2] = (buffer[i] >> 16) & 0xff;
		}

		free(buffer);
	} else {
		fprintf(stderr, "unexpected colour type (expected [%d, %d, %d], got [%d])\n", SPNG_COLOR_TYPE_INDEXED, SPNG_COLOR_TYPE_TRUECOLOR, SPNG_COLOR_TYPE_TRUECOLOR_ALPHA, header.color_type);
	}

	palette_load_from_png_end:

	fclose(file);
	spng_ctx_free(ctx);
}

/*
**  Search for the closest RGB colour in a palette.
*/
BYTE palette_search(PALLETE palette, int r, int g, int b)
{
	int i;
	BYTE best_i;
	int best_dist = INT_MAX;

	for (i = 256; i--;) {
		int dist = isqr(palette[i][0] - r) + isqr(palette[i][1] - g) + isqr(palette[i][2] - b);

		if (dist < best_dist) {
			best_i = i;
			best_dist = dist;
		}
	}

	return best_i;
}

void palette_filter_A(PALLETE dst, PALLETE src)
{
	int i, x;

	for (i = 0; i < 256; i++) {
		//x = RGB_BRIGHTNESS(palette[i][0], palette[i][1], palette[i][2]);

		dst[i][0] = imin(pow(src[i][0] / 255.0, 64.0 / 64) * 255.0, 255);
		dst[i][1] = imin(pow(src[i][1] / 255.0, 64.0 / 64) * 255.0, 255);
		dst[i][2] = imin(pow(src[i][2] / 255.0, 64.0 / 64) * 255.0, 255);
	}
}

void palette_filter_B(PALLETE dst, PALLETE src)
{
	int i;
	for (i = 0; i < 256; i++) {
		dst[i][0] = imin(imuldiv(src[i][0], 128, 255), 255);
		dst[i][1] = imin(imuldiv(src[i][1], 128, 255), 255);
		dst[i][2] = imin(imuldiv(src[i][2], 255, 255), 255);
	}
}

/*
**  Create a lightmap (maps palette colours from light to dark) for the specified brightness (sector light level) and gamma.
*/
void lightmap_create(LIGHTMAP lightmap, PALLETE palette, int brightness, int gamma)
{
	int fr = 0;
	int fg = 0;
	int fb = 0;
	int i, c;

	for (i = 0; i < LIGHTMAP_RES; i++) {
		for (c = 0; c < 256; c++) {
			lightmap[i][c] = palette_search(palette,
				imin(imuldiv(fr + imuldiv(palette[c][0] - fr, i, LIGHTMAP_RES-1), brightness * gamma, LIGHTMAP_RES-1), 255),
				imin(imuldiv(fg + imuldiv(palette[c][1] - fg, i, LIGHTMAP_RES-1), brightness * gamma, LIGHTMAP_RES-1), 255),
				imin(imuldiv(fb + imuldiv(palette[c][2] - fb, i, LIGHTMAP_RES-1), brightness * gamma, LIGHTMAP_RES-1), 255));
		}
	}
}

// adapted from https://quakewiki.org/wiki/Quake_palette
// currently doesn't support brightness levels - generates very similar to brightness 20 gamma 3.
// will need more tweaking to be usable
void generate_colormap(LIGHTMAP out_colormap, PALLETE palette)
{
	int x, y, i;

	for (x = 0; x < 256; x++) for (y = 0; y < 32; y++) {
		int rgb[3];

		for (i = 0; i < 3; i++) {
			rgb[i] = (palette[x][i] * (31 - y) + 16) >> 4; /* divide by 16, rounding to nearest integer */
			if (rgb[i] > 255) {
				rgb[i] = 255;
			}
		}

		out_colormap[31 - y][x] = palette_search(palette, rgb[0], rgb[1], rgb[2]);
	}
}

/*
**  Create a blender lookup table based on the specified palette.
**  Colours are blended 50/50.
*/
void blender_create(CLUT lut, PALLETE palette)
{
	int dst, src;

	for (src = 0; src < 256; src++) for (dst = 0; dst < 256; dst++) {
		lut[src][dst] = palette_search(palette,
			(palette[dst][0] + palette[src][0]) >> 1,
			(palette[dst][1] + palette[src][1]) >> 1,
			(palette[dst][2] + palette[src][2]) >> 1);
	}
}

/*
**  Create a blender lookup table with a specified alpha opacity.
**  Alpha is fixed-point, normalised between 0 and 1 (i.e. 0..65536).
*/
void blender_create_fixalpha(CLUT lut, PALLETE palette, int src_alpha)
{
	int dst, src;

	if (src_alpha < 0) src_alpha = 0;
	if (src_alpha > i2f(1)) src_alpha = i2f(1);

	int dst_alpha = i2f(1) - src_alpha;

	for (src = 0; src < 256; src++) for (dst = 0; dst < 256; dst++) {
		lut[src][dst] = palette_search(palette,
			f2i(palette[dst][0] * dst_alpha + palette[src][0] * src_alpha),
			f2i(palette[dst][1] * dst_alpha + palette[src][1] * src_alpha),
			f2i(palette[dst][2] * dst_alpha + palette[src][2] * src_alpha));
	}
}
