#include "editor.h"
#include "sector.h"
#include "wall.h"
#include "console.h"

/******************************************************************************/

WORD wall_create(WORD sid, undo_t* undos)
{
	WORD wid;

	assert(sid != 0);

	for (wid = 1; wid < MAX_WALL && walls[wid].sid; wid++);

	assert(wid < MAX_WALL);

	memset(&walls[wid], 0, sizeof(WALL));

	// adding undo-action after memset in same way as in sector_alloc()
	undo_add(undos, undo_wall, (undo_args_t){ .id = wid, .wall = walls[wid] });

	walls[wid].sid = sid;
	walls[wid].surface.texture = 0;

	return wid;
}

void find_portals(WORD lid, undo_t* undos)
{
	WORD i, j;

	for (i = 1; i < MAX_WALL; i++)
	{
		if (walls[i].sid && sectors[walls[i].sid].lid == lid)
		{
			for (j = 1; j < MAX_WALL; j++) // TODO could this be `for (j = i;`?
			{
				if (walls[j].sid && walls_touch(i, j))
				{
					if (walls[i].port != j) {
						if (undos) undo_add(undos, undo_wall, (undo_args_t){ .id = i, .wall = walls[i] });
						walls[i].port = j;
					}

					if (walls[j].port != i) {
						if (undos) undo_add(undos, undo_wall, (undo_args_t){ .id = j, .wall = walls[j] });
						walls[j].port = i;
					}
				}
			}
		}
	}
}

/******************************************************************************/

static WORD sector_alloc(WORD lid, undo_t* undos)
{
	WORD sid;

	assert(lid != 0);

	for (sid = 1; sid < MAX_SECTOR && sectors[sid].lid; sid++);

	assert(sid < MAX_SECTOR);

	memset(&sectors[sid], 0, sizeof(SECTOR));

	// undo-state being added AFTER memset 0 because undoing allocate should be equiv. to deallocate.
	// Relying on 'left-over' data is undefined behaviour.
	undo_add(undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] });

	sectors[sid].lid = lid;

	sectors[sid].top.texture = 2; // TODO configurable in script
	sectors[sid].top.slopez  = WORLD_UNIT * 8; // bot/top.slopez is floor/ceil height

	sectors[sid].bot.texture = 1; // TODO configurable in script

	return sid;
}

WORD sector_make(VECTOR pts[], int n, WORD lid, undo_t* undos)
{
	int i, j;
	WORD sid;
	WALL* wall;

	if (n < 3) return 0;

	// check no vertices on top of each other
	for (i = n; i--;) for (j = n; j--;) if (i != j && pts[i].x == pts[j].x && pts[i].y == pts[j].y) return 0;

	sid = sector_alloc(lid, undos); // will create an undo state for this sid.

	wall = 0;

	for (i = 0; i < n; i++)
	{
		WORD wid = wall_create(sid, undos); // will create an undo state for this wid.

		if (i == 0) sectors[sid].first_wall = wid;

		if (wall) wall->next = wid;

		wall    = &walls[wid];
		wall->x = pts[i].x;
		wall->y = pts[i].y;

		if (pts[i].z)
		{
			wall->surface.light = walls[pts[i].z].surface.light;
		}
	}

	assert(wall);

	// join last wall to first wall to form a closed sector
	wall->next = sectors[sid].first_wall;

	find_portals(lid, undos); // TODO option to not auto-find portals for multiple operations in sequence (will still need to update portals for sectors touching those affected)

	return sid;
}

// TODO consolidate sector_make_above and sector_make_below into one function
WORD sector_make_above(WORD sid, undo_t* undos)
{
	SECTOR* sector = &sectors[sid];

	if (sector->upper_sid) {
		con_warning("sector %u already has an upper portal!", sid);
		return 0;
	}

	if (sector->lid >= 65535) {
		con_warning("sector %u is already at the highest layer!", sid);
		return 0;
	}

	// use new_walls array in same way as making a sector
	VECTOR new_walls[256];
	int number_of_new_walls = 0;

	// make a 'copy' of sector wall positions
	WORD first_wid = FIRST_WALL(sid);
	WORD wid = first_wid;

	do {
		new_walls[number_of_new_walls].x = walls[wid].x;
		new_walls[number_of_new_walls].y = walls[wid].y;
		new_walls[number_of_new_walls].z = 0;

		number_of_new_walls++;
	} while ((wid = NEXT_WALL(wid)) != first_wid);

	// call sector_make with next-higher layer
	WORD new_sid = sector_make(new_walls, number_of_new_walls, sector->lid + 1, undos);

	if (new_sid) {
		sectors[new_sid].bot.slopez = sector->top.slopez;
		sectors[new_sid].top.slopez = sector->top.slopez + WORLD_UNIT * 8;

		undo_add(undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] });

		sectors[sid].upper_sid = new_sid;
		sectors[new_sid].lower_sid = sid;
	}

	return new_sid;
}

WORD sector_make_below(WORD sid, undo_t* undos)
{
	SECTOR* sector = &sectors[sid];

	if (sector->lower_sid) {
		con_warning("sector %u already has a lower portal!", sid);
		return 0;
	}

	if (!sector->lid) {
		con_warning("sector %u is unallocated!", sid);
		return 0;
	}

	if (sector->lid < 1) {
		con_warning("sector %u is already at the lowest layer!", sid);
		return 0;
	}

	// use new_walls array in same way as making a sector
	VECTOR new_walls[256];
	int number_of_new_walls = 0;

	// make a 'copy' of sector wall positions
	WORD first_wid = FIRST_WALL(sid);
	WORD wid = first_wid;

	do {
		new_walls[number_of_new_walls].x = walls[wid].x;
		new_walls[number_of_new_walls].y = walls[wid].y;
		new_walls[number_of_new_walls].z = 0;

		number_of_new_walls++;
	} while ((wid = NEXT_WALL(wid)) != first_wid);

	// call sector_make with next-lower layer
	WORD new_sid = sector_make(new_walls, number_of_new_walls, sector->lid - 1, undos);

	if (new_sid) {
		sectors[new_sid].top.slopez = sector->bot.slopez;
		sectors[new_sid].bot.slopez = sector->bot.slopez - WORLD_UNIT * 8;

		undo_add(undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] });

		sectors[sid].lower_sid = new_sid;
		sectors[new_sid].upper_sid = sid;
	}

	return new_sid;
}

bool sector_split(int x1, int y1, int x2, int y2, WORD lid, undo_t* undos)
{
	WORD w1;

	for (w1 = 1; w1 < MAX_WALL; w1++)
	{
		if (walls[w1].sid && sectors[walls[w1].sid].lid == lid && walls[w1].x == x1 && walls[w1].y == y1)
		{
			WORD w2 = sector_find_wall(walls[w1].sid, x2, y2);

			if (w2 && !walls_linked(w1, w2))
			{
				VECTOR poly[MAX_POLYGON];

				WORD s1 = sector_make(poly, wall_segment(poly, w1, w2), lid, undos);
				WORD s2 = sector_make(poly, wall_segment(poly, w2, w1), lid, undos);

				//assert(s1 && s2); // uh-oh, why was this commented out? (wasn't me, honest!)

				sectors[s1].bot = sectors[walls[w1].sid].bot;
				sectors[s1].top = sectors[walls[w1].sid].top;
				sectors[s2].bot = sectors[walls[w1].sid].bot;
				sectors[s2].top = sectors[walls[w1].sid].top;

				sector_free(walls[w1].sid, undos);

				return true;
			}
		}
	}

	return false;
}

void sector_free(WORD sid, undo_t* undos)
{
	WORD wid, first_wall, next_wall;

	assert(sid && FIRST_WALL(sid));

	wid = first_wall = FIRST_WALL(sid);

	do
	{
		next_wall = NEXT_WALL(wid);

		// if this wall is a portal to another wall, unset that wall's portal
		// (we're assuming that walls[wid].port is properly set)
		if (walls[wid].port) {
			if (undos) undo_add(undos, undo_wall, (undo_args_t){ .id = walls[wid].port, .wall = walls[walls[wid].port] });
			walls[walls[wid].port].port = 0;
		}

		if (undos) undo_add(undos, undo_wall, (undo_args_t){ .id = wid, .wall = walls[wid] });
		memset(&walls[wid], 0, sizeof(WALL));
	}
	while ((wid = next_wall) != first_wall);

	WORD lid = sectors[sid].lid;

	if (undos) undo_add(undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] });
	memset(&sectors[sid], 0, sizeof(SECTOR));

	find_portals(lid, &ed_undos); // TODO option to not auto-find portals for multiple operations in sequence (will still need to update portals for sectors touching those affected)
}
