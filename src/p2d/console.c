#include "console.h"
#include <string.h>
#include <stdarg.h>

CONSOLE console;

static void con_printline(const char *text)
{
	int len = strlen(text) + 2;

	expect(len < sizeof(console.buf));

	if (console.idx + len >= sizeof(console.buf)) {
		memmove(console.buf, console.buf + len, console.idx - len);
		console.idx -= len;
	}

	const char *fmt = (console.idx == 0) ? "%s" : "\n%s";
	console.idx += sprintf(&console.buf[console.idx], fmt, text);
	console.updated = true;
}

void con_printf(const char *text, ...)
{
	char line[1024];

	va_list args;
	va_start(args, text);
	vsnprintf((char *)(&line), 1023, text, args);
	va_end(args);

	printf("%s\n", line);
	con_printline(line);
}

// for now the following two functions just print to stderr,
// could add colour-change control characters in future similar to cube/sauer

void con_warning(const char *text, ...)
{
	char line[1024];

	va_list args;
	va_start(args, text);
	vsnprintf((char *)(&line), 1023, text, args);
	va_end(args);

	fprintf(stderr, "%s\n", line);
	con_printline(line);
}

void con_error(const char *text, ...)
{
	char line[1024];

	va_list args;
	va_start(args, text);
	vsnprintf((char *)(&line), 1023, text, args);
	va_end(args);

	fprintf(stderr, "%s\n", line);
	con_printline(line);
}

void con_submit(void)
{
	fe_Object* result;

	script_push();

	// shortcut: if line doesn't start with `(`, assume surround w/ ()
	// TODO better implementation that takes into account surrounding whitespace etc :^)

	size_t len = strlen(console.inputline);

	if (console.inputline[0] != '(') {
		char newline[len + 3];
		memcpy(newline + 1, console.inputline, len);
		newline[0] = '(';
		newline[len + 1] = ')';
		newline[len + 2] = '\0';

		result = script_do_string(newline);
	} else {
		result = script_do_string(console.inputline);
	}

	if (result && console.debug) {
		char result_str[2048];
		result_str[0] = '-';
		result_str[1] = '>';
		result_str[2] = ' ';
		//char result_str[2048] = "-> ";
		script_tostring(result, result_str + 3, sizeof(result_str) - 3);
		printf("%s\n", result_str);
	}

	script_pop();

	console.inputline[0] = '\0';
	console.submitting = false;
}
