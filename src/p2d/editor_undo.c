#include "editor.h"
#include "console.h"

void undo_init(undo_t* undos)
{
	memset(undos, 0, sizeof(undo_t));
}

void undo_deinit(undo_t* undos)
{
	if (undos->transactions) {
		// undos->transactions should be non-null IFF there are transactions allocated.
		int i;
		for (i = undos->transaction_n; i--;) {
			free(undos->transactions[i].actions);
		}

		free(undos->transactions);
	}
	
	memset(undos, 0, sizeof(undo_t));
}

void undo_begin(undo_t* undos)
{
	// begin an undo 'transaction'.
	// Strong warning if already begun transaction.

	if (undos->in_progress) {
		con_warning("Transaction already in progress. Continuing ongoing transaction.");
	} else {
		undos->in_progress = 1;
		//assert(undos->new_n == 0);
		undos->new_n = 0;
	}
}

void undo_add(undo_t* undos, undo_func_t func, undo_args_t args)
{
	// add a changed data. the params should be the necess. IDs and the data for it to be RESTORED to.
	// Strong warning if not in a transaction.

	if (!undos->in_progress) {
		con_warning("no transaction in progress");
	}

	assert((undos->new_n < UNDO_MAX_NEW, "max undo actions in a transaction reached! what kinda crazy operation are ya try'na do?"));

	memset(&undos->new[undos->new_n], 0, sizeof(undo_action_t));
	undos->new[undos->new_n].func = func;
	undos->new[undos->new_n].args = args;

	undos->new_n++;
}

void undo_add_wall(undo_t* undos, WORD wid)
{
	undo_add(undos, undo_wall, (undo_args_t){ .id = wid, .wall = walls[wid] });
}

void undo_add_sector(undo_t* undos, WORD sid)
{
	undo_add(undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] });
}

static char *strclone(const char *s)
{
	size_t len = strlen(s) + 1;
	char *ns = malloc(len);
	if (!ns) return NULL;
	memcpy(ns, s, len);
	return ns;
}

void undo_commit(undo_t* undos, const char *opname)
{
	// Commit an undo transaction.
	// Strong warning if transaction not in progress.

	if (!undos->in_progress) {
		con_warning("no transaction in progress");
		return;
	}

	if (!undos->new_n) {
		undos->in_progress = 0;
		return;
	}

	undos->transactions = realloc(undos->transactions, sizeof(undo_transaction_t) * (undos->transaction_n + 1));

	undo_action_t *actions = malloc(sizeof(undo_action_t) * undos->new_n);

	int i;
	for (i = 0; i < undos->new_n; i++) {
		actions[i].func = undos->new[i].func;
		actions[i].args = undos->new[i].args;
	}

	undos->transactions[undos->transaction_n].actions = actions;
	undos->transactions[undos->transaction_n].numactions = undos->new_n;
	undos->transactions[undos->transaction_n].opname = strclone(opname); // TODO have reference to language key instead to avoid wasteful copies of the same string

	undos->transaction_n++;
	undos->new_n = 0;
	undos->in_progress = 0;
}

void undo_rollback(undo_t* undos)
{
	if (!undos->in_progress) {
		con_warning("no transaction in progress");
		return;
	}

	int i;
	for (i = undos->new_n; i--;) {
		undos->new[i].func(undos->new[i].args);
		undos->transactions[undos->transaction_n].actions[i].func(undos->transactions[undos->transaction_n].actions[i].args);
	}

	undos->new_n = 0;
	undos->in_progress = 0;
}

void undo_undo(undo_t* undos)
{
	// Undoes the last commit.
	// If begun and not ended transaction, undoes to last complete transaction.
	// Frees undo actions as it goes through the commit.

	if (undos->new_n) {
		con_warning("unfinished transacton! rolling back to last complete transaction");
		undo_rollback(undos);
		return;
	}

	if (!undos->transaction_n) {
		con_printf("nothing more to undo");
		return;
	}

	undos->transaction_n--;

	// note: undo actions of a transaction are in chronological order,
	// so the functions should be called in reverse to undo them.
	int i;
	for (i = undos->transactions[undos->transaction_n].numactions; i--;) {
		undos->transactions[undos->transaction_n].actions[i].func(undos->transactions[undos->transaction_n].actions[i].args);
	}

	con_printf("undo: %s", undos->transactions[undos->transaction_n].opname);

	free(undos->transactions[undos->transaction_n].actions);
	free(undos->transactions[undos->transaction_n].opname);
	//memset(&undos->transactions[undos->transaction_n], 0, sizeof(undo_transaction_t));

	// reallocate undos' transactions with one less to 'chop off' end
	if (undos->transaction_n) {
		undos->transactions = realloc(undos->transactions, sizeof(undo_transaction_t) * undos->transaction_n);
	} else {
		free(undos->transactions);
		undos->transactions = NULL;
	}
}

/******************************************************************************/

void undo_wall(undo_args_t args)
{
	walls[args.id] = args.wall;
}

void undo_sector(undo_args_t args)
{
	sectors[args.id] = args.sector;
}
