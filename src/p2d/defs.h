
/* 
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
** 
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __TYPES_H__
#define __TYPES_H__

/*
** Include ANSIC Headers.
*/
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <memory.h>
#include <math.h>
#include <stdint.h>
#include <limits.h>
#include <stdbool.h>

#undef min
#undef max

#define bit(x) (1 << (x))

/*
**  Simple Datatypes
*/
typedef void VOID;
typedef int32_t LONG;
typedef uint8_t BYTE;
typedef uint16_t WORD;
typedef uint32_t DWORD;

typedef char STRING[256];
typedef char PATH[1024];

typedef struct
{
	LONG x, y;
} POINT;

typedef struct
{
	LONG x1, y1, x2, y2;
} RECT;

typedef struct
{
	LONG x, xx, xxx;
	LONG y, yy, yyy;
	LONG z, zz, zzz;
} MOTION;

typedef BYTE CLUT[256][256];

#define MAX_PALLETE 16
#define LIGHTMAP_RES 32
typedef BYTE LIGHTMAP[LIGHTMAP_RES][256];
typedef BYTE TEXTURE66[64][64];
typedef BYTE TEXTURE77[128][128];

#define TEXEL66(A,X,Y) *((BYTE*)A+((Y>>10)&(63<<6))+((X>>16)&63))
#define TEXEL77(A,X,Y) *((BYTE*)A+((Y>>9)&(127<<7))+((X>>16)&127))

#define P2D_HIRES_TEXTURES

#ifdef P2D_HIRES_TEXTURES
	#define TEXTURE TEXTURE77
	#define TEXEL TEXEL77
	#define TEXTURE_WIDTH 128
	#define TEXTURE_HEIGHT 128
#else
	#define TEXTURE TEXTURE66
	#define TEXEL TEXEL66
	#define TEXTURE_WIDTH 64
	#define TEXTURE_HEIGHT 64
#endif

#endif
