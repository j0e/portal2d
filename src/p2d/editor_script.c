#include "editor.h"
#include "script.h"
#include "console.h"

/* wall */

// returns (was-selected . (wid1 . <wid2 | nil>)) | nil
static fe_Object* sc_wall_select_at(fe_Context* ctx, fe_Object* arg)
{
	WORD layer = fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	// TODO some sort of 'view_to_client_distance' to translate edview pixels w/ certain zoom to world units
	//      so we can pass in a radius amount instead of having to do it this way.
	fe_Object* xy1 = fe_nextarg(ctx, &arg);
	fe_Object* xy2 = fe_nextarg(ctx, &arg);

	int
	x1 = fe_tonumber(ctx, fe_car(ctx, xy1)), y1 = fe_tonumber(ctx, fe_cdr(ctx, xy1)),
	x2 = fe_tonumber(ctx, fe_car(ctx, xy2)), y2 = fe_tonumber(ctx, fe_cdr(ctx, xy2));

	WORD wid1 = 0, wid2 = 0;

	for (int i = 0; i < MAX_WALL; i++) {
		if (!walls[i].sid) continue;

		if (is_wall_inside(i, x1, y1, x2, y2, layer)) {
			// no need to continue looping through the list after wall is found.
			wid1 = i;
			bool was_selected = hmi_get(&view.selection, wid1); // same as above for sectors
			// check for and handle the portal wall
			if (walls[wid1].port) {
				wid2 = walls[wid1].port;
				was_selected |= (hmi_get(&view.selection, wid2) ? 1 : 0); // are either of the walls selected? (should be both rly)
			}

			hmi_put(&view.selection, wid1, was_selected ? (void*)0 : (void*)1); // stupid ternary bullshit to stop compiler bitching about pointer-int casts when using `(!was_selected)`
			if (wid2) hmi_put(&view.selection, wid2, was_selected ? (void*)0 : (void*)1);

			return fe_cons(ctx, fe_bool(ctx, !was_selected), fe_cons(ctx, fe_number(ctx, wid1), wid2 ? fe_number(ctx, wid2) : fe_null(ctx)));
		}
	}

	return fe_null(ctx);
}

/* sector */

static fe_Object* sc_sector_make(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* points = fe_nextarg(ctx, &arg);
	if (fe_isnil(ctx, points)) return fe_null(ctx);

	WORD layer_id = (WORD)fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	VECTOR v_pts[256]; // TODO - reimpl sector_make in fe to avoid having to reformat args etc
	int n = 0;

	fe_Object* xy;

	do {
		xy = fe_car(ctx, points);
		v_pts[n].x = fe_tonumber(ctx, fe_car(ctx, xy));
		v_pts[n].y = fe_tonumber(ctx, fe_cdr(ctx, xy));
		v_pts[n].z = 0;
		n++;
	} while (!fe_isnil(ctx, points = fe_cdr(ctx, points)));

	WORD sector_id = sector_make(v_pts, n, layer_id, &ed_undos);
	return sector_id ? fe_number(ctx, sector_id) : fe_false(ctx);
}

static fe_Object* sc_sector_extend(fe_Context* ctx, fe_Object* arg)
{
	WORD sid = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	int delta = fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	WORD new_sid = 0;

	if (delta > 0) {
		new_sid = sector_make_above(sid, &ed_undos);
	}
	if (delta < 0) {
		new_sid = sector_make_below(sid, &ed_undos);
	}

	return new_sid ? fe_number(ctx, new_sid) : fe_false(ctx);
}

static fe_Object* sc_sector_split(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* xy1 = fe_nextarg(ctx, &arg);
	fe_Object* xy2 = fe_nextarg(ctx, &arg);

	return fe_bool(ctx, sector_split(
		fe_tonumber(ctx, fe_car(ctx, xy1)), fe_tonumber(ctx, fe_cdr(ctx, xy1)),
		fe_tonumber(ctx, fe_car(ctx, xy2)), fe_tonumber(ctx, fe_cdr(ctx, xy2)),
		fe_tonumber(ctx, fe_nextarg(ctx, &arg)), // layer_id
		&ed_undos
	));
}

static fe_Object* sc_sector_free(fe_Context* ctx, fe_Object* arg)
{
	WORD sid = fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	sector_free(sid, &ed_undos);

	return fe_null(ctx);
}

// adds the sector at the specified layer and point to the selection.
// assuming we're in sectors mode and no overlapping sectors.
// if a sector is found, returns a pair of whether the sector was selected or deselected and the sector_id.
// (in this order because (cons sector_id false) would look like a one-item list.)
// otherwise returns nil.
static fe_Object* sc_sector_select_at(fe_Context* ctx, fe_Object* arg)
{
	WORD layer = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	fe_Object* xy = fe_nextarg(ctx, &arg);
	int x = fe_tonumber(ctx, fe_car(ctx, xy)), y = fe_tonumber(ctx, fe_cdr(ctx, xy));

	bool intersects[MAX_WALL];
	get_line_intersections(intersects, x, y, layer);

	for (int i = 1; i < MAX_SECTOR; i++) {
		if (!sectors[i].lid || sectors[i].lid != layer) {
			continue;
		}

		if (is_inside_sector(i, x, y, intersects)) {
			// no need to continue looping through the list after sector is found.
			bool was_selected = hmi_get(&view.selection, i); // sector already selected? (in which case, deselect)

			hmi_put(&view.selection, i, was_selected ? (void*)0 : (void*)1); // storing plain integers in a void pointer type :^)
			return fe_cons(ctx, fe_bool(ctx, !was_selected), fe_number(ctx, i));
		}
	}

	return fe_null(ctx);
}

/* undo */

// theoretically, these three functions could be callable only from script
// then only functions that perform actions that add to the undos are impl'd in C.
static fe_Object* sc_undo_begin(fe_Context* ctx, fe_Object* arg)
{
	undo_begin(&ed_undos);
	return fe_null(ctx);
}

static fe_Object* sc_undo_commit(fe_Context* ctx, fe_Object* arg)
{
	char buf[128];
	if (!fe_isnil(ctx, arg)) {
		fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));
	}
	undo_commit(&ed_undos, buf); // TODO customisable op name
	return fe_null(ctx);
}

static fe_Object* sc_undo_rollback(fe_Context* ctx, fe_Object* arg)
{
	undo_rollback(&ed_undos);
	return fe_null(ctx);
}

/* selection (meta ???) */

static fe_Object* sc_selection_foreach(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* func = fe_nextarg(ctx, &arg);

	int gc = fe_savegc(ctx);

	hashiiterator_t iter = hmi_iter();
	while (hmi_next(&view.selection, &iter)) if (hmi_get(&view.selection, iter.key)) {
		// TODO possibility of reusing fe objects instead of remaking every time?
		fe_eval(ctx, fe_list(ctx, (fe_Object*[]){ func, fe_number(ctx, iter.key) }, 2));

		fe_restoregc(ctx, gc);
	}

	return fe_bool(ctx, 0);
}

static fe_Object* sc_ed_loadmap(fe_Context* ctx, fe_Object* arg)
{
	char fullpath[1024];
	char mapname[1009]; // :^)

	fe_tostring(ctx, fe_nextarg(ctx, &arg), mapname, sizeof(mapname));
	sprintf(fullpath, "%s/levels/%s.map", curgame, mapname);

	int result = level_load_from_file(fullpath);

	if (result) {
		con_printf("loaded from file %s", fullpath);
		view.redraw = 1;

		// execute map script here
		// first, /*jank BODGE way to*/ check if custom map script exists.
		sprintf(fullpath, "%s/levels/%s.fe", curgame, mapname);
		FILE* fp = fopen(fullpath, "rb");
		if (fp) {
			// map script found, use it.
			fclose(fp);
		} else {
			// map script not found, use default.
			sprintf(fullpath, "%s/leveldefault.fe", curgame);
		}
		int gc = fe_savegc(ctx);

		fe_Object* objs[2];
		objs[0] = fe_symbol(ctx, "include");
		objs[1] = fe_string(ctx, fullpath);

		/*fe_Object* result =*/ fe_eval(ctx, fe_list(ctx, objs, 2));

		fe_restoregc(ctx, gc);
	} else {
		con_printf("could not load level %s", fullpath);
	}

	return fe_bool(ctx, result);
}

static fe_Object* sc_ed_savemap(fe_Context* ctx, fe_Object* arg)
{
	char fullpath[1024];
	char mapname[1009]; // :^)

	fe_tostring(ctx, fe_nextarg(ctx, &arg), mapname, sizeof(mapname));
	sprintf(fullpath, "%s/levels/%s.map", curgame, mapname);

	int result = level_save_to_file(fullpath);

	if (result) {
		con_printf("saved to file %s", fullpath);
		view.redraw = 1;
	} else {
		con_printf("could not save level %s", fullpath);
	}

	return fe_bool(ctx, result);
}

static fe_Object* sc_selection_clear(fe_Context* ctx, fe_Object* arg)
{
	hmi_reinit(&view.selection);

	return fe_false(ctx);
}

static fe_Object* sc_place_camera(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* xy = fe_nextarg(ctx, &arg);
	int x = fe_tonumber(ctx, fe_car(ctx, xy));
	int y = fe_tonumber(ctx, fe_cdr(ctx, xy));

	objects[1].x = x;
	objects[1].y = y;
	// TODO sector_z for new camera z height
}

static fe_Object* sc_undo(fe_Context* ctx, fe_Object* arg)
{
	undo_undo(&ed_undos);
	view.redraw = 1;
	return fe_null(ctx);
}

/******************************************************************************/

// repetitive functions for getting and setting struct properties in script.
// uses slightly cursed xmacros to avoid insane amounts of copy-paste boilerplate.
// this seems the 'least bad' compromise to me atm.
// using a hashtable would be slower and would still require mapping hash key names to properties of separate sectors etc.

// for all of these functions, sets the new value if the argument is provided,
// otherwise returns the old value.
// properly checks the number of arguments, so e.g. `(ed:sector-bot-slopez 123 <nilvalue>)` will throw an error, not just silently return the old value.

// sector 'slopez's are handled separately so we can have validation of the new height
// (i.e. that it wouldn't put the floor above the ceiling or vice versa).
static fe_Object* sc_sector_top_slopez(fe_Context* ctx, fe_Object* arg)
{
	WORD id = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	if (!sectors[id].lid) return fe_null(ctx);
	if (!fe_isnil(ctx, arg)) {
		LONG newvalue = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
		if (newvalue >= sectors[id].bot.slopez) { // newvalue must be above or equal to sector bot.
			undo_add(&ed_undos, undo_sector, (undo_args_t){ .id = id, .sector = sectors[id] });
			sectors[id].top.slopez = newvalue;
		} else {
			con_warning("sector %u new ceiling height %d would be below floor height %d", id, newvalue, sectors[id].bot.slopez);
		}
	}
	return fe_number(ctx, sectors[id].top.slopez);
}

static fe_Object* sc_sector_bot_slopez(fe_Context* ctx, fe_Object* arg)
{
	WORD id = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	if (!sectors[id].lid) return fe_null(ctx);
	if (!fe_isnil(ctx, arg)) {
		LONG newvalue = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
		if (newvalue <= sectors[id].top.slopez) { // newvalue must be below or equal to sector top.
			undo_add(&ed_undos, undo_sector, (undo_args_t){ .id = id, .sector = sectors[id] });
			sectors[id].bot.slopez = newvalue;
		} else {
			con_warning("sector %u new floor height %d would be above ceiling height %d", id, newvalue, sectors[id].top.slopez);
		}
	}
	return fe_number(ctx, sectors[id].bot.slopez);
}

static fe_Object* sc_sector_mid_slopez(fe_Context* ctx, fe_Object* arg)
{
	WORD id = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	if (!sectors[id].lid) return fe_null(ctx);
	if (!fe_isnil(ctx, arg)) {
		LONG newvalue = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
		if (newvalue >= sectors[id].bot.slopez && newvalue <= sectors[id].top.slopez) { // newvalue must be in between sector bot and top
			undo_add(&ed_undos, undo_sector, (undo_args_t){ .id = id, .sector = sectors[id] });
			sectors[id].mid.slopez = newvalue;
		} else {
			con_warning("sector %u new mid height %d would be outside floor-ceiling height %d-%d", id, newvalue, sectors[id].bot.slopez, sectors[id].top.slopez);
		}
	}
	return fe_number(ctx, sectors[id].mid.slopez);
}

// note: value passed to texture, repeatx, repeaty and light functions must be a byte (0-255).
//       current plan is for validation to happen in the UI (so can exit early out of 'textbox value changed' routine)
//       but could validation be done here instead?

// TODO - for panningx and panningy, could handle these as fixed-point decimals in script?

#define AFFINE_LOOP \
	AFFINE(slopex, LONG) \
	AFFINE(slopey, LONG) \
	AFFINE(texture, BYTE) \
	AFFINE(repeatx, BYTE) \
	AFFINE(repeaty, BYTE) \
	AFFINE(panningx, LONG) \
	AFFINE(panningy, LONG) \
	AFFINE(light, BYTE)

#define AFFINE(_key, _type) \
	AFFINE_sector(bot, _key, _type) \
	AFFINE_sector(top, _key, _type) \
	AFFINE_wall(_key, _type)

#define AFFINE_sector(_topbot, _key, _type) \
	static fe_Object* sc_sector_##_topbot##_##_key(fe_Context* ctx, fe_Object* arg) \
	{ \
		WORD sid = fe_tonumber(ctx, fe_nextarg(ctx, &arg)); \
		if (!sectors[sid].lid) return fe_null(ctx); \
		if (!fe_isnil(ctx, arg)) { \
			_type newvalue = fe_tonumber(ctx, fe_nextarg(ctx, &arg)); \
			undo_add(&ed_undos, undo_sector, (undo_args_t){ .id = sid, .sector = sectors[sid] }); \
			sectors[sid]._topbot._key = newvalue; \
		} \
		return fe_number(ctx, sectors[sid]._topbot._key); \
	}

#define AFFINE_wall(_key, _type) \
	static fe_Object* sc_wall_##_key(fe_Context* ctx, fe_Object* arg) \
	{ \
		WORD wid = fe_tonumber(ctx, fe_nextarg(ctx, &arg)); \
		if (!walls[wid].sid) return fe_null(ctx); \
		if (!fe_isnil(ctx, arg)) { \
			_type newvalue = fe_tonumber(ctx, fe_nextarg(ctx, &arg)); \
			undo_add(&ed_undos, undo_wall, (undo_args_t){ .id = wid, .wall = walls[wid] }); \
			walls[wid].surface._key = newvalue; \
		} \
		return fe_number(ctx, walls[wid].surface._key); \
	}

AFFINE_LOOP

#undef AFFINE_sector
#undef AFFINE_wall

// other functions that are unnique to sectors or walls.
static fe_Object* sc_wall_sid(fe_Context* ctx, fe_Object* arg)
{
	WORD wid = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	if (!walls[wid].sid) return fe_null(ctx);
	return fe_number(ctx, walls[wid].sid);
}

/******************************************************************************/

script_regptr editor_regp[] = {
	{ "ed.keys", SP_HASHMAP_I, &editor_keybinds, 0 },
	{}
};

#define AFFINE_sector(_topbot, _key, _type) { "ed:sector-" #_topbot "-" #_key , sc_sector_##_topbot##_##_key },
#define AFFINE_wall(_key, _type) { "ed:wall-" #_key , sc_wall_##_key },

script_regfunc editor_regf[] = {
	{ "ed:undo",              sc_undo              },
	{ "ed:undo-begin",        sc_undo_begin        },
	{ "ed:undo-commit",       sc_undo_commit       },
	{ "ed:undo-rollback",     sc_undo_rollback     },
	{ "ed:selection-foreach", sc_selection_foreach },
	{ "ed:selection-clear",   sc_selection_clear   },
	{ "ed:place-camera",      sc_place_camera      },

	{ "ed:wall-select-at",    sc_wall_select_at    },

	{ "ed:sector-select-at",  sc_sector_select_at  },
	{ "ed:sector-make",       sc_sector_make       },
	{ "ed:sector-extend",     sc_sector_extend     },
	{ "ed:sector-split",      sc_sector_split      },
	{ "ed:sector-free",       sc_sector_free       },

	{ "ed:loadmap",           sc_ed_loadmap        },
	{ "ed:savemap",           sc_ed_savemap        },

	{ "ed:sector-top-slopez", sc_sector_top_slopez },
	{ "ed:sector-bot-slopez", sc_sector_bot_slopez },

	AFFINE_LOOP

	{ "ed:wall-sid", sc_wall_sid },

	{}
};

#undef AFFINE
#undef AFFINE_sector
#undef AFFINE_wall
#undef AFFINE_loop
