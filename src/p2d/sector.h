
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __SECTOR_H__
#define __SECTOR_H__

#include "defs.h"
#include "polygon.h"

#define SECTOR_RENDER_MIDDLE 1

typedef struct
{
	// Persistent Variables. Moving these variables will break the file format!
	WORD    lid;
	WORD    padding;
	WORD    first_wall;
	WORD    flags;
	SURFACE top;
	SURFACE bot;
	SURFACE mid;

	WORD upper_sid;
	WORD lower_sid;

	int reserved;

	// Calculated variables. These variables are not saved out to a file.

	LONG  vis_id;
	RECT  bounds;
	POINT center;
	LONG  locked;
} SECTOR;

WORD sector_find_wall(WORD sid, int x, int y);
int sector_point_inside(WORD sid, int x, int y);
WORD sector_from_point(int x, int y, WORD lid);
void sector_z(WORD sid, int x, int y, int* bot, int* top, int* mid);
void sector_combined_z(WORD sid, int x, int y, int* bot, int* top, int* mid);
void sector_transform(WORD sid, MATRIX matrix);
int sector_extrude_ceiling(WORD sid, POLYGON poly);
int sector_extrude_floor(WORD sid, POLYGON poly);
int sector_extrude_mid(WORD sid, POLYGON poly);
POINT sector_center(WORD sid);
RECT sector_bounds(WORD sid);
int sectors_are_linked(WORD s1, WORD s2);
void sector_calc_vis(OBJECT* camera);
void get_line_intersections(bool intersects[], int x, int y, WORD lid);
bool is_inside_sector(WORD sid, int x, int y, bool intersects[]);

#endif
