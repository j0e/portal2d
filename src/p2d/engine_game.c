#include "editor.h"
#include "engine.h"
#include "palette.h"
#include "script.h"

char curgame[128]; // initialise as 'game' somewhere.

// TEMP BODGE function.
void game_load_palette(PALLETE palette)
{
	char sbuf[2014];
	sprintf(sbuf, "%s/textures/palette.png", curgame);
	palette_load_from_png(palette, sbuf);

	memcpy(ed_palettes[0], palette, sizeof(PALLETE)); // TEMP
}

// game function for loading textures
void game_load_textures(void)
{
	int i;

	char sbuf[1024];

	for (i = 0; i < MAX_TEXTURE; i++) {
		if (textures[i].buffer) {
			picture_destroy(&textures[i]);
		}

		sprintf(sbuf, "%s/textures/%d.png", curgame, i);
		printf("%s\n", sbuf);
		picture_load_from_png(&textures[i], sbuf, 1);

		if (textures[i].buffer) {
			if (textures[i].width != TEXTURE_WIDTH || textures[i].height != TEXTURE_HEIGHT) {
				printf("texture %d should be %dx%d - will be resized\n", i, TEXTURE_WIDTH, TEXTURE_HEIGHT);
				picture_rescale(&textures[i], TEXTURE_WIDTH, TEXTURE_HEIGHT);
				// TODO set surface default x-scale/y-scale at this point?
			}
		}
	}
}

// function for loading/calculating game-specific lightmaps etc?

extern PALLETE palette; // BODGE
void game_set(const char* gamename)
{
	// TODO snprintf etc.
	sprintf(curgame, "%s", gamename);

	printf("Loading game content for game: %s\n", curgame);

	// reload textures and palette
	// these are now done in script - see sc_loadpalette, sc_texturereset, sc_texturereg
	//game_load_palette(palette);
	//game_load_textures();

	// if not in script context, run 'game.fe' game init script here.
	// the script version sc_game_set includes a call to include the game.fe file from fe.
	if (!script_inscript()) {
		char scriptpath[1024];
		sprintf(scriptpath, "%s/game.fe", curgame);

		script_push();
		script_do_file(scriptpath);
		script_pop();
	}
}
