#include "defs.h"
#include "script.h"
#include "engine.h"
#include "editor.h"
#include "include/hashmap.h"
//#include "picture.h"
#include "console.h"
#include <setjmp.h>

static fe_Context* fe_ctx;

////////////////////////////////////////////////////////////////////////////////

void panic_(const char *str, int line, const char *file, const char *func)
{
	fprintf(stderr, "fatal error: %s:%d in %s(): %s\n", file, line, func, str);
	exit(EXIT_FAILURE);
}

void expect_(const char *str, int line, const char *file, const char *func)
{
	char buf[1024];
	sprintf(buf, "assertion failed: \"%s\"", str);
	panic_(buf, line, file, func);
}

#ifdef PLAT_FENSTER
	typedef void* script_mutex;
#endif
#ifdef PLAT_SDL2
	typedef SDL_Mutex* script_mutex;
#endif

script_mutex fe_lock;

void script_mutex_lock()
{
	if (!fe_lock) return;

	#ifdef PLAT_FENSTER
		// no-op lol
	#endif

	#ifdef PLAT_SDL2
		SDL_LockMutex(fe_lock);
	#endif
}

void script_mutex_unlock()
{
	if (!fe_lock) return;

	#ifdef PLAT_FENSTER
		// no-op lol
	#endif

	#ifdef PLAT_SDL2
		SDL_UnlockMutex(fe_lock);
	#endif
}

static int gc = 0;

// semi-BODGE: check if we are in script execution context.
bool script_inscript(void)
{
	return gc != 0;
}

void script_push(void)
{
	script_mutex_lock(fe_lock);
	expect(gc == 0);
	gc = fe_savegc(fe_ctx);
}

void script_pop(void)
{
	fe_restoregc(fe_ctx, gc);
	gc = 0;
	script_mutex_unlock(fe_lock);
}

static jmp_buf error_buf;

static void error_handler(fe_Context* ctx, const char* msg, fe_Object* cl)
{
	char buf[1024];
	char buf2[512];
	sprintf(buf, "error: %s", msg);
	con_error(buf);
	while (!fe_isnil(ctx, cl)) {
		fe_tostring(ctx, fe_car(ctx, cl), buf2, sizeof(buf2));
		sprintf(buf, "=> %s", buf2);
		con_error(buf);
		cl = fe_cdr(ctx, cl);
	}
	longjmp(error_buf, 1);
}

static fe_Object* do_(fe_Object* (*fn)(fe_Context*, const char *), const char* str, const char* err)
{
	fe_Object* res = NULL;
	fe_ErrorFn oldfn = fe_handlers(fe_ctx)->error;
	fe_handlers(fe_ctx)->error = error_handler;
	if (setjmp(error_buf) == 0) {
		res = fn(fe_ctx, str);
		if (!res) { fe_error(fe_ctx, err); }
	}
	fe_handlers(fe_ctx)->error = oldfn;
	return res;
}

fe_Object* script_do_string(const char *str)
{
	expect(gc);
	return do_(fex_do_string, str, "failed to do string");
}

// slight BODGE
fe_Object* script_do_string_uncaught(const char *str)
{
	expect(gc);
	return fex_do_string(fe_ctx, str);
}

fe_Object* script_do_file(const char *filename)
{
	expect(gc);
	char err_buf[128];
	sprintf(err_buf, "failed to do file '%.64s'", filename);
	return do_(fex_do_file, filename, err_buf);
}

// functions for custom pointer types
// TODO: fe 'mark' and 'gc' handler functions.
//       will garbage collect fe-allocated objects and preserve any that were allocated (statically or dynamically) in C.

static const char *typenames[] = {
	"pair", "free", "nil", "number", "symbol", "string",
	"func", "macro", "prim", "cfunc", "ptr"
};
static const char* ptr_typenames[] = {
	"none(?)",
	"int", "float",
	"int32", "uint8", "uint16", "uint32",
	"sector", "wall",
	"hashmap string keys", "hashmap integer keys",
	"?", "?", "?", "?"
};

void* script_checkptrtype(fe_Context* ctx, fe_Object* obj, script_type type)
{
	// first check it's a PTR
	int obj_type = fe_type(ctx, obj);
	if (obj_type != FE_TPTR) {
		fex_errorf(ctx, "object is not a ptr (is %s)", typenames[obj_type]);
	}

	// now check its own type
	// assuming (as should always be the case) that the obj ptr is a script_ptr.
	// UNDEFINED BEHAVIOUR otherwise. :^)

	script_ptr* ptr = fe_toptr(ctx, obj);
	if (ptr->type != type) {
		fex_errorf(ctx, "ptr is not a %s (is %s)", type, ptr->type >= SP_NONE && ptr->type < SP_MAX ? ptr_typenames[ptr->type] : "WILDLY out of range");
	}

	return ptr->ptr;
}

// BODGE
script_ptr* script_checkptrtype2(fe_Context* ctx, fe_Object* obj, script_type type1, script_type type2)
{
	// first check it's a PTR
	int obj_type = fe_type(ctx, obj);
	if (obj_type != FE_TPTR) {
		fex_errorf(ctx, "object is not a ptr (is %s)", typenames[obj_type]);
	}

	script_ptr* ptr = fe_toptr(ctx, obj);
	if (ptr->type != type1 && ptr->type != type2) {
		fex_errorf(ctx, "ptr is not a %s or %s (is %s)", typenames[type1], typenames[type2], ptr->type >= SP_NONE && ptr->type < SP_MAX ? ptr_typenames[ptr->type] : "WILDLY out of range");
	}

	return ptr;
}

// BODGE(?)
int script_tostring(fe_Object* obj, char *dst, int size)
{
	return fe_tostring(fe_ctx, obj, dst, size);
}

////////////////////////////////////////////////////////////////////////////////

// public API

static fe_Object* sc_screenres(fe_Context* ctx, fe_Object* arg)
{
	if (plat_framebuffer) {
		con_printf("cannot set screen resolution while framebuffer is active");
		return fe_bool(ctx, 0);
	}

	int width = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	int height = fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	if (width < 320 || height < 240) {
		con_printf("minimum resolution is 320x240 (attempted %dx%d)", width, height);
		return fe_bool(ctx, 0);
	}

	plat_screen_w = width;
	plat_screen_h = height;

	framebuffer_w = width;
	framebuffer_h = height;

	return fe_bool(ctx, 0);
}

static fe_Object* sc_type(fe_Context* ctx, fe_Object* arg)
{
	int type = fe_type(ctx, fe_nextarg(ctx, &arg));
	if (type > FE_TPTR) {
		return fe_string(ctx, "?");
	}
	return fe_string(ctx, typenames[type]);
}

static fe_Object* sc_echo(fe_Context* ctx, fe_Object* arg)
{
	char buf[2048];
	int n = 0;
	while (!fe_isnil(ctx, arg)) {
		n += fe_tostring(ctx, fe_nextarg(ctx, &arg), &buf[n], sizeof(buf) - n);
	}
	con_printf(buf);
	return fe_bool(ctx, false);
}

static fe_Object* sc_error(fe_Context* ctx, fe_Object* arg)
{
	char str[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), str, sizeof(str));
	fe_error(ctx, str);
	return NULL; // never reached
}

static fe_Object* sc_string(fe_Context* ctx, fe_Object* arg)
{
	char buf[1024]; // TODO realloc if needed
	int n = 0;
	while (!fe_isnil(ctx, arg)) {
		n += fe_tostring(ctx, fe_nextarg(ctx, &arg), &buf[n], sizeof(buf) - n);
	}
	return fe_string(ctx, buf);
}

static fe_Object* sc_strlen(fe_Context* ctx, fe_Object* arg)
{
	// TODO: do this function properly! by 'iterating' through the string object and counting up strlens of each part
	char buf[1024];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), buf, sizeof(buf));
	return fe_number(ctx, strlen(buf));
}

static fe_Object* sc_exec(fe_Context* ctx, fe_Object* arg)
{
	char filename[1024];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), filename, sizeof(filename));
	fe_Object* result = fex_do_file(ctx, filename);
	if (result) {
		return result;
	} else {
		//fex_errorf("failed to do file '%.64s'", filename); // <- this still segfaults... the below is good enough for now
		fprintf(stderr, "failed to do file '%.64s'\n", filename); // TODO: console error and/or fe_error
		return fe_bool(ctx, false);
	}
}

static fe_Object* sc_ord(fe_Context *ctx, fe_Object *arg)
{
	static char buf[2] = {0};
	fe_tostring(ctx, fe_nextarg(ctx, &arg), (char *)&buf, sizeof(buf));
	return fe_number(ctx, (float)buf[0]);
}

static fe_Object* sc_chr(fe_Context *ctx, fe_Object *arg)
{
	uint8_t num = (uint8_t)fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	char buf[2] = {num, 0};
	return fe_string(ctx, buf);
}

// TODO generalise min, max, sl, sr, etc. to more than 2 operands
static fe_Object* sc_min(fe_Context *ctx, fe_Object *arg)
{
	fe_Number a = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	fe_Number b = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	return fe_number(ctx, a<b ? a : b);
}

static fe_Object* sc_max(fe_Context *ctx, fe_Object *arg)
{
	fe_Number a = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	fe_Number b = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	return fe_number(ctx, a>b ? a : b);
}

static fe_Object* sc_clamp(fe_Context *ctx, fe_Object *arg)
{
	fe_Number a = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	fe_Number b = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	fe_Number c = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	return fe_number(ctx, a>b ? (a<c ? a : c) : (b<c ? b : c));
}

static fe_Object* sc_sl(fe_Context *ctx, fe_Object *arg)
{
	return fe_number(ctx, (int)fe_tonumber(ctx, fe_nextarg(ctx, &arg)) << (int)fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
}

static fe_Object* sc_sr(fe_Context *ctx, fe_Object *arg)
{
	return fe_number(ctx, (int)fe_tonumber(ctx, fe_nextarg(ctx, &arg)) >> (int)fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
}

// quick mod-down functions
static fe_Object* sc_mod_down_ctrl (fe_Context *ctx, fe_Object *arg) { return fe_bool(ctx, MOD_DOWN(MOD_CTRL )); }
static fe_Object* sc_mod_down_shift(fe_Context *ctx, fe_Object *arg) { return fe_bool(ctx, MOD_DOWN(MOD_SHIFT)); }
static fe_Object* sc_mod_down_alt  (fe_Context *ctx, fe_Object *arg) { return fe_bool(ctx, MOD_DOWN(MOD_ALT  )); }
static fe_Object* sc_mod_down_meta (fe_Context *ctx, fe_Object *arg) { return fe_bool(ctx, MOD_DOWN(MOD_META )); }

static fe_Object* sc_hsnew(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = malloc(sizeof(script_ptr));
	ptr->type = SP_HASHMAP_S;

	hashmap_t* hashmap = malloc(sizeof(hashmap_t));
	memset(hashmap, 0, sizeof(hashmap_t));
	ptr->ptr = hashmap;

	return fe_ptr(ctx, ptr);
}

static fe_Object* sc_hinew(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = malloc(sizeof(script_ptr));
	ptr->type = SP_HASHMAP_I;

	hashmap_t* hashmap = malloc(sizeof(hashmap_t));
	memset(hashmap, 0, sizeof(hashmap_t));
	ptr->ptr = hashmap;

	return fe_ptr(ctx, ptr);
}

// TODO: currently no way to unset from hashmap!
static fe_Object* sc_hset(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = script_checkptrtype2(ctx, fe_nextarg(ctx, &arg), SP_HASHMAP_S, SP_HASHMAP_I);

	fe_Object* key_obj = fe_nextarg(ctx, &arg);
	fe_Object* value = fe_nextarg(ctx, &arg);

	switch (ptr->type) {
		case SP_HASHMAP_S:
			// string keys.
			char key_s[256];
			fe_tostring(ctx, key_obj, key_s, sizeof(key_s));
			hm_put(ptr->ptr, key_s, value);
			break;

		case SP_HASHMAP_I:
			// integer keys.
			// NOTE: will need to mod fe to use a different number type than float
			//       in order to use full range of 32-bit uint hash keys.
			uint32_t key_i = (uint32_t)fe_tonumber(ctx, key_obj);
			hmi_put(ptr->ptr, key_i, value);
			break;
	}

	// return the object or the table? While we decide, return neither lol
	return fe_bool(ctx, 0);
}

static fe_Object* sc_hget(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = script_checkptrtype2(ctx, fe_nextarg(ctx, &arg), SP_HASHMAP_S, SP_HASHMAP_I);
	fe_Object* key_obj = fe_nextarg(ctx, &arg);

	fe_Object* value;

	switch (ptr->type) {
		case SP_HASHMAP_S:
			// string keys.
			char key_s[256];
			fe_tostring(ctx, key_obj, key_s, sizeof(key_s));
			value = hm_get(ptr->ptr, key_s);
			break;

		case SP_HASHMAP_I:
			// integer keys.
			uint32_t key_i = (uint32_t)fe_tonumber(ctx, key_obj);
			value = hmi_get(ptr->ptr, key_i);
			break;
	}

	return value ? value : fe_bool(ctx, 0);
}

static fe_Object* sc_hhas(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = script_checkptrtype2(ctx, fe_nextarg(ctx, &arg), SP_HASHMAP_S, SP_HASHMAP_I);
	fe_Object* key_obj = fe_nextarg(ctx, &arg);

	switch (ptr->type) {
		case SP_HASHMAP_S:
			// string keys.
			char key_s[256];
			fe_tostring(ctx, key_obj, key_s, sizeof(key_s));
			return fe_bool(ctx, hm_has(ptr->ptr, key_s));

		case SP_HASHMAP_I:
			// integer keys.
			uint32_t key_i = (uint32_t)fe_tonumber(ctx, fe_nextarg(ctx, &arg));
			return fe_bool(ctx, hmi_has(ptr->ptr, key_i));
	}
}

////////////////////////////////////////////////////////////////////////////////

#define AFFINE_LOOP \
	AFFINE(SP_INT,   int) \
	AFFINE(SP_FLOAT, float) \
	AFFINE(SP_LONG,  LONG) \
	AFFINE(SP_BYTE,  BYTE) \
	AFFINE(SP_WORD,  WORD) \
	AFFINE(SP_DWORD, DWORD)

// TODO macro for casting script pointer value pointer values.
//      also slightly redundant type function just for interface consistency. :)
//      SPTR is of type `script_ptr*`.
#define SP_type(SPTR) ( (SPTR)->type )
#define SP_value(SPTR, TYPE) ( *(TYPE*)(SPTR)->ptr )

static fe_Object* sc_en_get(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* sptr = fe_toptr(ctx, fe_nextarg(ctx, &arg));

	switch (sptr->type) {
		#define AFFINE(SP_TYPE, CAST_TYPE) case SP_TYPE: return fe_number(ctx, *(CAST_TYPE*)sptr->ptr);
			AFFINE_LOOP
		#undef AFFINE
		case SP_BOOL: return fe_bool(ctx, *(bool*)sptr->ptr);
		case SP_FIXED: return fe_number(ctx, f2fl(*(int*)sptr->ptr));
	}

	fex_errorf(ctx, "ptr must be scalar type, is %s", ptr_typenames[sptr->type]);
}

static fe_Object* sc_en_set(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* sptr = fe_toptr(ctx, fe_nextarg(ctx, &arg));

	if (SP_is_readonly(sptr)) {
		fex_errorf(ctx, "cannot change value of read-only ptr");
	}

	switch (sptr->type) { // TODO 'bounds checking' etc
		#define AFFINE(SP_TYPE, CAST_TYPE) case SP_TYPE: return fe_number(ctx, *(CAST_TYPE*)sptr->ptr = (CAST_TYPE)fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
			AFFINE_LOOP
		#undef AFFINE
		case SP_BOOL: return fe_bool(ctx, *(bool*)sptr->ptr = !fe_isnil(ctx, fe_nextarg(ctx, &arg)));
		case SP_FIXED: return fe_number(ctx, f2fl(*(int*)sptr->ptr = (int)fl2f(fe_tonumber(ctx, fe_nextarg(ctx, &arg)))));
		//case SP_FIXED: return fe_number(ctx, f2fl(SP_value(sptr, int) = (int)fl2f(fe_tonumber(ctx, fe_nextarg(ctx, &arg)))));
	}

	fex_errorf(ctx, "ptr must be scalar type, is %s", ptr_typenames[sptr->type]);
}

static fe_Object* sc_en_add(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* sptr = fe_toptr(ctx, fe_nextarg(ctx, &arg));

	if (SP_is_readonly(sptr)) {
		fex_errorf(ctx, "cannot change value of read-only ptr");
	}

	switch (sptr->type) { // TODO 'bounds checking' etc
		#define AFFINE(SP_TYPE, CAST_TYPE) case SP_TYPE: return fe_number(ctx, *(CAST_TYPE*)sptr->ptr += (CAST_TYPE)fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
			AFFINE_LOOP
		#undef AFFINE
		case SP_FIXED: return fe_number(ctx, f2fl(*(int*)sptr->ptr += fl2f(fe_tonumber(ctx, fe_nextarg(ctx, &arg)))));
	}

	fex_errorf(ctx, "ptr must be number type, is %s", ptr_typenames[sptr->type]);
}

static fe_Object* sc_en_subtract(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* sptr = fe_toptr(ctx, fe_nextarg(ctx, &arg));

	if (SP_is_readonly(sptr)) {
		fex_errorf(ctx, "cannot change value of read-only ptr");
	}

	switch (sptr->type) { // TODO 'bounds checking' etc
		#define AFFINE(SP_TYPE, CAST_TYPE) case SP_TYPE: return fe_number(ctx, *(CAST_TYPE*)sptr->ptr -= (CAST_TYPE)fe_tonumber(ctx, fe_nextarg(ctx, &arg)));
			AFFINE_LOOP
		#undef AFFINE
		case SP_FIXED: return fe_number(ctx, f2fl(*(int*)sptr->ptr -= fl2f(fe_tonumber(ctx, fe_nextarg(ctx, &arg)))));
	}

	fex_errorf(ctx, "ptr must be number type, is %s", ptr_typenames[sptr->type]);
}

#undef AFFINE_LOOP

////////////////////////////////////////////////////////////////////////////////

// TODO refactor this - currently used for passing editor keybind functions to run in static fe_ctx
void script_keypress(fe_Object* func)
{
	if (fe_isnil(fe_ctx, func)) return; // TODO other types as well
	// TODO doing it like this means errors aren't handled - instead use 'script_do_string' (see aq) or similar way to set error handler
	int gc = fe_savegc(fe_ctx);
	fe_eval(fe_ctx, fe_list(fe_ctx, (fe_Object*[]){ func }, 1));
	fe_restoregc(fe_ctx, gc);
}

////////////////////////////////////////////////////////////////////////////////

static fe_Object* script_mark(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = fe_toptr(ctx, arg); // TODO or fe_nextarg(ctx, &arg)?

	switch (ptr->type) {
		case SP_HASHMAP_S: {
			hashmap_t* hashmap = ptr->ptr;
			hashiterator_t iterator = hm_iter();
			
			while (hm_next(hashmap, &iterator)) {
				fe_mark(ctx, hm_get(hashmap, iterator.key));
			}
		}	break;
			
		case SP_HASHMAP_I: {
			hashimap_t* hashimap = ptr->ptr;
			hashiiterator_t iterator = hmi_iter();

			while (hmi_next(hashimap, &iterator)) {
				fe_mark(ctx, hmi_get(hashimap, iterator.key));
			}
		}	break;
	}

	return NULL; // TODO - is this correct? seems not right to return a new fe_bool in the garbage collector
}

static fe_Object* script_gc(fe_Context* ctx, fe_Object* arg)
{
	script_ptr* ptr = fe_toptr(ctx, arg); // TODO or fe_nextarg(ctx, &arg)?

	// TODO handling (i.e. free()) of 'dynamic' script ptrs when needed

	switch (ptr->type) {
		case SP_HASHMAP_S:
			hashmap_t* hashmap = ptr->ptr;
			// frees all keys and hash entries --
			// does not free values, but as they are fe_Objects they will be added to the fe freelist (unless they're also referenced elsewhere).
			hm_destroy(hashmap);
			free(hashmap);
			ptr->ptr = NULL;
			free(ptr); // TODO - possible to nullify the fe_ptr object's ptr address?
			break;

		case SP_HASHMAP_I:
			hashimap_t* hashimap = ptr->ptr;
			hmi_destroy(hashimap);
			free(hashimap);
			ptr->ptr = NULL;
			free(ptr);
			break;
	}

	return NULL;
}

// modded from fex
void script_register(fe_Context *ctx, script_regptr* ptrs, script_regfunc* funcs)
{
	int gc = fe_savegc(ctx);
	int i;

	if (ptrs) for (i = 0; ptrs[i].name; i++) {
		fe_set(ctx, fe_symbol(ctx, ptrs[i].name), fe_ptr(ctx, (script_ptr*)(&ptrs[i].type)));
		fe_restoregc(ctx, gc);
	}

	if (funcs) for (i = 0; funcs[i].name; i++) {
		fe_set(ctx, fe_symbol(ctx, funcs[i].name), fe_cfunc(ctx, funcs[i].fn));
		fe_restoregc(ctx, gc);
	}
}

static void *fe_data;

void script_init(void)
{
	int fe_bytes = 1024 * 1024; // TODO configurable?

	fe_data = malloc(fe_bytes);

	fe_ctx = fe_open(fe_data, fe_bytes);

	fe_handlers(fe_ctx)->mark = script_mark;
	fe_handlers(fe_ctx)->gc = script_gc;

	// TODO init SDL mutex :^] (use pthreads mutex for non-SDL platforms?)

	// register script functions
	script_register(fe_ctx,
		(script_regptr[]){
			{}
		},
		(script_regfunc[]){
			{ "screenres",      sc_screenres },
			{ "echo",           sc_echo },
			{ "error",          sc_error },
			{ "str",            sc_string },
			{ "include",        sc_exec },
			{ "type",           sc_type },
			{ "chr",            sc_chr },
			{ "ord",            sc_ord },
			{ "strlen",         sc_strlen },
			{ "min",            sc_min },
			{ "max",            sc_max },
			{ "clamp",          sc_clamp },
			{ "<<",             sc_sl },
			{ ">>",             sc_sr },

			{ "mod-down:ctrl",  sc_mod_down_ctrl },
			{ "mod-down:shift", sc_mod_down_shift },
			{ "mod-down:alt",   sc_mod_down_alt },
			{ "mod-down:meta",  sc_mod_down_meta },

			{ "hsnew",          sc_hsnew },
			{ "hinew",          sc_hinew },
			{ "hget",           sc_hget },
			{ "hset",           sc_hset },
			{ "hhas",           sc_hhas },

			{ "$",              sc_en_get },
			{ "$=",             sc_en_set },
			{ "$+=",            sc_en_add },
			{ "$-=",            sc_en_subtract },
			{}
		}
	);

	#define REG(name) extern script_regptr name##_regp[]; extern script_regfunc name##_regf[]; script_register(fe_ctx, name##_regp, name##_regf)
		REG(editor);
		REG(ed_ui);
		REG(ed_view);
		REG(engine);
	#undef REG

	script_push();
	script_do_file("script/init.fe");
	script_pop();
}

void script_deinit(void)
{
	fe_close(fe_ctx);
	free(fe_data);
}
