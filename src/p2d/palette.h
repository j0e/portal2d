
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __PALETTE_H__
#define __PALETTE_H__

#include "defs.h"

typedef BYTE COLOUR[3];
typedef COLOUR PALLETE[256];

// TODO put CLUT, LIGHTMAP, etc. in here too?

void palette_load_from_raw(PALLETE palette, const char* filename);
void palette_load_from_png(PALLETE palette, const char *filename);

BYTE palette_search(PALLETE palette, int r, int g, int b);

void lightmap_create(LIGHTMAP lightmap, PALLETE palette, int brightness, int gamma);
void blender_create(CLUT lut, PALLETE palette);
void blender_create_fixalpha(CLUT lut, PALLETE palette, int src_alpha);
void generate_colormap(LIGHTMAP out_colormap, PALLETE palette);

#endif
