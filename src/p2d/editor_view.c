#include "editor.h"
#include "maths.h"
#include "script.h"
#include "console.h"

// 2d map view microui widget.
// TODO 'refactor' into 'ui_mapview' and make into two 'parts':
//      1. a generic set of functions for drawing a 2d view of a map onto a PICTURE struct at any bpp
//      2. microui widget wrapper around that for the editor, plus editor-specific view stuff
WORD edview_layer;
int ed_view_mx, ed_view_my; // last mouse coordinates relative to top-left of view window
bool ed_view_hover;

static uint32_t colours[EC_MAX] = {
	[EC_BACKGROUND]  = 0x000000,
	[EC_WALL]        = 0xffffff,
	[EC_WALL_SEL]    = 0xff0000,
	[EC_WALL_NEW]    = 0xff00ff,
	[EC_VERTEX]      = 0x8080ff,
	[EC_VERTEX_SEL]  = 0xffcc66,
	[EC_VERTEX_NEW]  = 0xffcc66,

	[EC_SEL_RECT]    = 0x00ffff,
	[EC_GRID_LINE]   = 0x0000ff,
	[EC_GRID_ORIGIN] = 0x0040ff,

	[EC_CAMERA]      = 0xffcc00,
};

void ed_view_setcolour(int i, uint8_t r, uint8_t g, uint8_t b)
{
	if (i < 0 || i >= EC_MAX) {
		fprintf(stderr, "edview colour index %d out of range\n", i);
		return;
	}

	colours[i] = (r << 16) | (g << 8) | b;
}

uint32_t rgb8_to_rgb32(uint8_t red, uint8_t green, uint8_t blue) {
		return ((uint32_t)red << 16) | ((uint32_t)green << 8) | blue;
}

static void view_to_client_snap(EDVIEW* view, int* x, int* y)
{
	float scale = view->zoom * view->grid / view->zoom_div;

	*x = (int)floor( (*x - (view->buf.width  / 2.0) + (view->x * view->zoom / view->zoom_div)) / scale + 0.5) * view->grid;
	*y = (int)floor(-(*y - (view->buf.height / 2.0) - (view->y * view->zoom / view->zoom_div)) / scale + 0.5) * view->grid;
}

static void view_to_client_free(EDVIEW* view, int* x, int* y)
{
	float scale = (float)view->zoom / (float)view->zoom_div;
	*x = (int)floor( (*x - ((float)view->buf.width  / 2.0)) / scale + 0.5) + view->x;
	*y = (int)floor(-(*y - ((float)view->buf.height / 2.0)) / scale + 0.5) + view->y;
}

static void client_to_view(EDVIEW* view, int* x, int* y)
{
	*x =   imuldiv(*x - view->x, view->zoom, view->zoom_div) + (view->buf.width >> 1);
	*y = - imuldiv(*y - view->y, view->zoom, view->zoom_div) + (view->buf.height >> 1);
}

static void line(PICTURE* pic, int x1, int y1, int x2, int y2, uint32_t c)
{
	int dx = abs(x2 - x1), sx = x1 < x2 ? 1 : -1;
	int dy = abs(y2 - y1), sy = y1 < y2 ? 1 : -1;
	int err = (dx > dy ? dx : -dy) / 2, e2;
	for (;;) {
		if (x1 >= 0 && x1 < pic->width && y1 >= 0 && y1 < pic->height) pic->scanlines.l[y1][x1] = c;
		if (x1 == x2 && y1 == y2) {
			break;
		}
		e2 = err;
		if (e2 > -dx) {
			err -= dy;
			x1 += sx;
		}
		if (e2 < dy) {
			err += dx;
			y1 += sy;
		}
	}
}

static void fillrect(PICTURE* pic, int x1, int y1, int x2, int y2, uint32_t c)
{
	if (x1 < 0) x1 = 0;
	if (y1 < 0) y1 = 0;
	if (x2 >= pic->width) x2 = pic->width;
	if (y2 >= pic->height) y2 = pic->height;

	int row, col;
	for (row = y1; row < y2; row++) {
		for (col = x1; col < x2; col++) {
			pic->scanlines.l[row][col] = c;
		}
	}
}

static void linerect(PICTURE* pic, int x1, int y1, int x2, int y2, uint32_t c)
{
	x2-=1; y2-=1;
	
	line(pic, x1, y1, x2, y1, c);
	line(pic, x1, y1, x1, y2, c);
	line(pic, x2, y1, x2, y2, c);
	line(pic, x1, y2, x2, y2, c);
}

void draw_vertex(PICTURE* pic, int x, int y, int type, int size)
{
	uint32_t colour;
	switch (type) { // TODO proper constants for this or something
		case 0:
			colour = colours[EC_VERTEX];
			break;
		case 1:
			colour = colours[EC_VERTEX_SEL];
			break;
		case 2:
			colour = colours[EC_VERTEX_NEW];
			break;
	}
	if (size) {
		linerect(pic, x - size, y - size, x + size + 1, y + size + 1, colour);
	}
}

static void draw_sector(EDVIEW* view, WORD sid, SECTOR* sector, int draw_verts, EDITORMODE drawmode /* BODGE */)
{
	int x1, y1, x2, y2;
	int colour;

	if (!sector) {
		sector = &sectors[sid];
	}

	if (!sector->lid) {
		return;
	}

	// draw sector walls
	int wid = sector->first_wall;

	do {
		wall_line(wid, &x1, &y1, &x2, &y2);
		client_to_view(view, &x1, &y1);
		client_to_view(view, &x2, &y2);

		// TODO if x1 y1 x2 y2 are out of bounds: continue

		colour = colours[EC_WALL];

		if (
			(
				drawmode == EM_WALLS
				&& ( hmi_get(&view->selection, wid) || (walls[wid].port && hmi_get(&view->selection, walls[wid].port)) )
			)
			|| (
				drawmode == EM_SECTORS
				&& ( hmi_get(&view->selection, sid) || (walls[wid].port && hmi_get(&view->selection, walls[walls[wid].port].sid)) )
			)
		) {
			colour = colours[EC_WALL_SEL];
		}

		if (walls[wid].port) {
			colour = rgb8_to_rgb32(rgb32_red(colour) / 2, rgb32_green(colour) / 2, rgb32_blue(colour) / 2);
		}

		if (sector->lid != edview_layer) {
			colour = rgb8_to_rgb32(rgb32_red(colour) / 3, rgb32_green(colour) / 3, rgb32_blue(colour) / 3);
		}

		line(&view->buf, x1, y1, x2, y2, colour);
	} while ((wid = NEXT_WALL(wid)) != sector->first_wall);

	if (draw_verts) {
		// draw sector vertices on top of walls
		wid = sector->first_wall;

		do {
			x1 = walls[wid].x;
			y1 = walls[wid].y;
			client_to_view(view, &x1, &y1);

			// TODO if x1 y1 are out of bounds: continue

			draw_vertex(&view->buf, x1, y1, 0, view->zoom / 8);
		} while ((wid = NEXT_WALL(wid)) != sector->first_wall);
	}
}

void ed_view_draw(EDVIEW* view, fe_Context* ctx)
{
	int i, x1, x2, x, xx, y1, y2, y, yy, sid;
	uint32_t colour;

	int draw_background = 1;
	if (draw_background) {
		fillrect(&view->buf, 0, 0, view->buf.width, view->buf.height, colours[EC_BACKGROUND]);

		x1 = 0; xx = imuldiv(view->zoom, view->grid, view->zoom_div);
		y1 = 0; yy = imuldiv(view->zoom, view->grid, view->zoom_div);

		view_to_client_snap(view, &x1, &y1);
		client_to_view(view, &x1, &y1);

		//x1 -= imuldiv(view->x, view->zoom, view->zoom_div);
		//y1 += imuldiv(view->y, view->zoom, view->zoom_div);

		if (xx > 0 && yy > 0) {
			while (x1 < 0) x1 += xx;
			while (y1 < 0) y1 += yy;

			for (y = y1; y < view->buf.height; y += yy)
				for (x = x1; x < view->buf.width; x += xx)
					view->buf.scanlines.l[y][x] = colours[EC_GRID_LINE]; // TODO! actual grid lines instead of / as well as dots
		}

		x = y = 0;
		client_to_view(view, &x, &y);

		line(&view->buf, 0, y, view->buf.width, y,                colours[EC_GRID_ORIGIN]);
		line(&view->buf, x, 0, x,               view->buf.height, colours[EC_GRID_ORIGIN]);
	}

	// BODGE
	EDITORMODE drawmode = EM_NONE;
	fe_Object* mode_obj = fe_eval(ctx, fe_symbol(ctx, "ed.mode"));
	if (!fe_isnil(ctx, mode_obj)) {
		char buf[2];
		fe_tostring(ctx, mode_obj, buf, sizeof(buf));
		switch (*buf) {
			case 'w': drawmode = EM_WALLS;   break;
			case 's': drawmode = EM_SECTORS; break;
		}
	}

	// TODO each of these vars local to edview or the level data structure etc. & passed thru as func args
	int thislayer[MAX_SECTOR];
	int thislayer_n = 0;

	for (sid = 0; sid < MAX_SECTOR; sid++)
	{
		SECTOR* sector = &sectors[sid];

		if (sector->lid)
		{
			if (sector->lid == edview_layer) {
				thislayer[thislayer_n++] = sid;
			} else {
				// TODO option to draw layers only above or only beneath current layer
				draw_sector(view, sid, sector, 0, drawmode);
			}
		}
	}

	if (thislayer_n) {
		for (i = 0; i < thislayer_n; i++) {
			sid = thislayer[i];
			draw_sector(view, sid, &sectors[sid], 1, drawmode);
		}
	}

	fe_Object* new_walls = fe_eval(ctx, fe_symbol(ctx, "ed.new-walls"));

	if (!fe_isnil(ctx, new_walls)) {
		fe_Object* first_new_wall = new_walls; // save first wall for further down

		// draw new walls' lines
		fe_Object* xy = fe_car(ctx, new_walls);
		x1 = fe_tonumber(ctx, fe_car(ctx, xy));
		y1 = fe_tonumber(ctx, fe_cdr(ctx, xy));
		client_to_view(view, &x1, &y1);

		while (!fe_isnil(ctx, new_walls = fe_cdr(ctx, new_walls))) {
			xy = fe_car(ctx, new_walls);
			x2 = fe_tonumber(ctx, fe_car(ctx, xy));
			y2 = fe_tonumber(ctx, fe_cdr(ctx, xy));
			client_to_view(view, &x2, &y2);

			line(&view->buf, x1, y1, x2, y2, colours[EC_WALL_NEW]);

			x1 = x2;
			y1 = y2;
		}

		// draw new walls' vertices
		new_walls = first_new_wall;
		do {
			xy = fe_car(ctx, new_walls);
			x1 = fe_tonumber(ctx, fe_car(ctx, xy));
			y1 = fe_tonumber(ctx, fe_cdr(ctx, xy));
			client_to_view(view, &x1, &y1);

			draw_vertex(&view->buf, x1, y1, 2, view->zoom >= 8);
		} while (!fe_isnil(ctx, new_walls = fe_cdr(ctx, new_walls)));
	}

	/*
	if (number_of_new_walls) {
		// draw new wall lines
		x1 = new_walls[0].x;
		y1 = new_walls[0].y;
		client_to_view(view, &x1, &y1);

		for (i = 1; i < number_of_new_walls; i++) {
			x2 = new_walls[i].x;
			y2 = new_walls[i].y;
			client_to_view(view, &x2, &y2);

			line(&view->buf, x1, y1, x2, y2, colours[EC_WALL_NEW]);

			x1 = x2;
			y1 = y2;
		}

		// draw new wall vertices
		for (i = 0; i < number_of_new_walls; i++) {
			x1 = new_walls[i].x;
			y1 = new_walls[i].y;
			client_to_view(view, &x1, &y1);
			draw_vertex(&view->buf, x1, y1, 2, view->zoom >= 8);
		}
	}
	*/

	// draw camera
	x1 = objects[1].x;
	y1 = objects[1].y;
	client_to_view(view, &x1, &y1);
	linerect(&view->buf, x1-4, y1-4, x1+5, y1+5, colours[EC_CAMERA]);
}

/******************************************************************************/

void ed_view_init(EDVIEW* view, int width, int height)
{
	view->zoom     = 16;
	view->zoom_div = WORLD_UNIT;
	view->grid     = WORLD_UNIT * 4;
	picture_create(&view->buf, width, height, 32, 0, 0);
	hmi_init(&view->selection);

	//ed_view_draw(view); // TODO refactor to somewhere with fe_Context
}

void ed_view_deinit(EDVIEW* view)
{
	picture_destroy(&view->buf);
	view->buf.buffer = 0;
	hmi_destroy(&view->selection);
}

void ed_view_update(mu_Context* ctx, EDVIEW* view, fe_Context* fe_ctx /* BODGE */)
{
	mu_Id id = mu_get_id(ctx, &view->buf, sizeof(view->buf));
	mu_Rect rect = mu_layout_next(ctx);
	mu_update_control(ctx, id, rect, 0 /* | MU_OPT_HOLDFOCUS */);

	/* input */
	ed_view_mx = ctx->mouse_pos.x - rect.x;
	ed_view_my = ctx->mouse_pos.y - rect.y;

	if (ctx->hover == id) {
		ed_view_hover = 1;

		/* MOUSE */

		// TODO - BODGE!!! do these properly!
		if (ctx->mouse_pressed) {
			char mousefunc_str[64];
			int i = sprintf(mousefunc_str, "(ed:view-mousepress %d %d 0)", ed_view_mx, ed_view_my) - 2;

			if (ctx->mouse_pressed & MU_MOUSE_LEFT) {
				mousefunc_str[i] = '1';
				script_do_string(mousefunc_str);
			}
			if (ctx->mouse_pressed & MU_MOUSE_RIGHT) {
				mousefunc_str[i] = '2';
				script_do_string(mousefunc_str);
			}
			if (ctx->mouse_pressed & MU_MOUSE_MIDDLE) {
				mousefunc_str[i] = '3';
				script_do_string(mousefunc_str);
			}
		}

		if (ctx->mouse_released) {
			char mousefunc_str[64];
			int i = sprintf(mousefunc_str, "(ed:view-mouserelease %d %d 0)", ed_view_mx, ed_view_my) - 2;

			if (ctx->mouse_released & MU_MOUSE_LEFT) {
				mousefunc_str[i] = '1';
				script_do_string(mousefunc_str);
			}
			if (ctx->mouse_released & MU_MOUSE_RIGHT) {
				mousefunc_str[i] = '2';
				script_do_string(mousefunc_str);
			}
			if (ctx->mouse_released & MU_MOUSE_MIDDLE) {
				mousefunc_str[i] = '3';
				script_do_string(mousefunc_str);
			}
		}
		
		if (ctx->scroll_delta.y) {
			char mousefunc_str[64];
			int i = sprintf(mousefunc_str, "(ed:view-mousewheel %d %d  0)", ed_view_mx, ed_view_my) - 2;

			if (ctx->scroll_delta.y > 0 && ctx->scroll_delta.y <=  9) { // DUMB
				mousefunc_str[i-1] = ' ';
				mousefunc_str[i] = '0' + ctx->scroll_delta.y;
			}

			if (ctx->scroll_delta.y < 0 && ctx->scroll_delta.y >= -9) {
				mousefunc_str[i-1] = '-';
				mousefunc_str[i] = '0' - ctx->scroll_delta.y;
			}

			script_do_string(mousefunc_str);
		}
	} else {
		ed_view_hover = 0;
	}

	/* draw */

	if (rect.w != view->buf.width || rect.h != view->buf.height) {
		picture_resize(&view->buf, rect.w, rect.h);
		view->redraw = 1;
	}

	if (view->redraw) {
		ed_view_draw(view, fe_ctx);
		view->redraw = 0;
	}

	mu_draw_image(ctx, &view->buf, 255, rect, rect);
	// TODO: any 'high speed' mouse stuff could possibly be drawn here,
	//       directly on top of the frame buffer
	//       (e.g. next new wall update on mouse move)
}

////////////////////////////////////////////////////////////////////////////////

static fe_Object* sc_ed_view_redraw(fe_Context* ctx, fe_Object* arg)
{
	view.redraw = 1;
	return fe_null(ctx);
}

// TODO this could now also become an enginenumber
static fe_Object* sc_ed_view_setcolouri(fe_Context* ctx, fe_Object* arg)
{
	int i = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	int r = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	int g = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	int b = fe_tonumber(ctx, fe_nextarg(ctx, &arg));

	if (i < 0 || i > EC_MAX) {
		con_printf("editor colour index %d out of range (must be 0..%d)", i, EC_MAX);
		return fe_bool(ctx, 0);
	}

	if (r<0||r>255 || g<0||g>255 || b<0||b>255) {
		con_printf("rgb values must each be 0..255, got (%d, %d, %d)", r, g, b);
		return fe_bool(ctx, 0);
	}

	ed_view_setcolour(i, r, g, b);

	return fe_null(ctx);
}

static fe_Object* sc_view_to_client_free(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* xy = fe_nextarg(ctx, &arg);
	int x = fe_tonumber(ctx, fe_car(ctx, xy));
	int y = fe_tonumber(ctx, fe_cdr(ctx, xy));
	view_to_client_free(&view, &x, &y);
	return fe_cons(ctx, fe_number(ctx, x), fe_number(ctx, y));
}

static fe_Object* sc_view_to_client_snap(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* xy = fe_nextarg(ctx, &arg);
	int x = fe_tonumber(ctx, fe_car(ctx, xy));
	int y = fe_tonumber(ctx, fe_cdr(ctx, xy));
	view_to_client_snap(&view, &x, &y);
	return fe_cons(ctx, fe_number(ctx, x), fe_number(ctx, y));
}

static fe_Object* sc_client_to_view(fe_Context* ctx, fe_Object* arg)
{
	fe_Object* xy = fe_nextarg(ctx, &arg);
	int x = fe_tonumber(ctx, fe_car(ctx, xy));
	int y = fe_tonumber(ctx, fe_cdr(ctx, xy));
	client_to_view(&view, &x, &y);
	return fe_cons(ctx, fe_number(ctx, x), fe_number(ctx, y));
}

static fe_Object* sc_view_mxy(fe_Context* ctx, fe_Object* arg)
{
	return fe_cons(ctx, fe_number(ctx, ed_view_mx), fe_number(ctx, ed_view_my));
}

script_regptr ed_view_regp[] = {
	{ "ed.view-x",        SP_INT,  &view.x,        0           },
	{ "ed.view-y",        SP_INT,  &view.y,        0           },
	{ "ed.view-zoom",     SP_INT,  &view.zoom,     0           },
	{ "ed.view-zoom-div", SP_INT,  &view.zoom_div, SP_READONLY },
	{ "ed.view-grid",     SP_INT,  &view.grid,     0           },
	{ "ed.view-layer",    SP_INT,  &edview_layer,  0           },
	{ "ed.view-mx",       SP_INT,  &ed_view_mx,    SP_READONLY },
	{ "ed.view-my",       SP_INT,  &ed_view_my,    SP_READONLY },
	{ "ed.view-hover",    SP_BOOL, &ed_view_hover, SP_READONLY },
	{}
};

script_regfunc ed_view_regf[] = {
	{ "ed:view-redraw",          sc_ed_view_redraw      },
	{ "ed:view-to-client-free",  sc_view_to_client_free },
	{ "ed:view-to-client-snap",  sc_view_to_client_snap },
	{ "ed:client-to-view",       sc_client_to_view      },
	{ "ed:view-mousecoords",     sc_view_mxy            },
	{}
};
