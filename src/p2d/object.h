
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include "matrix.h"
#include "surface.h"

#define bit(x) (1 << (x))

typedef enum {
	// it is planned for object flags to have custom script-definable behaviours.

	// 'reserved' flags that affect things executed every frame (so not thingy-able by scripts)
	// in future, could replace with 'event hooks' similar to what is planned with surfaces etc.
	// e.g. collision event hook could be registered as 'collide square' or 'collide round' funcptr.
	OF_CIRCULAR = bit(0),
} OBJECTFLAG;

typedef struct
{
	LONG x, xx, xxx;
	LONG y, yy, yyy;
	LONG z, zz, zzz;
	TVECTOR rot;
	WORD below, above, radius;
	WORD tag;
	WORD sid;
	WORD _unused1;
	DWORD flags;
	SURFACE front;
	SURFACE back;

	//

	// TODO 'pre-computed values'?
	//      would then need to be conscious about order of execution
	//      (maybe movement physics would have to be first).

	// char flrceil; // 0 if in air (not 'on' floor or ceiling); -1 if touching/through floor, 1 if touching/through ceiling
} OBJECT;

void object_local_space(OBJECT* object, MATRIX matrix);
void object_matrix(OBJECT* object, MATRIX matrix, int width, int height);
void object_update(OBJECT* object);
void object_collision(OBJECT* object);

#endif

