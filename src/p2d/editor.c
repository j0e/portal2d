
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#include <stdarg.h>
#include "engine.h"
#include "wall.h"
#include "polygon.h"
#include "level.h"
#include "maths.h"
#include "editor.h"
#include "console.h"

undo_t ed_undos; // editor undos

EDITORMODE ed_mode;

void editor_screen_on(void)
{
	plat_mouse_show(1);

	con_printf("Editor Mode Started");

	// objects[1].x = 0;
	// objects[1].y = 0;
	// objects[1].z = i2f(WORLD_UNIT * 4);

	ed_mode = EM_DRAW;

	edview_layer = 32768;

	if (!ed_fbuffer.buffer) {
		picture_create(&ed_fbuffer, plat_screen_w, plat_screen_h, 32 /* TODO plat_bpp or summat */, 0, plat_framebuffer);
	}

	ed_ui_init();
}

void editor_screen_off()
{
	ed_ui_deinit();
}

void editor_screen_execute(void)
{
	plat_time_update();
	plat_input_update();

	ed_ui_update();
}

void editor_screen_keypress(int plat_key, int text_key)
{
	ed_ui_keypress(plat_key, text_key);
}

void editor_screen_keyrelease(int plat_key, int text_key)
{
	ed_ui_keyrelease(plat_key, text_key);
}

void editor_screen_mousepress(int mouse_x, int mouse_y, int button)
{
	ed_ui_mousepress(mouse_x, mouse_y, button);
}

void editor_screen_mouserelease(int mouse_x, int mouse_y, int button)
{
	ed_ui_mouserelease(mouse_x, mouse_y, button);
}

void editor_screen_mousemove(int x, int y)
{
	ed_ui_mousemove(x, y);
}

void screen_change_editor(void)
{
	SCREEN_CHANGE(editor);
}

