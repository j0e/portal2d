// platform.h: protos for platform-specific stuff go in here.
// implementations in platform/<platform name>.c - only one to be included.

#ifndef __PLATFORM_H__
#define __PLATFORM_H__

#include <stdint.h>

// platform-specific headers
#ifdef PLAT_INCLUDE_FILE
	#include PLAT_INCLUDE_FILE
#endif

extern int plat_exiting;

/* Timer Routines. Clocked at 120(!) ticks per second. */

extern unsigned plat_mstotal;

void plat_time_init();
//void plat_time_deinit();
unsigned plat_time_update();

/* events */

typedef void (*plat_fn_keypress_t)(int plat_key, int text_key);
typedef void (*plat_fn_keyrelease_t)(int plat_key, int text_key);
typedef void (*plat_fn_mousepress_t)(int x, int y, int button);
typedef void (*plat_fn_mouserelease_t)(int x, int y, int button);
typedef void (*plat_fn_mousemove_t)(int x, int y);

extern plat_fn_keypress_t plat_event_keypress;
extern plat_fn_keyrelease_t plat_event_keyrelease;
extern plat_fn_mousepress_t plat_event_mousepress;
extern plat_fn_mouserelease_t plat_event_mouserelease;
extern plat_fn_mousemove_t plat_event_mousemove;

/* video */

extern unsigned plat_screen_w;
extern unsigned plat_screen_h;
// TODO separate framebuffer width and height

extern uint32_t *plat_framebuffer;

void plat_video_init(unsigned width, unsigned height, const char* title);
void plat_video_deinit();
void plat_video_update8(uint8_t *src);
void plat_palette_install(uint8_t palette_i, uint8_t newpalette[256][3]);
void plat_palette_set(uint8_t palette_i);
void plat_window_update();

/* input */

#define KEY_PRESSED(A) (plat_keydown[A] != plat_keyprev[A] && plat_keydown[A])
#define KEY_DOWN(A) plat_keydown[A]

#define MOD_PRESSED(A) ((plat_moddown & (A)) && !(plat_modprev & (A)))
#define MOD_DOWN(A) (plat_moddown & (A))

extern uint8_t plat_keydown[];
extern uint8_t plat_keyprev[];
extern uint32_t plat_moddown;
extern uint32_t plat_modprev;
extern uint8_t plat_mousedown[];
extern uint8_t plat_mouseprev[];
extern int plat_mouse_x;
extern int plat_mouse_y;

void plat_input_init();
//void plat_input_deinit();
void plat_input_update();
void plat_mouse_show(int visible);
void plat_mouse_write(int x, int y);

#endif
