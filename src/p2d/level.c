
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#include "level.h"
#include "editor.h"

int tick;

WALL    walls[MAX_WALL];
SECTOR  sectors[MAX_SECTOR];
OBJECT  objects[MAX_OBJECT];
PICTURE textures[MAX_TEXTURE];
WORD    sector_list[MAX_SECTOR];

int sector_list_count;

int chunk_write(FILE* fp, int type, int id, int flag, void* buff, int size)
{
	return
		(fwrite(&type, sizeof(int), 1, fp) == 1) &&
		(fwrite(&id  , sizeof(int), 1, fp) == 1) &&
		(fwrite(&flag, sizeof(int), 1, fp) == 1) &&
		(fwrite(&size, sizeof(int), 1, fp) == 1) &&
		(fwrite(buff , size       , 1, fp) == 1);
}

int chunk_read(FILE* fp, int* type, int* id, int* flag, void* buff, int* size)
{
	return
		(fread(type, sizeof(int), 1, fp) == 1) &&
		(fread(id  , sizeof(int), 1, fp) == 1) &&
		(fread(flag, sizeof(int), 1, fp) == 1) &&
		(fread(size, sizeof(int), 1, fp) == 1) &&
		(fread(buff, *size      , 1, fp) == 1);
}

// suppress 'multi-character constant' warnings when compiling with GCC
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmultichar"

void wall_write(int wid, FILE* fp)
{
	chunk_write(fp, 'WALL', wid, 0, &walls[wid], offsetof(WALL, reserved));
}

void sector_write(int sid, FILE* fp)
{
	chunk_write(fp, 'SECT', sid, 0, &sectors[sid], offsetof(SECTOR, reserved));
}

void level_write(FILE* fp)
{
	// header
	chunk_write(fp, 'p2eL', MAPVERSION, 0, NULL, 0);

	int i;
	for (i = 0; i < MAX_WALL  ; i++) if (walls  [i].sid) wall_write(i, fp);
	for (i = 0; i < MAX_SECTOR; i++) if (sectors[i].lid) sector_write(i, fp);
	// TODO entities etc. here
}

void level_clear(void)
{
	memset(walls, 0, sizeof(walls));
	memset(sectors, 0, sizeof(sectors));
}

void level_read_wall(WALL* wall, void *data, size_t size, WORD version)
{
	memset(wall, 0, sizeof(WALL));
	memcpy(wall, data, size);
}

void level_read_sector(SECTOR* sector, void *data, size_t size, WORD version)
{
	memset(sector, 0, sizeof(SECTOR));
	memcpy(sector, data, size);
}

void level_read(FILE* fp)
{
	char buffer[1024];
	WORD version = 0;
	unsigned offset = 0;

	int type = 0, id, flag, size;

	level_clear();

	undo_clear(&ed_undos);

	int header_read = 0;

	// check for header - note: until header has some extra data,
	//                    this function will return 0
	if (chunk_read(fp, &type, &id, &flag, buffer, &size) == 0) {
		if (type == 'p2eL') { // new format header
			// id = version :^)
			version = id;

			// in future (if version > 1) read additional header data

			header_read = 1;
		} else if (!type) {
			// 0 bytes read and type not set, file is empty(?)
			return;
		} else {
			// 0 bytes read and type is unknown number
			fprintf(stderr, "unknown type %d", type);
			return;
		}
	}

	do {
		switch (type) {
			case 'p2eL':
				// There should be only one of these, at the very start of the file.
				// If you put it anywhere else, you deserve whatever happens to you.
				if (header_read == 1) {
					header_read++;
				} else if (header_read) {
					fprintf(stderr, "multiple headers in file\n");
				} else {
					fprintf(stderr, "header not at start of file\n");
				}

				break;
			case 'WALL':
				assert(id < MAX_WALL);
				level_read_wall(&walls[id], buffer, size, version);
				break;

			case 'SECT':
				assert(id < MAX_SECTOR);
				level_read_sector(&sectors[id], buffer, size, version);
				break;

			case 'OBJE':
				fprintf(stderr, "TODO object implementation here\n"); // level_read_entity(...);
				break;

			default:
				fprintf(stderr, "unknown chunk type %d\n", type);
				break;
		}
	} while (chunk_read(fp, &type, &id, &flag, buffer, &size));
}

#pragma GCC diagnostic pop

int level_save_to_file(const char *filename)
{
	FILE* fp = fopen(filename, "wb");

	if (fp)
	{
		level_write(fp);
		fclose(fp);
		return 1;
	} else {
		char sbuf[1024];
		sprintf(sbuf, "level_save_to_file %s", filename);
		perror(sbuf);
		return 0;
	}
}

int level_load_from_file(const char *filename)
{
	// note: script should not be executed in this function.
	//       leave that to editor/engine/game modes.
	FILE* fp = fopen(filename, "rb");

	if (fp)
	{
		level_read(fp);
		fclose(fp);

		undo_reinit(&ed_undos);
		return 1;
	} else {
		char sbuf[1024];
		sprintf(sbuf, "level_load_from_file %s", filename);
		perror(sbuf);
		return 0;
	}
}

