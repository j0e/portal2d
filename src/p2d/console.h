#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include "script.h"
#include <stdbool.h>

#define CONSOLE_BUF_SIZE 4096

typedef struct {
	char buf[CONSOLE_BUF_SIZE];
	int idx;
	bool updated;
	char inputline[2048];
	bool submitting;
	bool debug; // TODO better name
} CONSOLE;

extern CONSOLE console;

void con_printf(const char *text, ...);
void con_warning(const char *text, ...);
void con_error(const char *text, ...);
void con_submit(void);

#endif
