
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __MATHS_H__
#define __MATHS_H__

#include "defs.h"
#include <math.h>
#include <stdint.h>

/*
**  MATH
*/

#ifndef M_PI
#define M_PI (3.141592653589793238462643383279502884L)
#endif

#define MIN(A,B) ((A)<(B) ? (A) : (B))
#define MAX(A,B) ((A)>(B) ? (A) : (B))
#define CLAMP(A,B,C) MIN(MAX(A,B),C)

int isqrt(int value);

static inline int imin(int value1, int value2) {
	return value1 < value2 ? value1 : value2;
}

static inline int imax(int value1, int value2) {
	return value1 > value2 ? value1 : value2;
}

static inline int iclamp(int value1, int value2, int value3) {
	return imin(imax(value1, value2), value3);
}

static inline int isqr(int value) {
	return value * value;
}

// TODO https://stackoverflow.com/a/39431923
static inline int32_t imuldiv(int32_t value, int32_t mul, int32_t div) {
	#ifdef DIV0_SATURATE
		// BODGE lol
		int64_t result = ((int64_t)value * mul / (div ? div : 0x7fffffff));

		if (result > 2147483647) {
			return 2147483647;
		} else if (result < -2147483648) {
			return -2147483648;
		} else return (int32_t)result;
	#else
		return (int32_t)((int64_t)value * mul / div);
	#endif
}

/*
**  FIXED (16.16)
**  Thanks to the KenBuild, Doom and Decent for some great ideas.
*/

#define FRACCEIL 0xFFFF
#define FIXHALF 0x8000
#define FIXONE 0x10000

#define f2i(a) ((a)>>16)
#define i2f(a) ((a)<<16)
// TODO rename to f2n (fixed to number)
#define f2fl(a) (((float)(a))/ i2f(1))
// TODO rename to n2f
#define fl2f(a) ((int)((a)*i2f(1)))

// TODO (or should this be in e.g. level.c/h?) i2w and w2i
//      (integer to world unit i.e. << / >> 6)

#define fixceil(A) (((A)+FRACCEIL)>>16)

#define SINTABLE_MAX 2048
#define SIN_90    (SINTABLE_MAX>>2)
#define SIN_180   (SINTABLE_MAX>>1)
#define SIN_360   SINTABLE_MAX
#define SIN_UNIT  i2f(1)
#define fixsin(A) sintable[(A)&(SINTABLE_MAX-1)]
#define fixcos(A) sintable[((A)+(SIN_90))&(SINTABLE_MAX-1)]

extern int sintable[SINTABLE_MAX];

// TODO extern sintable build function

static inline int fixdiv(int a, int b) {
	#ifdef DIV0_SATURATE
		// BODGE lol
		//return ((int64_t)a << 16) / (int32_t)(b ? b : 0x7fffffff);
		return b ? ((int64_t)a << 16) / (int32_t)b : 0;
	#else
		return ((int64_t)a << 16) / b;
	#endif
}

static inline int fixinv(int a) {
	return fixdiv(i2f(1), a);
}

static inline int fixmul(int a, int b) {
	return ((int64_t)a * b) >> 16;
}

static inline int fixdot3(int a1, int b1, int a2, int b2, int a3, int b3) {
	return ((int64_t)a1 * b1 + (int64_t)a2 * b2 + (int64_t)a3 * b3) >> 16;
}

#endif
