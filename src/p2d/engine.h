
/*
** The contents of this file are subject to the Mozilla Public License
** Version 1.1 (the "License"); you may not use this file except in compliance
** with the License. You may obtain a copy of the License at
** http://www.mozilla.org/MPL/
**
** Software distributed under the License is distributed on an "AS IS" basis,
** WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
** the specific language governing rights and limitations under the License.
*/

#ifndef __ENGINE_H__
#define __ENGINE_H__

#include "defs.h"
#include "maths.h"
#include "palette.h"
#include "picture.h"
#include "matrix.h"
#include "platform.h"
#include "object.h"

#define NEXT_WALL(A) (walls[A].next)
#define FIRST_WALL(A) (sectors[A].first_wall)

#define SCREEN_CHANGE(SCR)  \
	current_screenoff();                                 \
	current_execute         = SCR##_screen_execute;      \
	current_screenon        = SCR##_screen_on;           \
	current_screenoff       = SCR##_screen_off;          \
	plat_event_keypress     = SCR##_screen_keypress;     \
	plat_event_keyrelease   = SCR##_screen_keyrelease;   \
	plat_event_mousepress   = SCR##_screen_mousepress;   \
	plat_event_mouserelease = SCR##_screen_mouserelease; \
	plat_event_mousemove    = SCR##_screen_mousemove;    \
	current_screenon();

extern void (*current_execute)(void);
extern void (*current_screenon)(void);
extern void (*current_screenoff)(void);

/*
**  engine.c
*/

#define WORLD_UNIT 64

extern int mousegrab; // TODO this should go in PLAT really

extern OBJECT* camera;

extern PICTURE pic_console;

// new customisable engine/gameplay variables.
extern int sv_height;
extern int sv_gravity;
extern int sv_friction_div;
extern int sv_landing;
extern int sv_accel;
extern int sv_accel_div;
extern int sv_jump;
extern int sv_crouch;
extern int sv_turn;

void screen_change_engine(void);

void engine_create(void);
void engine_destroy(void);

// textures
extern PALLETE palette; // TEMP(?)
void texture_reset(void);
bool texture_register(char *filename);
void loadsky(char *filename);

/*
**  engine_game.c
*/

extern char curgame[128];


extern void game_load_palette(PALLETE palette);
extern void game_load_textures(void);
void game_set(const char* gamename);

/*
**  engine_render.c
*/

// #define P2D_STIPPLE_BLEND // TODO make this a separate render mode

extern unsigned framebuffer_w;
extern unsigned framebuffer_h;

#define NUMLIGHTMAPS 32
extern LIGHTMAP lightmaps[NUMLIGHTMAPS]; // TODO level-specific lightmaps.

extern CLUT blender; // TODO multiple blenders

// frame buffers
extern PICTURE pic_bbuffer;
extern PICTURE pic_zbuffer;
extern PICTURE pic_stencil;

// images
extern PICTURE pic_font;
extern PICTURE pic_arrow;
extern PICTURE pic_crosshair;

extern PICTURE pic_sky; // TODO should be level-specific

void render_init_buffers(void);
void render_deinit_buffers(void);

extern void render_view(OBJECT* camera);
extern void render_console(void); // TODO rename to render_console

#endif
