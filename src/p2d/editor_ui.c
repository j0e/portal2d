#include "editor.h"
#include "engine.h"
#include "picture.h"
#include "platform.h"
#include "script.h"
#include "include/microui.h"
#include "include/microui_font.h"
#include "console.h"

PICTURE ed_fbuffer; // wrapper for main framebuffer
PALLETE ed_palettes[MAX_PALLETE];

EDVIEW view;

static mu_Context mu_ctx;
static int clip_x1, clip_y1, clip_x2, clip_y2;

hashimap_t editor_keybinds; // for now must not be static as init'd in engine_create

static int get_text_width(mu_Font font, const char *text, int len) {
	if (len == -1) {
		len = strlen(text);
	}
	
	int res = 0;
	for (const char *p = text; *p && len--; p++) {
		if ((*p & 0xc0) == 0x80) {
			continue;
		}
		int chr = mu_min((unsigned char) *p, 127);
		res += mu_atlas[MU_ATLAS_FONT + chr].w;
	}
	return res;
}

static int get_text_height(mu_Font font)
{
	return 18;
}

uint8_t rgb32_red(uint32_t rgb) {
		return (rgb >> 16) & 0xFF;
}

uint8_t rgb32_green(uint32_t rgb) {
		return (rgb >> 8) & 0xFF;
}

uint8_t rgb32_blue(uint32_t rgb) {
		return rgb & 0xFF;
}

uint32_t rgb888_to_rgb32(uint8_t red, uint8_t green, uint8_t blue) {
		return ((uint32_t)red << 16) | ((uint32_t)green << 8) | blue;
}

static void draw_picture(PICTURE* dst, mu_Rect _dst, PICTURE* src, mu_Rect _src)
{
	int x1 = _dst.x, y1 = _dst.y,  x2 = _dst.x + _dst.w, y2 = _dst.y + _dst.h;

	int u1 = _src.x, v1 = _src.y,  u2 = _src.x + _src.w, v2 = _src.y + _src.h;

	int w, h, u, uu, vv;

	if ((w = x2 - x1) <= 0 || (h = y2 - y1) <= 0) return;

	u1 = i2f(u1);
	v1 = i2f(v1);
	u2 = i2f(u2);
	v2 = i2f(v2);

	uu = (u2 - u1) / w;
	vv = (v2 - v1) / h;

	if (x1 < 0          )   {u1 -= x1 * uu; x1 = 0;}
	if (y1 < 0          )   {v1 -= y1 * vv; y1 = 0;}
	if (x2 > dst->width )   {x2  = dst->width     ;}
	if (y2 > dst->height)   {y2  = dst->height    ;}

	if ((w = x2 - x1) <= 0 || (h = y2 - y1) <= 0) return;

	if (src->bpp == 8 && dst->bpp == 8)
	{
		#define INNER_LOOP \
			for (;  y1 < y2;  y1++,v1+=vv) { \
				BYTE* I = src->scanlines.b[f2i(v1)]; \
				BYTE* O = &dst->scanlines.b[y1][x1]; \
				for(w = x2 - x1, u = u1;  w--;  u += uu, O++) { AFFINE } \
			}

		#define AFFINE *O = I[f2i(u)];
			INNER_LOOP
		#undef AFFINE

		#undef INNER_LOOP
	}
	else if (src->bpp == 32 && dst->bpp == 32)
	{
		#define INNER_LOOP \
			for (;  y1 < y2;  y1++,v1+=vv) { \
				uint32_t* I = src->scanlines.l[f2i(v1)]; \
				uint32_t* O = &dst->scanlines.l[y1][x1]; \
				for(w = x2 - x1, u = u1;  w--;  u += uu, O++) { AFFINE } \
			}

		#define AFFINE *O = I[f2i(u)];
			INNER_LOOP
		#undef AFFINE

		#undef INNER_LOOP
	}
	else if (src->bpp == 8 && dst->bpp == 32)
	{
		#define INNER_LOOP \
			for (;  y1 < y2;  y1++,v1+=vv) { \
				uint8_t* I = src->scanlines.b[f2i(v1)]; \
				uint32_t* O = &dst->scanlines.l[y1][x1]; \
				for(w = x2 - x1, u = u1;  w--;  u += uu, O++) { AFFINE } \
			}

		// TEMP - just use 'primary' engine palette for now.
		// TODO - Is this inefficient?
		#define AFFINE  *O = ((uint32_t)ed_palettes[0][I[f2i(u)]][0] << 16) | ((uint32_t)ed_palettes[0][I[f2i(u)]][1] << 8) | ed_palettes[0][I[f2i(u)]][2];
			INNER_LOOP
		#undef AFFINE

		#undef INNER_LOOP
	}
	else {
		fprintf(stderr, "unsupported bpp combination %d -> %d\n", src->bpp, dst->bpp);
	}
}

void ed_console_ui(mu_Context* ctx)
{
	mu_layout_row(ctx, 1, (int[]) { -1 }, -25);
	mu_begin_panel(ctx, "Console Output");
		mu_Container *panel = mu_get_current_container(ctx);
		mu_layout_row(ctx, 1, (int[]) { -1 }, -1);
		mu_text(ctx, console.buf);
	mu_end_panel(ctx);

	if (console.updated) {
		panel->scroll.y = panel->content_size.y;
		console.updated = false;
	}

	/* input textbox + submit button */
	mu_layout_row(ctx, 2, (int[]) { 70, -1 }, 0);
	if (mu_button(ctx, "DO IT")) { console.submitting = true; }
	if (mu_textbox(ctx, console.inputline, sizeof(console.inputline)) & MU_RES_SUBMIT) {
		mu_set_focus(ctx, ctx->last_id);
		console.submitting = true;
	}
}

static fe_Object* sc_update_view(fe_Context* ctx, fe_Object* arg)
{
	ed_view_update(&mu_ctx, &view, ctx);
	return fe_null(ctx);
}


void ed_ui_init(void)
{
	clip_x1 = 0;
	clip_y1 = 0;
	clip_x2 = plat_screen_w;
	clip_y2 = plat_screen_h;

	mu_init(&mu_ctx);
	mu_ctx.text_width = get_text_width;
	mu_ctx.text_height = get_text_height;

	ed_view_init(&view, 640, 480);
}

void ed_ui_deinit(void)
{
	ed_view_deinit(&view);
}

extern PICTURE pic_logo;

static bool change_to_3d = false;

// semi-BODGE
static fe_Object* sc_change_to_3d(fe_Context* ctx, fe_Object* arg)
{
	change_to_3d = true;
	return fe_null(ctx);
}

void ed_ui_update(void)
{
	mu_begin(&mu_ctx);

	const char *window_title = "portal2E editor";

	if (mu_begin_window_ex(&mu_ctx, window_title, mu_rect(0, 0, plat_screen_w, plat_screen_h), MU_OPT_NOTITLE | MU_OPT_NOCLOSE | MU_OPT_NORESIZE)) {
		script_push();
		script_do_string_uncaught("(ed:ui-frame)"); // TODO separate functions for 'update' and 'draw'
		script_pop();

		// submit console at end of frame.
		if (console.submitting) {
			con_submit();
		}

		mu_end_window(&mu_ctx);
	}

	// any other windows can go here if needed.

	mu_end(&mu_ctx);

	if (change_to_3d) { // BODGE
		change_to_3d = false;
		screen_change_engine(); // TODO 'ed:screen-off' function etc
	} else {
		ed_ui_render();
		plat_window_update();
	}
}

void ed_ui_render(void)
{
	mu_Rect fullscreen = mu_rect(0, 0, plat_screen_w, plat_screen_h); // TODO - resizeable version of this
	mu_Rect pic_logo_rect = mu_rect(0, 0, pic_logo.width, pic_logo.height);

	draw_picture(&ed_fbuffer, fullscreen, &pic_logo, pic_logo_rect);

	mu_Command *cmd = NULL;

	while (mu_next_command(&mu_ctx, &cmd)) {
		switch (cmd->type) {
			case MU_COMMAND_TEXT: //draw_text(cmd->text.str, cmd->text.pos, cmd->text.color); break;
			{
				int  x1,y1, x2,y2,  u1,v1, u2,v2;

				const char *text = cmd->text.str;

				// TODO clean up this function!  failed first attempt at cleanup below...
				mu_Rect dst = { cmd->text.pos.x, cmd->text.pos.y, 0, 0 };
				for (const char *p = text; *p; p++) {
					if ((*p & 0xc0) == 0x80) { continue; }
					// clip x1/y1
					if (dst.x < 0 || dst.y < 0) continue;
					x1 = dst.x, y1 = dst.y;
					int chr = mu_min((unsigned char) *p, 127);
					mu_Rect src = mu_atlas[MU_ATLAS_FONT + chr];
					//clip x2/y2
					if (dst.x + src.w > plat_screen_w || dst.y + src.h > plat_screen_h) break;
					x2 = dst.x + (dst.w = src.w);
					y2 = dst.y + (dst.h = src.h);

					// HaRzA b0NgWaTeR m0dE:·
					//x2 = (dst.x == x1 ? x1 : error("width mismatch")) + (dst.w = src.w)
					//y2 = (assert(dst.y == y1), y1) + (dst.w = src.w)

					// get texture rectangle - assume it's valid - TODO bounds checking if we use custom fonts
					u1 = src.x, v1 = src.y;
					u2 = (src.x + src.w), v2 = (src.y + src.h);

					if (x1 < clip_x1) x1 = clip_x1;
					if (y1 < clip_y1) y1 = clip_y1; // TODO adjust u,v coordinates
					if (x2 > clip_x2) x2 = clip_x2;
					if (y2 > clip_y2) y2 = clip_y2;

					// render to framebuffer here
					int x, y;
					//int dest_r, dest_g, dest_b;
					for (y = y1;  y < y2;  y++) for (x = x1;  x < x2;  x++) {
						uint32_t dst_i = y * plat_screen_w + x;

						uint8_t src_pixel = mu_atlas_texture[((v1 + y - y1) * MU_ATLAS_WIDTH) + (u1 + x - x1)];
						if (src_pixel) {
							// opaque colourkey blend.
							//plat_framebuffer[dst_i] = src_pixel*65536 + src_pixel*256 + src_pixel;

							// additive blend (TODO colours).
							plat_framebuffer[dst_i] = rgb888_to_rgb32(
								imin(rgb32_red  (plat_framebuffer[dst_i]) + src_pixel, 255),
								imin(rgb32_green(plat_framebuffer[dst_i]) + src_pixel, 255), 
								imin(rgb32_blue (plat_framebuffer[dst_i]) + src_pixel, 255));

							// TODO proper alpha blending
						}
					}

					//assert((u1 + x - x1) == u2 - 1);
					//assert((v1 + y - y1) == v2 - 1);

					dst.x += dst.w;
				}

				// moar 'optimised' code layout but this version doesn't work.
				// TODO either reattempt or find problem with below so we can use it instead of the novicefordjones version above...
				/*
				mu_Rect dst = { cmd->text.pos.x, cmd->text.pos.y, 0, 0 };
				for (const char *p = cmd->text.str; *p; p++) {
					if ((*p & 0xc0) == 0x80) continue;
					if (dst.x < 0 || dst.y < 0) continue;
					int chr = mu_min((unsigned char) *p, 127);
					mu_Rect src = mu_atlas[MU_ATLAS_FONT + chr];
					if (dst.x + src.w > plat_screen_w || dst.y + src.h > plat_screen_h) break;

					int ix, iy;
					uint16_t r, g, b;
					for (iy = 0;  iy < dst.h;  iy++) for (ix = 0;  ix < dst.w;  ix++) {
						uint8_t src_pixel = mu_atlas_texture[((src.y + iy) * MU_ATLAS_WIDTH) + (src.x + ix)];

						if (src_pixel) {
							//plat_framebuffer[y * plat_screen_w + x] = src_pixel*65536 + src_pixel*256 + src_pixel; // opaque colourkey blend.
							uint32_t dst_i = (dst.y + iy) * plat_screen_w + (dst.x + ix); // additive blend.
							plat_framebuffer[dst_i] = rgb888_to_rgb32(
								(uint8_t)(r = rgb32_red(plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : r,
								(uint8_t)(g = rgb32_green(plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : g,
								(uint8_t)(b = rgb32_blue(plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : b
							);
						}
					}

					//assert((src.x + ix) == u2 - 1);
					//assert((src.y + iy) == v2 - 1);

					dst.x += dst.w;
				}
				*/
			}
			break;

			case MU_COMMAND_RECT: //draw_fillrect(cmd->rect.rect, cmd->rect.color); break;
			{
				int x1 = cmd->rect.rect.x,  y1 = cmd->rect.rect.y,  x2 = cmd->rect.rect.x + cmd->rect.rect.w,  y2 = cmd->rect.rect.y + cmd->rect.rect.h;
				if (x1 < clip_x1) x1 = clip_x1;
				if (y1 < clip_y1) y1 = clip_y1;
				if (x2 > clip_x2) x2 = clip_x2;
				if (y2 > clip_y2) y2 = clip_y2;

				if (x1 < 0) x1 = 0;
				if (y1 < 0) y1 = 0;
				if (x2 > plat_screen_w) x2 = plat_screen_w;
				if (y2 > plat_screen_h) y2 = plat_screen_h;

				int Y, X;
				for (Y = y1;  Y < y2;  Y++) for (X = x1;  X < x2;  X++) {
					plat_framebuffer[Y * plat_screen_w + X] = rgb888_to_rgb32(cmd->rect.color.r, cmd->rect.color.g, cmd->rect.color.b);
				}
			}
			break;

			case MU_COMMAND_ICON: //draw_icon(cmd->icon.id, cmd->icon.rect, cmd->icon.color); break;
			{
				mu_Rect src = mu_atlas[cmd->icon.id];
				int x1 = cmd->icon.rect.x + (cmd->icon.rect.w - src.w) / 2;
				int y1 = cmd->icon.rect.y + (cmd->icon.rect.h - src.h) / 2;
				int x2 = x1 + src.w;
				int y2 = y1 + src.h;
				if (x1 < clip_x1) x1 = clip_x1;
				if (y1 < clip_y1) y1 = clip_y1;
				if (x2 > clip_x2) x2 = clip_x2;
				if (y2 > clip_y2) y2 = clip_y2;

				int x, y, u, v;
				uint16_t r, g, b;

				for (y = y1, v = 0;  y < y2;  y++, v++) for (x = x1, u = 0;  x < x2;  x++, u++) {
					uint8_t src_pixel = mu_atlas_texture[v * MU_ATLAS_WIDTH + u];

					if (src_pixel) {
						//plat_framebuffer[y * plat_screen_w + x] = src_pixel*65536 + src_pixel*256 + src_pixel; // opaque colourkey blend.
						uint32_t dst_i = (y) * plat_screen_w + (x); // additive blend.
						plat_framebuffer[dst_i] = rgb888_to_rgb32(
							(uint8_t)(r = rgb32_red  (plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : r,
							(uint8_t)(g = rgb32_green(plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : g,
							(uint8_t)(b = rgb32_blue (plat_framebuffer[dst_i]) + src_pixel) > 255 ? 255 : b
						);
					}
				}
			}
			break;

			case MU_COMMAND_CLIP: //set_clip_rect(cmd->clip.rect); break;
			{
				clip_x1 = cmd->clip.rect.x;
				clip_y1 = cmd->clip.rect.y;
				clip_x2 = cmd->clip.rect.x + cmd->clip.rect.w;
				clip_y2 = cmd->clip.rect.y + cmd->clip.rect.h;
			}
			break;

			case MU_COMMAND_IMAGE: //draw_image(cmd->image.buf, cmd->image.bpp, cmd->image.dst, cmd->image.src); break;
			{
				void *buf = cmd->image.buf;
				uint8_t bpp = cmd->image.bpp;

				if (bpp == 255)
				{	// PICTURE
					draw_picture(&ed_fbuffer, cmd->image.dst, (PICTURE*)buf, cmd->image.src);
				}
				else
				{	// normal image
					fprintf(stderr, "tbh, raw buf drawing not supported rn\n");
				}
			}
			break;
		}
	}

	#undef COPYRECT
	#undef COPYCOLOUR
	#undef COPYVEC2
}

// TODO mouse buttons platform-dependent? e.g. '3' could be middle mouse in some IO libs
//      in which case, define 'plat_mouse_left' etc.
static const char button_map[256] = {
	[1] = MU_MOUSE_LEFT,
	[2] = MU_MOUSE_MIDDLE,
	[3] = MU_MOUSE_RIGHT,
};

static const char key_map[256] = {
	[KB_SHIFT]     = MU_KEY_SHIFT,
	[KB_CTRL]      = MU_KEY_CTRL,
	[KB_ALT]       = MU_KEY_ALT,
	[KB_ENTER]     = MU_KEY_RETURN,
	[KB_BACKSPACE] = MU_KEY_BACKSPACE,
	[KB_ESC]       = MU_KEY_ESCAPE,
};

void ed_ui_keypress(int plat_key, int text_key)
{
	int c = key_map[plat_key & 0xff];
	if (c) {
		mu_input_keydown(&mu_ctx, c);
	} else if (text_key >= 32 && text_key <= 126) {
		mu_input_text(&mu_ctx, (char[]){ (char)text_key, 0 });
	}

	// TODO better way to check if textbox is not focused (which is what this is supposed to do)
	if (mu_ctx.focus == 0) {
		fe_Object* func = hmi_get(&editor_keybinds, plat_key); // TODO better way to do mode-specific binds

		if (func) {
			script_keypress(func);
		}
	}
}

void ed_ui_keyrelease(int plat_key, int text_key)
{
	int c = key_map[plat_key & 0xff];
	if (c) {
		mu_input_keyup(&mu_ctx, c);
	}
}

// editor view mouse events are handled in ed_view_update during the microui loop.
void ed_ui_mousepress(int mouse_x, int mouse_y, BYTE button)
{
	switch (button) {
		case 4:
			mu_input_scroll(&mu_ctx, 0, -1);
			break;

		case 5:
			mu_input_scroll(&mu_ctx, 0, 1);
			break;

		default:
			int b = button_map[button & 0xff];
			if (b) {
				mu_input_mousedown(&mu_ctx, mouse_x, mouse_y, b);
			}
			break;
	}
}

void ed_ui_mouserelease(int mouse_x, int mouse_y, BYTE button)
{
	int b = button_map[button & 0xff];
	if (b) {
		mu_input_mouseup(&mu_ctx, mouse_x, mouse_y, b);
	}
}

void ed_ui_mousemove(int x, int y)
{
	mu_input_mousemove(&mu_ctx, x, y);
}

////////////////////////////////////////////////////////////////////////////////

/*
	SCRIPT
*/

static fe_Object* sc_row(fe_Context* ctx, fe_Object* arg)
{
	int widths[MU_MAX_WIDTHS];
	int count = 0;
	int height = 0;

	/* get widths */
	fe_Object* w = fe_nextarg(ctx, &arg);
	while (!fe_isnil(ctx, w)) {
		widths[count++] = fe_tonumber(ctx, fe_car(ctx, w));
		w = fe_cdr(ctx, w);
	}

	/* get height */
	if (!fe_isnil(ctx, arg)) {
		height = fe_tonumber(ctx, fe_nextarg(ctx, &arg));
	}

	mu_layout_row(&mu_ctx, count, widths, height);
	return fe_bool(ctx, false);
}

static fe_Object* sc_begin_column(fe_Context* ctx, fe_Object* arg)
{
	mu_layout_begin_column(&mu_ctx);
	return fe_bool(ctx, false);
}

static fe_Object* sc_end_column(fe_Context* ctx, fe_Object* arg)
{
	mu_layout_end_column(&mu_ctx);
	return fe_bool(ctx, false);
}

static fe_Object* sc_push_id(fe_Context* ctx, fe_Object* arg)
{
	char str[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), str, sizeof(str));
	mu_push_id(&mu_ctx, str, strlen(str));
	return fe_bool(ctx, false);
}

static fe_Object* sc_pop_id(fe_Context* ctx, fe_Object* arg)
{
	mu_pop_id(&mu_ctx);
	return fe_bool(ctx, false);
}

static fe_Object* sc_label(fe_Context* ctx, fe_Object* arg)
{
	char label[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), label, sizeof(label));
	mu_label(&mu_ctx, label);
	return fe_bool(ctx, false);
}

static fe_Object* sc_button(fe_Context* ctx, fe_Object* arg)
{
	char label[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), label, sizeof(label));
	return fe_bool(ctx, mu_button(&mu_ctx, label));
}

static fe_Object* sc_layout_next(fe_Context* ctx, fe_Object* arg)
{
	mu_layout_next(&mu_ctx);
	return fe_bool(ctx, false);
}

static fe_Object* sc_console_ui(fe_Context* ctx, fe_Object* arg)
{
	ed_console_ui(&mu_ctx);
	return fe_bool(ctx, false);
}

static fe_Object* sc_begin_panel(fe_Context* ctx, fe_Object* arg)
{
	char id[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), id, sizeof(id));
	mu_begin_panel(&mu_ctx, id);
	return fe_null(ctx);
}

static fe_Object* sc_end_panel(fe_Context* ctx, fe_Object* arg)
{
	mu_end_panel(&mu_ctx);
	return fe_null(ctx);
}

static fe_Object* sc_begin_panel_noframe(fe_Context* ctx, fe_Object* arg)
{
	char id[128];
	fe_tostring(ctx, fe_nextarg(ctx, &arg), id, sizeof(id));
	mu_begin_panel_ex(&mu_ctx, id, MU_OPT_NOFRAME);
	return fe_null(ctx);
}

static fe_Object* sc_end_panel_noframe(fe_Context* ctx, fe_Object* arg)
{
	mu_end_panel(&mu_ctx);
	return fe_null(ctx);
}

static fe_Object* sc_mouse_down(fe_Context* ctx, fe_Object* arg)
{
	switch ((int)fe_tonumber(ctx, fe_nextarg(ctx, &arg))) {
		case  1: return fe_bool(ctx, mu_ctx.mouse_down & MU_MOUSE_LEFT);
		case  2: return fe_bool(ctx, mu_ctx.mouse_down & MU_MOUSE_RIGHT);
		case  3: return fe_bool(ctx, mu_ctx.mouse_down & MU_MOUSE_MIDDLE);
		default: return fe_false(ctx); // or null?
	}
}

static fe_Object* sc_mouse_pressed(fe_Context* ctx, fe_Object* arg)
{
	switch ((int)fe_tonumber(ctx, fe_nextarg(ctx, &arg))) {
		case  1: return fe_bool(ctx, mu_ctx.mouse_pressed & MU_MOUSE_LEFT);
		case  2: return fe_bool(ctx, mu_ctx.mouse_pressed & MU_MOUSE_RIGHT);
		case  3: return fe_bool(ctx, mu_ctx.mouse_pressed & MU_MOUSE_MIDDLE);
		default: return fe_false(ctx); // or null?
	}
}

static fe_Object* sc_mouse_released(fe_Context* ctx, fe_Object* arg)
{
	switch ((int)fe_tonumber(ctx, fe_nextarg(ctx, &arg))) {
		case  1: return fe_bool(ctx, mu_ctx.mouse_released & MU_MOUSE_LEFT);
		case  2: return fe_bool(ctx, mu_ctx.mouse_released & MU_MOUSE_RIGHT);
		case  3: return fe_bool(ctx, mu_ctx.mouse_released & MU_MOUSE_MIDDLE);
		default: return fe_false(ctx); // or null?
	}
}

////////////////////////////////////////////////////////////////////////////////

// TEMP(?) - integerbox

static int integer_textbox(mu_Context* ctx, int* value, int state, mu_Rect r, mu_Id id, integerbox_edit_func callback) {
	if (
		ctx->mouse_pressed == MU_MOUSE_LEFT
		//&& ctx->key_down & MU_KEY_SHIFT
		&& ctx->hover == id
		&& ctx->number_edit != id
		&& state > 0
	) {
		ctx->number_edit = id;
		if (state != 1) {
			sprintf(ctx->number_edit_buf, "");
		} else {
			snprintf(ctx->number_edit_buf, MU_MAX_FMT - 1, MU_INTEGER_FMT, *value);
		}
	}

	if (ctx->number_edit == id && state > 0) {
		int res = mu_textbox_raw(ctx, ctx->number_edit_buf, sizeof(ctx->number_edit_buf), id, r, 0);
		if (res & MU_RES_SUBMIT) {
			char** end;
			int newvalue = (int)strtol(ctx->number_edit_buf, end, 10);

			// check for non-numeric value entered
			if (*end == ctx->number_edit_buf) {
				ctx->number_edit = 0;
				return 0;
			}

			// run callback validation function.
			// should change *value and return 1 if valid, otherwise return 0.
			if (callback) {
				callback(value, newvalue);
			} else {
				*value = newvalue;
			}

			ctx->number_edit = 0;
		} else if (ctx->key_pressed & MU_KEY_ESCAPE || ctx->focus != id) {
			ctx->number_edit = 0;
		} else {
			return 1;
		}
	}
	return 0;
}

// expects a fieldspec - (state label validate change) [for now - we could make it something else later on]
static fe_Object* sc_ui_ed_field(fe_Context* ctx, fe_Object* arg) {
	// functionality seems arbitrarily split between C and fe atm.
	// validation function is to be 'passed' to C, but then the
	// change mechanism is to happen in fe which then calls another C function.

	// can use this if fieldspec is 'needed'
	//fe_Object* fieldspec = fe_nextarg(ctx, &arg);

	// state must remain 'constant' i.e. reassigning in the fieldspec would cause id to change.
	// this *should* mean that state remains the same (as it points to the same address in memory).
	//fe_Object* fieldstate = fe_car(ctx, fieldspec); // ( value . isactive )
	fe_Object* fieldstate = fe_nextarg(ctx, &arg);

	// don't validate for now lmao
	//fe_Object* validatefunc = fe_caadadadaar(ctx, whatever);

	int oldvalue = fe_tonumber(ctx, fe_car(ctx, fieldstate));
	int active = fe_tonumber(ctx, fe_cdr(ctx, fieldstate)); // 0, 1, 2

	int result = 0;

	mu_Id id = mu_get_id(&mu_ctx, fieldstate, sizeof(fieldstate));
	mu_Rect base = mu_layout_next(&mu_ctx);
	
	int value = oldvalue;

	/* handle text input mode */
	// TODO: if the fieldactive is 0, do not allow mouse interaction
	if (integer_textbox(&mu_ctx, &value, active, base, id, NULL)) {
		return fe_null(ctx);
	}

	// check if value has changed after integer_textbox editing,
	// only update if textbox is 'active'
	if (value != oldvalue && active) {
		result |= MU_RES_SUBMIT;

		// update value stored in fieldstate object.
		// TODO(?) it would be cooler if we could change the numeric value of the existing object directly,
		//         rather than replacing it with a new object
		fe_setcar(ctx, fieldstate, fe_number(ctx, value));
	}

	/* handle normal mode */
	mu_update_control(&mu_ctx, id, base, MU_OPT_ALIGNCENTER);

	/* draw base */
	mu_draw_control_frame(&mu_ctx, id, base, MU_COLOR_BASE, MU_OPT_ALIGNCENTER);
	/* draw text  */
	char buf[MU_MAX_FMT + 1];
	if (active > 0) {
		if (active == 2) {
			sprintf(buf, "-");
		} else {
			sprintf(buf, MU_INTEGER_FMT, value);
		}
		mu_draw_control_text(&mu_ctx, buf, base, MU_COLOR_TEXT, MU_OPT_ALIGNCENTER);
	}

	return result ? fe_number(ctx, result) : fe_null(ctx);
}

////////////////////////////////////////////////////////////////////////////////

script_regptr ed_ui_regp[] = {
	{ "ui.mouse-x", SP_INT, &mu_ctx.mouse_pos.x, 0 },
	{ "ui.mouse-y", SP_INT, &mu_ctx.mouse_pos.y, 0 },
	{}
};

script_regfunc ed_ui_regf[] = {
	{ "ui:row",             sc_row           },
	{ "ui:begin-column",    sc_begin_column  },
	{ "ui:end-column",      sc_end_column    },
	{ "ui:push-id",         sc_push_id       },
	{ "ui:pop-id",          sc_pop_id        },
	{ "ui:label",           sc_label         },
	{ "ui:button-state",    sc_button        },
	{ "ui:layout-next",     sc_layout_next   },
	{ "ui:begin-panel", sc_begin_panel },
	{ "ui:end-panel", sc_end_panel },
	{ "ui:begin-panel-noframe", sc_begin_panel_noframe },
	{ "ui:end-panel-noframe", sc_end_panel_noframe },

	{ "ui:mouse-down",      sc_mouse_down    },
	{ "ui:mouse-pressed",   sc_mouse_pressed },
	{ "ui:mouse-released",   sc_mouse_released },

	{ "ui:ed-field", sc_ui_ed_field },

	{ "ed:ui-update-view",  sc_update_view   },
	{ "ed:console-ui",      sc_console_ui    },
	{ "ed:ui-change-to-3d", sc_change_to_3d  }, // semi-BODGE - this function won't 'interrupt' the rest of the function that calls it etc...
	{}
};
