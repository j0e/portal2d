#include "p2d/engine.h"
#include "p2d/editor.h"
#include "p2d/platform.h"
#include "p2d/script.h"

static void noop() { }

int main(int argc, char *argv[]) {
	engine_create();

	plat_input_init();
	plat_video_init(plat_screen_w, plat_screen_h, "portal2d");


	if (argc > 1) {
		level_load_from_file(argv[1]); // TODO set curgame from first param (or named --param)
	} else {
		//level_load_from_file("media/maps/test.map");
	}
	// load init map. TEMP
	//char buf[1024];
	//sprintf(buf, "(ed:loadmap \"%s\")", argc > 1 ? argv[1] : "test");
	//script_push();
	//script_do_string(buf);
	//script_pop();

	plat_time_init();

	current_screenoff = noop; // BODGE

	screen_change_editor();
	//screen_change_engine();

	while (!plat_exiting) {
		current_execute();
	}

	engine_destroy();
}
